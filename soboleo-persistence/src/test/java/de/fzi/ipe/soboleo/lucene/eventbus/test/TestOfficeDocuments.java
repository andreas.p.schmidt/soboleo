package de.fzi.ipe.soboleo.lucene.eventbus.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.beans.document.DocumentType;
import de.fzi.ipe.soboleo.event.officedocument.AddOfficeDocument;
import de.fzi.ipe.soboleo.event.officedocument.RemoveOfficeDocument;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.execution.lucene.webdocument.SearchResult;
import de.fzi.ipe.soboleo.lucene.LuceneResult;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;
import de.fzi.ipe.soboleo.lucene.eventubus.LuceneEventBusAdaptor;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetAllDocuments;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetAllOfficeDocuments;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentByUri;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentInputStream;

public class TestOfficeDocuments {

	private static LuceneEventBusAdaptor luceneeventbus;

	private static String directoryTestLucene = "C:\\soboleo\\spaces\\fzi\\";
	private static String directoryStorage = "c:\\soboleo\\spaces\\fzi\\storage\\";

	// private static String directoryTestLucene = "C:\\soboleo\\spaces\\fzi\\";
	// private static String directoryStorage =
	// "c:\soboleo\spaces\fzi\storage\";

	private static EventBus eventBus;

	protected static final Logger logger = Logger
			.getLogger(TestOfficeDocuments.class);

	private static String documentUri = "";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		try {
			logger.info("setup eventbus");
			eventBus = new EventBus();
			// set eventbus
			luceneeventbus = new LuceneEventBusAdaptor(new File(
					directoryTestLucene, "index"), new File(directoryStorage),
					eventBus, null);
			eventBus.addListener(luceneeventbus);
		} catch (Exception e) {
			logger.error(e);
		}

	}

	@Test
	public void testAddOfficeDocument() {

		logger.info("add office document testwordx.doc");

		File f = new File("src/test/resources/files/testwordx.docx");

		try {
			FileInputStream fis = new FileInputStream(f);
			logger.info("fis stream : " + fis.available());

			AddOfficeDocument aod = new AddOfficeDocument(DocumentType.doc,
					fis, "testwordx.docx", "testuser");

			luceneeventbus.receiveEvent(aod);

			logger.info("uri of added document : "
					+ luceneeventbus.getLastAddedDocumentKey());

		} catch (Exception e) {
			logger.error(e);
		}

	}

	@Test
	public void searchAllOfficeDocuments() {
		logger.info("search all office documents");

		GetAllOfficeDocuments gaod = new GetAllOfficeDocuments(
				ResultType.OFFICE_DOCUMENT);
		LuceneResult result = (LuceneResult) luceneeventbus.process(gaod);

		logger.info("elements in result: " + result.length());

		try {
			// show results:
			for (int x = 0; x < result.length(); x++) {
				IndexDocument id = (IndexDocument) result.doc(x);

				logger.info("id : " + id.getTitle() + " - uri : " + id.getURI()
						+ " dt : " + id.getURL());
			}
		} catch (Exception e) {
			logger.error(e);
		}

	}

	@Test
	public void searchOfficeDocumentByUri() {
		// searches only the added document from index

		logger.info("search document by uri :"
				+ luceneeventbus.getLastAddedDocumentKey());

		GetOfficeDocumentByUri gu = new GetOfficeDocumentByUri(luceneeventbus
				.getLastAddedDocumentKey());
		LuceneResult result = (LuceneResult) luceneeventbus.process(gu);

		// one result is expected only
		logger.info("result size: " + result.length());

		Assert.assertTrue(result.length() == 1);

		logger.info("search by uri was successful!");

		try {
			// the following infomation is needed by the downloader servlet
			IndexDocument id = (IndexDocument) result.doc(0);

			logger.info("id: " + id.getURL() + " - uri: " + id.getURI());
		} catch (Exception e) {
			logger.error(e);
		}

	}

	@Test
	public void searchAllDocuments() {
		logger.info("search all documents: web and office");

		GetAllDocuments gaod = new GetAllDocuments();
		LuceneResult result = (LuceneResult) luceneeventbus.process(gaod);

		logger.info("elements in result: " + result.length());

		try {
			// show results:
			for (int x = 0; x < result.length(); x++) {
				IndexDocument id = (IndexDocument) result.doc(x);

				logger.info("id : " + id.getTitle() + " - uri : " + id.getURI()
						+ " dt : " + id.getURL());
			}
		} catch (Exception e) {
			logger.error(e);
		}

	}

	@Test
	public void searchDocumentsByQuery() {
		logger.info("search documents by query: testtext");

		GetDocumentsSearchResult gaod = new GetDocumentsSearchResult(
				"testtext", new HashSet<String>());

		SearchResult result = (SearchResult) luceneeventbus.process(gaod);

		logger.info("elements in result: " + result.getLuceneResult().length());

		try {
			// show results:
			for (int x = 0; x < result.getLuceneResult().length(); x++) {
				IndexDocument id = (IndexDocument) result.getLuceneResult().doc(x);

				logger.info("id : " + id.getTitle() + " - uri : " + id.getURI()
						+ " dt : " + id.getURL());
			}
		} catch (Exception e) {
			logger.error(e);
		}

	}

	@Test
	public void loadInputStreamForDocument() {
		logger.info("documentUri: " + luceneeventbus.getLastAddedDocumentKey());

		GetOfficeDocumentInputStream gis = new GetOfficeDocumentInputStream(
				DocumentType.doc.toString(), luceneeventbus
						.getLastAddedDocumentKey());

		InputStream result = (InputStream) luceneeventbus.process(gis);

		Assert.assertTrue(result != null);

		try {
			logger.info("input stream : " + result.available());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// @Test
	public void deleteOfficeDocument() {
		logger.info("documentUri: " + luceneeventbus.getLastAddedDocumentKey());

		RemoveOfficeDocument rod = new RemoveOfficeDocument(luceneeventbus
				.getLastAddedDocumentKey());
		luceneeventbus.receiveEvent(rod);
	}

}
