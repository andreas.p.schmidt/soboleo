package de.fzi.ipe.soboleo.space.logging.client.test;

import org.apache.log4j.Logger;
import org.junit.Test;

import de.fzi.ipe.soboleo.space.logging.client.UserLoggingService;
import de.fzi.ipe.soboleo.space.logging.client.UserLoggingServiceImpl;

public class TestUserLoggingService {

	
	protected static final Logger logger = Logger
	.getLogger(TestUserLoggingService.class);

	@Test
	public void testAddUserEvent()
	{
		UserLoggingService userLoggingService=new UserLoggingServiceImpl();
		boolean success = userLoggingService.addUserEventRequest("FischersFritzFischtFrischeFische", "mailto:braun@fzi.de", "default", "", "browseAllTags", "");
		logger.info("addEvent: " +success);
	}
}
