package de.fzi.ipe.soboleo.data.documentstorage.analyse.test;

import java.util.List;

import org.junit.Test;

import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.data.documentstorage.analyse.AnalyseSearchRequests;

public class TestLoadLogfiles {

	
	
	@Test
	public void loadLogFiles()
	{
		AnalyseSearchRequests lo=new AnalyseSearchRequests("c:/soboleo/spaces", "fzi", 30);
		
	}
	
	@Test
	public void loadConceptStatisticList()
	{
		AnalyseSearchRequests asr=new AnalyseSearchRequests();
		List<ConceptStatistic> statistic=asr.createAndReturnStatistic("c:/soboleo/spaces", "fzi", 30);
		
		for (ConceptStatistic cs:statistic)
		{
			System.out.println("cs : " +cs.getConceptId() + " | " +cs.getCount());
		}
	}
	
}
