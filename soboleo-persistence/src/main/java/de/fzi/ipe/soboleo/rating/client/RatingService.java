package de.fzi.ipe.soboleo.rating.client;

import de.fzi.ipe.soboleo.beans.rating.Rating;

/** this is the interface which is invoked by the client side. 
 * calls the rating service from web service von Grazern
 * @author awalter
 *
 */
public interface RatingService {

	/**
	 * Queries the overall rating of a resource
	 * @param key key of the requesting user
	 * @param uri mailto-uri of the requesting user
	 * @param digitalResourceUri the uri to get the rating fore
	 * @return rating object containing the number of ratings and the rating score
	 */
	public Rating getOverallRating(String key, String mailtoUri, String space, String digitalResourceUri);

	
	/**
	 * Queries the rating by a user for a ressource.
	 * @param maker the marker id of the user, its email uri mailto:<email>
	 * @param key key of the requesting user
	 * @param mailtoUri mailto-uri of the requesting user 
	 * @param digitalResourceUri the uri to get the rating fore
	 * @return rating object containing the number of ratings and the rating score
	 */
	public Rating getUserRating(String key, String mailtoUri, String space, String maker, String digitalResourceUri);
	
	
	/**
	 * Sets the rating by a user for a resource .
	 * @param key key of the requesting user
	 * @param maker the marker id of the user, its email uri mailto:<email>
	 * @param digitalResourceUri the uri to get the rating fore
	 * @param score score to set for the ressource
	 * @return boolean for success of operation
	 */
	public boolean setUserRating(String key, String maker, String space, String digitalResourceUri, int score);
	
	
}
