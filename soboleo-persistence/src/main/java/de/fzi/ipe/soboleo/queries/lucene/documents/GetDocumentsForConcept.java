/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.lucene.documents;

import java.util.Set;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Event to ask the LuceneEventBusAdaptor for the SearchingLuceneWrapper. 
 */
public class GetDocumentsForConcept extends QueryEvent{
	
	private String conceptURI;
	private Set<String> subconceptURIs;
	// private ResultType resultType;
	
	@SuppressWarnings("unused") //needed for GWT
	private GetDocumentsForConcept() {}
	
	public GetDocumentsForConcept(String conceptURI) {
		this.conceptURI = conceptURI;
	}
	
	public String getConceptURI(){
		return conceptURI; 
	}
	
	public Set<String> getSubconceptURIs(){
		return subconceptURIs; 
	}
		
	public void setSubconceptURIs(Set<String> subconceptURIs){
		this.subconceptURIs = subconceptURIs;
	}
}
