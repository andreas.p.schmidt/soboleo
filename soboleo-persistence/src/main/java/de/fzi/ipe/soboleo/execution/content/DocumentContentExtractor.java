/*
 * Code for SOBOLEO project 
 * Created on 21.07.2006 by Guy
 */
package de.fzi.ipe.soboleo.execution.content;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;
import org.apache.poi.hslf.extractor.PowerPointExtractor;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;
import org.htmlparser.Parser;
import org.htmlparser.util.ParserException;
import org.htmlparser.visitors.TextExtractingVisitor;
import org.pdfbox.cos.COSDocument;
import org.pdfbox.pdfparser.PDFParser;
import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;

import de.fzi.ipe.soboleo.beans.document.DocumentType;



public class DocumentContentExtractor{

	public static final long MAX_FILE_SIZE = 3*1024*1024;

	protected static final Logger logger = Logger.getLogger(DocumentContentExtractor.class);
	
	/** this method gets a binary file with its input stream. it tries to extract the 
	 * content based on DocumentType.
	 * If it is not possible to extract content, ioexception is thrown
	 * @param is the input stream to extract.
	 * @param type the document type 
	 * @return the text content
	 * @throws IOException If it is not possible to extract content, ioexception is thrown
	 */
	public static String getExtractContentFromFile(InputStream is, DocumentType type) throws IOException
	{
		String content="";
		
		try
		{
			if (type.equals(DocumentType.pdf))
			{
				logger.info("extract content from pdf file");
				content=getTextFromPDF(is) ;
			}
			
			if (type.equals(DocumentType.doc))
			{
				// cache inputstream
				ContentUtil.cacheInputStream(is);
				try
				{
					InputStream fis=ContentUtil.getCachedInputStream();
					logger.info("try docx extraction");
					logger.info("extract content from word docx file");
					content=getTextFromWordX(fis);
				}
				catch(Exception e)
				{
					InputStream fis=ContentUtil.getCachedInputStream();
					logger.info("extract content from word file");
					content=getTextFromWord(fis);

				}
			}
			
			if (type.equals(DocumentType.xls))
			{
				// cache inputstream
				ContentUtil.cacheInputStream(is);
				try
				{
					InputStream fis=ContentUtil.getCachedInputStream();
					logger.info("try xlsx extraction");
					logger.info("extract content from excel xlsx file");
					content=getTextFromExcelX(fis);
				}
				catch(Exception e)
				{
					InputStream fis=ContentUtil.getCachedInputStream();
					logger.info("extract content from excel xls file");
					content=getTextFromExcel(fis);

				}
			}
				
			if (type.equals(DocumentType.ppt))
			{
				// cache inputstream
				ContentUtil.cacheInputStream(is);
				try
				{
					InputStream fis=ContentUtil.getCachedInputStream();
					logger.info("try pptx extraction");
					logger.info("extract content from powerpoint pptx file");
					content=getTextFromPowerPointX(fis);
				}
				catch(Exception e)
				{
					InputStream fis=ContentUtil.getCachedInputStream();
					logger.info("extract content from powerpoint ppt file");
					content=getTextFromPowerPoint(fis);

				}
			}
			
			
			return content;
		} catch(Exception pe)
		{
			throw new IOException(pe);
		}
	}
	
	
	
	/** this method downloads a document at its given url. based on the content type,
	 * it tries to extract the content.
	 * 
	 * @param url url of document
	 * @return the text content
	 * @throws IOException if url is invalid
	 */
	public static String getDownloadDocumentAndExtractContent(URL url) throws IOException
	{
		String content = "";
		URLConnection urlCon = url.openConnection();
		InputStream is = null;
		try {	
			if(urlCon.getContentLength() > MAX_FILE_SIZE) {
				throw new IOException("Content too big");
			}
			is = urlCon.getInputStream();
			String contentType = urlCon.getContentType();
			
			if (contentType.startsWith("text/html")) {
				content = getTextFromHTML(is) ;
			}
			else if (contentType.startsWith("application/pdf")) {
				content = getTextFromPDF(is) ;
			}
			else if (contentType.startsWith("application/msword") || contentType.startsWith("application/vnd.ms-word")) {
				content = getTextFromWord(is) ;
			}
			else if (contentType.startsWith("application/mspowerpoint") || contentType.startsWith("application/vnd.ms-powerpoint")) {
				content = getTextFromPowerPoint(is) ;		
			}
			else if (contentType.startsWith("application/msexcel") || contentType.startsWith("application/vnd.ms-excel")) {
				content = getTextFromExcel(is) ;				
			}
			else if (contentType.startsWith("application/vnd.openxmlformats-officedocument.spreadsheetml")) {
				content = getTextFromExcelX(is) ;				
			}
			else if (contentType.startsWith("application/vnd.openxmlformats-officedocument.presentationml")) {
				content = getTextFromPowerPointX(is) ;				
			}
			else if (contentType.startsWith("application/vnd.openxmlformats-officedocument.wordprocessingml")) {
				content = getTextFromWordX(is) ;				
			}
			else if (contentType.startsWith("application/octet-stream") || contentType.startsWith("text/plain")){
				String path = urlCon.getURL().getPath();
				if (path.endsWith(".xls")) content = getTextFromExcel(is);
				else if (path.endsWith(".doc")) content = getTextFromWord(is);
				else if (path.endsWith(".ppt")) content = getTextFromPowerPoint(is);
				else if (path.endsWith(".pptx")) content = getTextFromPowerPointX(is);
				else if (path.endsWith(".xlsx")) content = getTextFromExcelX(is);
				else if (path.endsWith(".docx")) content = getTextFromWordX(is);
				else if (path.endsWith(".pdf")) content = getTextFromPDF(is);
				else throw new IOException("Unknown File Type at "+path +", mime type was "+contentType);
			}
			else throw new IOException("Unknown MIME Type " +contentType + ", for "+url);
			return content ;
		}catch (XmlException xme) {
			throw new IOException(xme);
		}catch (ParserException pe) {
			throw new IOException(pe);
		} catch (OpenXML4JException e) {
			throw new IOException(e);
		} finally {
			if (is!=null) is.close();
		}
	}
	
	
	// this are the methods called to extract documents
	// (aw changed them to private)
	
	private static String getTextFromHTML(InputStream is)  throws ParserException,IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

		StringBuilder content = new StringBuilder();
		String line = reader.readLine();
		while (line != null) {
			content.append(line);
			line = reader.readLine();
		}
		
		Parser p = new Parser();
		p.setInputHTML(content.toString());
		TextExtractingVisitor visitor = new TextExtractingVisitor(); 
		p.visitAllNodesWith(visitor); 
		String textInPage = visitor.getExtractedText();
		textInPage = textInPage.replaceAll("<", "&lt;"); //escape html - sadly necessary
		return textInPage;
	}

	
	private  static String getTextFromPDF(InputStream is) throws IOException {

		
		PDFParser parser = new PDFParser(is);  
		parser.parse();  
		COSDocument cd = parser.getDocument();  
		PDFTextStripper stripper = new PDFTextStripper();  
		String text = stripper.getText(new PDDocument(cd));  
		return text;
		
		/*
		PDDocument pddoc = PDDocument.load(is) ;
		
		logger.info("pages pddoc: " +pddoc.getNumberOfPages());
		
		try {
			PDFTextStripper pdfText = new PDFTextStripper();
			return pdfText.getText(pddoc);			
		} finally {
			pddoc.close();
		}
		 */

	}

	
	private static String getTextFromWord(InputStream is)throws IOException {
		WordExtractor documentExtractor = null ;
		HWPFDocument wordReader = new HWPFDocument(is) ;
		documentExtractor = new WordExtractor(wordReader) ;
		return documentExtractor.getText();
	}
	

	
	private static String getTextFromPowerPoint(InputStream is)throws IOException {
		PowerPointExtractor documentExtractor = new PowerPointExtractor(is) ;
		return documentExtractor.getText();
	}
	
	private static String getTextFromExcel(InputStream is)throws IOException {
		POIFSFileSystem poiFileSystem = new POIFSFileSystem(is) ;
		ExcelExtractor documentExtractor = new ExcelExtractor(poiFileSystem) ;
		return documentExtractor.getText(); 	
	}
	
	private static String getTextFromWordX(InputStream is) throws XmlException,OpenXML4JException,IOException, InvalidFormatException{
		OPCPackage pack = OPCPackage.open(is);
		XWPFDocument doc = new XWPFDocument(pack);
		XWPFWordExtractor extractor = new XWPFWordExtractor(doc) ;
		
		String text= extractor.getText() ;
		pack.close();
		return text;
	}
	
	private static String getTextFromExcelX(InputStream is) throws XmlException,OpenXML4JException,IOException {
		OPCPackage pack = OPCPackage.open(is);
		XSSFExcelExtractor extractor = new  XSSFExcelExtractor(pack);	
		return extractor.getText() ;
	}
	
	private static String getTextFromPowerPointX(InputStream is) throws XmlException,OpenXML4JException,IOException {
		OPCPackage pack = OPCPackage.open(is);
		XSLFPowerPointExtractor extractor = new  XSLFPowerPointExtractor(pack);
		return extractor.getText() ;		
	}
	

}






