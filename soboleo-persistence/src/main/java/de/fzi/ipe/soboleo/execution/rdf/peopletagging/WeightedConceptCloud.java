/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.peopletagging;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;


/**
 * Represents the weighted cloud of all concepts associated 
 * for example a query or a person.
 */
public class WeightedConceptCloud {
	
	private Map<String,Float> contents = new HashMap<String,Float>();
	private TaggedPerson taggedPerson; 
	
	public WeightedConceptCloud(TaggedPerson taggedPerson) {
		this.taggedPerson = taggedPerson;
	}

	public WeightedConceptCloud() {
		this.taggedPerson = null;
	}
	
	public TaggedPerson getTaggedPerson() {
		return taggedPerson;
	}
	
	public void addConcept(String conceptURI, float weight) {
		Float oldValue = contents.get(conceptURI);
		if (oldValue == null) oldValue = new Float(0);
		contents.put(conceptURI, oldValue+weight);
	}
	
	public Set<String> getConcepts() {
		return contents.keySet();
	}
	
	public Float getValue(String conceptURI) {
		return contents.get(conceptURI);
	}
	
	
	/**
	 * Compares two WeightedConceptClouds. A high value represents a good 
	 * match between the clouds. 
	 * @param otherCloud
	 * @return
	 */
	public float compare(WeightedConceptCloud otherCloud) {
		float sum = 0;
		for (String conceptURI: contents.keySet()) {
			Float weight = contents.get(conceptURI);
			Float otherWeight = otherCloud.contents.get(conceptURI);
			if (otherWeight != null) {
				sum += (weight+otherWeight)/2;
			}
		}
		return sum;
	}
	
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (String key:contents.keySet()) {
			builder.append(key+"=>"+contents.get(key)+"\n");
		}
		return builder.toString();
	}

}
