/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.webdocument;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Retrieves the PersonTags
 */
public class GetAggregatedWebDocumentTags extends QueryEvent{

	private String webdocURI;
	private String proxyURL;
	private String makerURI;
	
	public GetAggregatedWebDocumentTags(String resourceURI, String makerURI, String proxyURL) {
		this.webdocURI = resourceURI;
		this.proxyURL = proxyURL;
		this.makerURI = makerURI;
	}
	
	@SuppressWarnings("unused")
	private GetAggregatedWebDocumentTags() {; }
	
	public String getWebDocumentURI() {
		return webdocURI;
	}
	
	public String getPageURL(){
		return proxyURL;
	}
	
	public String getMakerURI(){
		return makerURI;
	}
}
