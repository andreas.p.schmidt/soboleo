/*
 * FZI - Information Process Engineering 
 * Created on 23.01.2009 by zach
 */
package de.fzi.ipe.soboleo.data.tripleStore.sesame;

import java.io.File;
import java.io.IOException;

import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStoreFactory;

public class SesameTripleStoreFactory implements TripleStoreFactory{
	
	
	@Override
	public TripleStore getTripleStore(File taxonomyDirectory, String datastoreName) throws IOException {
		return new SesameTripleStore(taxonomyDirectory, datastoreName);
	}
	

	
}
