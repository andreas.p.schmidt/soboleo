/*
 * FZI - Information Process Engineering 
 * Created on 24.09.2009 by zach
 */
package de.fzi.ipe.soboleo.data.tripleStore.tools;

import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.rdfxml.RDFXMLWriter;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;


public class Exporter {

	/**
	 * Creates a complete RDF export from the triple store. 
	 */
	public static String exportRDF(RepositoryConnection conInput) throws RepositoryException, RDFHandlerException {
		StringWriter stringWriter = new StringWriter();
		RDFXMLWriter writer = new RDFXMLWriter(stringWriter);
		writer.startRDF();
		
		RepositoryResult<Statement> statements = conInput.getStatements(null, null, null, false);
		while (statements.hasNext()) writer.handleStatement(statements.next());
		writer.endRDF();
		return stringWriter.toString();
	}
	
	/**
	 * Creates a complete RDF export from the triple store. 
	 */
	public static String exportRDFwithContexts(RepositoryConnection conInput) throws RepositoryException, RDFHandlerException {
		StringWriter stringWriter = new StringWriter();
		RDFXMLWriter writer = new RDFXMLWriter(stringWriter);
		writer.startRDF();
		
		RepositoryResult<Statement> statements = conInput.getStatements(null, null, null, true);
		while (statements.hasNext()) writer.handleStatement(statements.next());
		writer.endRDF();
		return stringWriter.toString();
	}
	
	/**
	 * A simple method that creates an SKOS export from a triple store. This is done
	 * by iterating over all triples in the store and including all those where at least
	 * one of s,p,o is an SKOS URI. 
	 * @throws RDFHandlerException 
	 */
	public static String exportSKOS(RepositoryConnection conInput) throws RepositoryException, RDFHandlerException {
		StringWriter stringWriter = new StringWriter();
		RDFXMLWriter writer = new RDFXMLWriter(stringWriter);
		writer.startRDF();
		
		Set<String> skosURIs = new HashSet<String>();
		for (SKOS skos: SKOS.values()) skosURIs.add(skos.toString());

		RepositoryResult<Statement> statements = conInput.getStatements(null, null, null, false);
		while (statements.hasNext()) {
			Statement currentStatement = statements.next();
			if (skosURIs.contains(currentStatement.getSubject().toString())) writer.handleStatement(currentStatement);
			else if (skosURIs.contains(currentStatement.getPredicate().toString())) writer.handleStatement(currentStatement);
			else if (skosURIs.contains(currentStatement.getObject().toString())) writer.handleStatement(currentStatement);
		}
		
		writer.endRDF();
		return stringWriter.toString();
	}
		
}
