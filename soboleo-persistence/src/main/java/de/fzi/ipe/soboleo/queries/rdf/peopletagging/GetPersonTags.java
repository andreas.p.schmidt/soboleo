/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.peopletagging;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Retrieves the PersonTags
 */
public class GetPersonTags extends QueryEvent{

	private String taggedPersonURI;
	private String conceptURI;
	private String makerURI;
	
	public GetPersonTags(String resourceURI, String conceptURI, String makerURI) {
		this.taggedPersonURI = resourceURI;
		this.conceptURI = conceptURI;
		this.makerURI = makerURI;
	}
	
	@SuppressWarnings("unused")
	private GetPersonTags() {; }
	
	public String getTaggedPersonURI() {
		return taggedPersonURI;
	}
	
	public String getConceptURI(){
		return conceptURI;
	}
	
	public String getMakerURI(){
		return makerURI;
	}
}
