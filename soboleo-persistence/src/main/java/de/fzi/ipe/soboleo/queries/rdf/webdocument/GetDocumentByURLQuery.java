/*
 * FZI - Information Process Engineering 
 * Created on 06.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.webdocument;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;


/**
 * Query Event used to retrieve a document by document URL
 */
public class GetDocumentByURLQuery extends QueryEvent {

	private String url;
	
	@SuppressWarnings("unused") //needed for GWT
	private GetDocumentByURLQuery() {}
	
	public GetDocumentByURLQuery(String url) {
		this.url = url;
	}
	
	
	public String getURL() {
		return url;
	}
	
}
