/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.peopletagging;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;

import de.fzi.ipe.soboleo.beans.ontology.FOAF;
import de.fzi.ipe.soboleo.beans.ontology.RDF;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.ManualPersonTag;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

public class RDFManualPersonTag extends RDFPersonTag{

	public RDFManualPersonTag(URI annotationID, TripleStore tripleStore) {
		super(annotationID, tripleStore);
	}
	
	public void createTag(URI personID) throws IOException {
		super.createTag(personID);
		URI tagClassId = vf.createURI(SoboleoNS.PEOPLE_MANUAL_PERSON_TAG.toString());
		URI rdfType = vf.createURI(RDF.TYPE.toString());
		tripleStore.addTriple(tagURI, rdfType, tagClassId, null);
		tripleStore.setTimestamp(tagURI, vf.createURI(SoboleoNS.PEOPLE_PERSON_TAG_DATE.toString()), null);
	}

	public void setUserId(String userId) throws IOException {
		URI userID = vf.createURI(userId);
		URI pred = vf.createURI(FOAF.MAKER.toString());
		tripleStore.removeTriples(tagURI, pred, null, null);
		tripleStore.addTriple(tagURI, pred, userID, null);
	}
	
	public String getUserId() throws IOException {
		URI pred = vf.createURI(FOAF.MAKER.toString());
		List<Statement> statements = tripleStore.getStatementList(tagURI,pred,null);
		if (statements.size() ==0) return null;
		else return ((URI)statements.get(0).getObject()).toString();
	}	
	
	public void setURL(String urlString) throws IOException {
		Literal url = vf.createLiteral(urlString);
		URI hasURL = vf.createURI(SoboleoNS.PEOPLE_PERSON_TAG_URL.toString());
		tripleStore.removeTriples(tagURI, hasURL, null, null);
		tripleStore.addTriple(tagURI,hasURL,url,null);
	}
	
	public String getURL() throws IOException {
		URI hasURL = vf.createURI(SoboleoNS.PEOPLE_PERSON_TAG_URL.toString());
		List<Statement> statements = tripleStore.getStatementList(tagURI, hasURL, null);
		if (statements.size() == 0) return null;
		else return ((Literal)statements.get(0).getObject()).stringValue();
	}
	
	/** a slightly optimized method to create a tag object - a copy of the data
	 * in the tripleStore about a tag. It works with just one call to the triple store -
	 * only a fourth of what would be needed if the other methods where to be used. 
	 */
	public ManualPersonTag getClientSideTag() throws IOException {
		String userID = null, conceptID = null, pageURL=null;
		Calendar cal = null;
		List<Statement> statements = tripleStore.getStatementList(tagURI, null, null);
		for (Statement s:statements) {
			if (s.getPredicate().toString().equals(FOAF.MAKER.toString())) userID = s.getObject().toString();
			else if (s.getPredicate().toString().equals(SoboleoNS.PEOPLE_PERSON_TAG_MEANS.toString())) conceptID = s.getObject().toString();
			else if (s.getPredicate().toString().equals(SoboleoNS.PEOPLE_PERSON_TAG_URL.toString())) pageURL = ((Literal)s.getObject()).toString();
			else if (s.getPredicate().toString().equals(SoboleoNS.PEOPLE_PERSON_TAG_DATE.toString())) {
				Literal l = (Literal) s.getObject();
				cal = l.calendarValue().toGregorianCalendar();
			}
		}
		return new ManualPersonTag(tagURI.toString(),userID,conceptID,pageURL,cal.getTime());
	}
	

	
}
