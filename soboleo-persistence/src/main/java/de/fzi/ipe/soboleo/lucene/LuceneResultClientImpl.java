/*
 * FZI - Information Process Engineering 
 * Created on 27.08.2009 by zach
 */
package de.fzi.ipe.soboleo.lucene;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;




/**
 * An implementation of the LuceneResult interface that stores all its information
 * and does not rely on access to the lucene index. 
 */
public class LuceneResultClientImpl implements LuceneResult{

	
	private List<IndexDocWrapper> indexDocs;
	
	private static class IndexDocWrapper {
		IndexDocument indexDocument;
		String text,highlightText;
		float score;
		ResultType resultType;
		
		IndexDocWrapper(IndexDocument indexDoc, String text, String highlighText, float score, ResultType resultType) {
			this.indexDocument = indexDoc;
			this.text = text;
			this.highlightText = highlighText;
			this.score = score;
			this.resultType = resultType;
		}
	}
	
	private class ResultIterator implements Iterator<IndexDocument> {
		
		int currentID = 0;

		@Override
		public boolean hasNext() {
			return (length() > currentID);
		}

		@Override
		public IndexDocument next() {
			currentID ++;
			return doc(currentID-1);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
	
	public LuceneResultClientImpl(LuceneResult baseResult) throws IOException {
		indexDocs = new ArrayList<IndexDocWrapper>(baseResult.length());
		for (int i=0;i<baseResult.length();i++) {
			IndexDocument doc = baseResult.doc(i);
			String text = baseResult.getText(i);
			String highlightText = baseResult.highlightText(i);
			float score = baseResult.score(i);
			ResultType resultType = baseResult.getType(i);
			indexDocs.add(new IndexDocWrapper(doc,text,highlightText,score,resultType));
		}
	}
	
	
	@Override
	public IndexDocument doc(int n)  {
		return indexDocs.get(n).indexDocument;
	}


	@Override
	public String getText(int i) {
		return indexDocs.get(i).text;
	}

	@Override
	public String highlightText(int i) {
		return indexDocs.get(i).highlightText;
	}



	@Override
	public Iterator<IndexDocument> iterator() {
		return new ResultIterator();
	}

	@Override
	public int length() {
		return indexDocs.size();
	}

	@Override
	public int getLength() {
		return indexDocs.size();
	}

	@Override
	public float score(int n) throws IOException {
		return indexDocs.get(n).score;
	}


	@Override
	public ResultType getType(int i) throws IOException {
		return indexDocs.get(i).resultType;
	}

	@Override
	public Iterator<IndexDocument> getIterator()
	{ return iterator(); }
	
	
}
