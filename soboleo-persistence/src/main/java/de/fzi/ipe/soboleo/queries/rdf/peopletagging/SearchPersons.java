/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.peopletagging;

import java.util.Set;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;


/**
 * Searches for peoples based on the string given. 
 * The string is searched for concept references and these
 * are then used to find persons. Returns a sorted list
 * of ClientSide.TaggedPerson objects, the best matching 
 * ones are returned first. 
 */
public class SearchPersons extends QueryEvent{

	private String searchString;
	private String askerURI;
	private Set<String> extractedConceptURIs;
	
	public SearchPersons(String searchString) {
		this.searchString = searchString;
	}
	
	public SearchPersons(String searchString, String askerURI){
		this.searchString = searchString;
		this.askerURI = askerURI;
	}
	
	@SuppressWarnings("unused")
	private SearchPersons() {;}
	
	public String getSearchString() {
		return searchString;
	}
	
	public String getAskerURI(){
		return askerURI;
	}
	
	public Set<String> getExtractedConceptURIs(){
		return extractedConceptURIs;
	}
	
	public void setExtractedConceptURIs(Set<String> extractedConceptURIs){
		this.extractedConceptURIs = extractedConceptURIs;
	}
	
}
