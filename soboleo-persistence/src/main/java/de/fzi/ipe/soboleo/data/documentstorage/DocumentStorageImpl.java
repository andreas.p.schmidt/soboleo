package de.fzi.ipe.soboleo.data.documentstorage;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;

import de.fzi.ipe.soboleo.beans.document.DocumentType;

public class DocumentStorageImpl implements DocumentStorage {

	protected static final Logger logger = Logger
			.getLogger(DocumentStorageImpl.class);

	private String storagePath;

	public boolean initializeStorage(String storagePath) {

		this.storagePath = storagePath;

		// try if storage path exists
		File sp = new File(storagePath);

		// absolute path:
		logger.info("absolute path: " + sp.getAbsolutePath());

		if (sp.exists()) {
			// return true if directory is writeable
			if (sp.canWrite()) {
				return true;
			}
		} else {

			return sp.mkdir();

		}
		return false;
	}

	public boolean addDocument(String documentKey, DocumentType dt,
			InputStream is) {

		logger.info("add document : " + documentKey + " - " + dt);

		String targetDirectory = storagePath + File.separator + dt;
		File directory = new File(targetDirectory);
		if (!directory.exists()) {
			directory.mkdir();
		}

		try 
		{

			final byte[] thumb = new byte[is.available()];
			is.read(thumb);

			final DataOutputStream os = new DataOutputStream(
					new FileOutputStream(directory.getAbsoluteFile()
							+ File.separator + documentKey));
			os.write(thumb);
			os.close();

			return true;
		} catch (Exception e) {
			logger.error(e);
		}

		return false;
	}

	@Override
	public InputStream loadDocument(String documentKey, String dt) {

		String targetDirectory = storagePath + File.separator + dt;

		try {
			InputStream in = new FileInputStream(new File(targetDirectory
					+ File.separator + documentKey));
			return in;
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	@Override
	public boolean removeDocument(String documentKey, String dt) {
		String targetDirectory = storagePath + File.separator + dt;

		File f = new File(targetDirectory + File.separator + documentKey);
		logger.info("remove file: " + f.getAbsolutePath());
		boolean status = f.delete();

		return status;
	}

}
