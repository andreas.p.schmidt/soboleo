package de.fzi.ipe.soboleo.rating.client;

import java.util.Date;

import org.apache.log4j.Logger;

import at.tug.mature.clientws.MatureClientWSUtils;
import at.tug.mature.clientws.cn.entities.InvalidKeyException;
//import at.tug.mature.clientws.cn.InvalidKeyException;
//import at.tug.mature.clientws.cn.WebserviceAccessServiceStub.GetUserRatings;
//import at.tug.mature.clientws.cn.WebserviceAccessServiceStub.GetUserRatingsResponse;
//import at.tug.mature.clientws.cn.WebserviceAccessServiceStub.RatingAssignment;
import de.fzi.ipe.soboleo.beans.rating.Rating;


public class RatingServiceImpl implements RatingService {


	static Logger logger = Logger.getLogger(RatingServiceImpl.class);

	
	public boolean setUserRating(String key, String mailtoUri, String space, String digitalResourceUri, int score)
	{
		
		try {
			logger.info("call setUserRating for uri : " +key +" " + mailtoUri + " " + digitalResourceUri + " " +score);
			System.out.println("call setUserRating for uri : " + digitalResourceUri);
			// instance of the service
			if(!space.equals(MatureClientWSUtils.KENT_SPACE)){
				logger.info("log for CN");
				System.out.println("log for CN");
				at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy client = new at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy();
				logger.info("log for CN");
				System.out.println("log for CN");
				try{
					boolean success = client.setUserRating(mailtoUri, score, digitalResourceUri, MatureClientWSUtils.UNIVERSAL_KEY);
					logger.info("status for " + digitalResourceUri + ": " + success);
					System.out.println("status for " + digitalResourceUri + ": " + success);
					return success;
				} catch (InvalidKeyException e) {
					logger.error("invalid key : " + e);
					System.out.println("invalid key : " + e);
				}
			}
			else {
				at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy client = new at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy();
				logger.info("log for Kent");
				try{
					boolean success = client.setUserRating(mailtoUri, score, digitalResourceUri, MatureClientWSUtils.UNIVERSAL_KEY);
					logger.info("status for " + digitalResourceUri + ": " + success);
					System.out.println("status for " + digitalResourceUri + ": " + success);
					return success;
				} catch (at.tug.mature.clientws.kent.entities.InvalidKeyException e) {
					logger.error("invalid key : " + e);
					System.out.println(e);
				}

			}
		} catch (Exception e) {
			logger.error(e);
			System.out.println( e);
		}
		return false;
	}
	
	public Rating getUserRating(String key, String mailtoUri, String space, String maker, String digitalResourceUri)
	{
		try {
			logger.info("call getUserRating for uri : " +key +" " +mailtoUri +" " +space + " " +maker +" " + digitalResourceUri);
			System.out.println("call getUserRating for uri : " +key +" " +mailtoUri +" " +space + " " +maker +" " + digitalResourceUri);
			// instance of the service
			if(!space.equals(MatureClientWSUtils.KENT_SPACE)){
				logger.info("log for CN");
				System.out.println("log for CN");
//				at.tug.mature.clientws.cn.WebserviceAccessServiceStub client = new at.tug.mature.clientws.cn.WebserviceAccessServiceStub();
//				GetUserRatings getUserRatings = new GetUserRatings();
//				getUserRatings.setAgentKey(MatureClientWSUtils.UNIVERSAL_KEY);
//				getUserRatings.setAgentURI(mailtoUri);
//				getUserRatings.setMakerURI(maker);
//				getUserRatings.setDigitalResourceURI(digitalResourceUri);
//				try{
//					GetUserRatingsResponse response = client.getUserRatings(getUserRatings);
//					if(response != null) {
//						RatingAssignment[] ratings = response.getGetUserRatingsReturn();
//						logger.info("return response score: " + ratings[0].getRatingScore()
//								+ " , " + ratings[0].getRatingDate());
//						System.out.println("return response score: " + ratings[0].getRatingScore()
//								+ " , " + ratings[0].getRatingDate());
//						return new Rating(ratings[0].getRatingScore(), new Date(ratings[0].getRatingDate()));
//					}
//				}catch (InvalidKeyException e){
//					logger.error("invalid key : " + e);
//					System.out.println("error invalid key" +e);
//				}
				at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy client = new at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy();
				// create the request
				try {
					at.tug.mature.clientws.cn.entities.RatingAssignment[] response = client.getUserRatings(maker, digitalResourceUri, mailtoUri, MatureClientWSUtils.UNIVERSAL_KEY);
					if (response != null && response.length>0){
						at.tug.mature.clientws.cn.entities.RatingAssignment rating = response[0];
						logger.info("return response score: " + rating.getRatingScore()
								+ " , " + rating.getRatingDate());
						System.out.println("return response score: " + rating.getRatingScore()
								+ " , " + rating.getRatingDate());
						return new Rating(rating.getRatingScore(), new Date(rating.getRatingDate()));
					}
				} catch (at.tug.mature.clientws.cn.entities.InvalidKeyException e) {
					logger.error("invalid key : " + e);
					System.out.println("error invalid key" +e);
				}
			} else {
				logger.info("log for Kent");
				at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy client = new at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy();
				// create the request
				try {
					at.tug.mature.clientws.kent.entities.RatingAssignment[] response = client.getUserRatings(maker, digitalResourceUri, mailtoUri, MatureClientWSUtils.UNIVERSAL_KEY);
					if (response != null && response.length>0){
						at.tug.mature.clientws.kent.entities.RatingAssignment rating = response[0];
						logger.info("return response score: " + rating.getRatingScore()
								+ " , " + rating.getRatingDate());
						return new Rating(rating.getRatingScore(), new Date(rating.getRatingDate()));
					}
				} catch (at.tug.mature.clientws.kent.entities.InvalidKeyException e) {
					logger.error("invalid key : " + e);
				}

			}
		} catch (Exception e) {
			logger.error("error in calling the rating webservice : " +e);
			System.out.println("error getUserRating" +e);
		}
		return new Rating(0, new Date());
	}
	
	public Rating getOverallRating(String key, String mailtoUri, String space, String digitalResourceUri)
	{

		try {
			logger.info("call overallRating for uri : " + digitalResourceUri);
			System.out.println("call overallRating for uri : " +key +" " +mailtoUri +" " + digitalResourceUri);
			// instance of the service
			if(!space.equals(MatureClientWSUtils.KENT_SPACE)){
				logger.info("log for CN");
				at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy client = new at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy();
				// create the request
				try {
					logger.info("start service request");
					at.tug.mature.clientws.cn.entities.OverallRatingEntry entry = client.getOverallRating("", digitalResourceUri, mailtoUri, MatureClientWSUtils.UNIVERSAL_KEY);
					logger.info("return response freq: " + entry.getRatinFreq()
							+ " , " + entry.getRatingScore());
					return new Rating(entry.getRatinFreq(), (int)entry.getRatingScore());
	
				} catch (at.tug.mature.clientws.cn.entities.InvalidKeyException e) {
					logger.error("invalid key : " + e);
				}
			}else {
				logger.info("log for Kent");
				at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy client = new at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy();
				// create the request
				try {
					logger.info("start service request");
					at.tug.mature.clientws.kent.entities.OverallRatingEntry entry = client.getOverallRating("", digitalResourceUri, mailtoUri, MatureClientWSUtils.UNIVERSAL_KEY);
					logger.info("return response freq: " + entry.getRatinFreq()
							+ " , " + entry.getRatingScore());
					return new Rating(entry.getRatinFreq(), (int)entry.getRatingScore());
	
				} catch (at.tug.mature.clientws.kent.entities.InvalidKeyException e) {
					logger.error("invalid key : " + e);
				}
			}
		} catch (Exception e) {
			logger.error("error in calling the rating webservice : " +e);
			System.out.println("error get overall rating" +e);
		}
		return new Rating(0,0);
	}
	
}
