/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.skos;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Event to ask the SkosEventBusAdaptor for a Set of ClientSideConcepts that have an appropriate label or description. 
 */
public class GetConceptsForLabel extends QueryEvent{
	
	private String label;
	private Language lang;
	private SKOS property;
	
	public GetConceptsForLabel(String label, Language lang, SKOS property) {
		this.label = label;
		this.lang = lang;
		this.property = property;
	}
	
	public GetConceptsForLabel(String label, Language lang) {
		this.label = label;
		this.lang = lang;
	}
	
	public GetConceptsForLabel(String label) {
		this.label = label;
	}
	
	@SuppressWarnings("unused")
	private GetConceptsForLabel() {;}
	
	public String getLabel(){
		return label;
	}
	
	public Language getLanguage(){
		return lang;
	}
	
	public SKOS getProperty(){
		return property;
	}
}
