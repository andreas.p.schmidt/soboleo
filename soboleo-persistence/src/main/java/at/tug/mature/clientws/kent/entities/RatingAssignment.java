/**
 * RatingAssignment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.tug.mature.clientws.kent.entities;

public class RatingAssignment  implements java.io.Serializable {
    private java.lang.String digitalResourceURI;

    private java.lang.String makerURI;

    private long ratingDate;

    private int ratingScore;

    public RatingAssignment() {
    }

    public RatingAssignment(
           java.lang.String digitalResourceURI,
           java.lang.String makerURI,
           long ratingDate,
           int ratingScore) {
           this.digitalResourceURI = digitalResourceURI;
           this.makerURI = makerURI;
           this.ratingDate = ratingDate;
           this.ratingScore = ratingScore;
    }


    /**
     * Gets the digitalResourceURI value for this RatingAssignment.
     * 
     * @return digitalResourceURI
     */
    public java.lang.String getDigitalResourceURI() {
        return digitalResourceURI;
    }


    /**
     * Sets the digitalResourceURI value for this RatingAssignment.
     * 
     * @param digitalResourceURI
     */
    public void setDigitalResourceURI(java.lang.String digitalResourceURI) {
        this.digitalResourceURI = digitalResourceURI;
    }


    /**
     * Gets the makerURI value for this RatingAssignment.
     * 
     * @return makerURI
     */
    public java.lang.String getMakerURI() {
        return makerURI;
    }


    /**
     * Sets the makerURI value for this RatingAssignment.
     * 
     * @param makerURI
     */
    public void setMakerURI(java.lang.String makerURI) {
        this.makerURI = makerURI;
    }


    /**
     * Gets the ratingDate value for this RatingAssignment.
     * 
     * @return ratingDate
     */
    public long getRatingDate() {
        return ratingDate;
    }


    /**
     * Sets the ratingDate value for this RatingAssignment.
     * 
     * @param ratingDate
     */
    public void setRatingDate(long ratingDate) {
        this.ratingDate = ratingDate;
    }


    /**
     * Gets the ratingScore value for this RatingAssignment.
     * 
     * @return ratingScore
     */
    public int getRatingScore() {
        return ratingScore;
    }


    /**
     * Sets the ratingScore value for this RatingAssignment.
     * 
     * @param ratingScore
     */
    public void setRatingScore(int ratingScore) {
        this.ratingScore = ratingScore;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RatingAssignment)) return false;
        RatingAssignment other = (RatingAssignment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.digitalResourceURI==null && other.getDigitalResourceURI()==null) || 
             (this.digitalResourceURI!=null &&
              this.digitalResourceURI.equals(other.getDigitalResourceURI()))) &&
            ((this.makerURI==null && other.getMakerURI()==null) || 
             (this.makerURI!=null &&
              this.makerURI.equals(other.getMakerURI()))) &&
            this.ratingDate == other.getRatingDate() &&
            this.ratingScore == other.getRatingScore();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDigitalResourceURI() != null) {
            _hashCode += getDigitalResourceURI().hashCode();
        }
        if (getMakerURI() != null) {
            _hashCode += getMakerURI().hashCode();
        }
        _hashCode += new Long(getRatingDate()).hashCode();
        _hashCode += getRatingScore();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RatingAssignment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://entities.mature.tug.at", "RatingAssignment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("digitalResourceURI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.mature.tug.at", "digitalResourceURI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("makerURI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.mature.tug.at", "makerURI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ratingDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.mature.tug.at", "ratingDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ratingScore");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.mature.tug.at", "ratingScore"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
