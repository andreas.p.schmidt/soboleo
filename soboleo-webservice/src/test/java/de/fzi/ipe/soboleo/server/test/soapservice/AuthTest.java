package de.fzi.ipe.soboleo.server.test.soapservice;

import org.junit.Test;

import de.fzi.ipe.soboleo.server.soapwebservice.AuthenticateRequest;
import de.fzi.ipe.soboleo.server.soapwebservice.AuthenticateResponse;
import de.fzi.ipe.soboleo.server.soapwebservice.AuthentificationService;
import de.fzi.ipe.soboleo.server.soapwebservice.AuthentificationServiceImpl;
import de.fzi.ipe.soboleo.server.soapwebservice.InvalidLoginException;

public class AuthTest {

	
	
	@Test
	public void testLogin()
	{
		AuthentificationService auth= new AuthentificationServiceImpl();
		
		AuthenticateRequest parameters=new AuthenticateRequest();
		parameters.setUserEmail("awalter@fzi.de");
		parameters.setPassword("anna");
		
		try {
			AuthenticateResponse response=auth.authenticate(parameters);
			System.out.println("response: " +response.getKey());
		} catch (InvalidLoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
