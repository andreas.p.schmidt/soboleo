
package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="proxyURL" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tagAssignmentList" type="{http://mature-ip.eu/MATURE/types}tagAssignment" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ratingAssignmentList" type="{http://mature-ip.eu/MATURE/types}ratingAssignment" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="lastMetric" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userURI",
    "name",
    "proxyURL",
    "tagAssignmentList",
    "ratingAssignmentList",
    "lastMetric"
})
@XmlRootElement(name = "getUserModelResponse", namespace = "http://mature-ip.eu/MATURE")
public class GetUserModelResponse {

    @XmlElement(required = true)
    protected String userURI;
    @XmlElement(required = true)
    protected String name;
    protected List<String> proxyURL;
    protected List<TagAssignment> tagAssignmentList;
    protected List<RatingAssignment> ratingAssignmentList;
    @XmlElement(required = true)
    protected String lastMetric;

    /**
     * Gets the value of the userURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserURI() {
        return userURI;
    }

    /**
     * Sets the value of the userURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserURI(String value) {
        this.userURI = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the proxyURL property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the proxyURL property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProxyURL().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getProxyURL() {
        if (proxyURL == null) {
            proxyURL = new ArrayList<String>();
        }
        return this.proxyURL;
    }

    /**
     * Gets the value of the tagAssignmentList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tagAssignmentList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTagAssignmentList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TagAssignment }
     * 
     * 
     */
    public List<TagAssignment> getTagAssignmentList() {
        if (tagAssignmentList == null) {
            tagAssignmentList = new ArrayList<TagAssignment>();
        }
        return this.tagAssignmentList;
    }

    /**
     * Gets the value of the ratingAssignmentList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ratingAssignmentList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRatingAssignmentList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RatingAssignment }
     * 
     * 
     */
    public List<RatingAssignment> getRatingAssignmentList() {
        if (ratingAssignmentList == null) {
            ratingAssignmentList = new ArrayList<RatingAssignment>();
        }
        return this.ratingAssignmentList;
    }

    /**
     * Gets the value of the lastMetric property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastMetric() {
        return lastMetric;
    }

    /**
     * Sets the value of the lastMetric property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastMetric(String value) {
        this.lastMetric = value;
    }

}
