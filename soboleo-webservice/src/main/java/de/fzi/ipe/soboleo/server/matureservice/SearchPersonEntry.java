
package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchPersonEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchPersonEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="score" type="{http://mature-ip.eu/MATURE/types}searchScoreType"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tagFreqsEvidenceList" type="{http://mature-ip.eu/MATURE/types}tagFreqEvidence" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchPersonEntry", namespace = "http://mature-ip.eu/MATURE/types", propOrder = {
    "personURI",
    "score",
    "name",
    "tagFreqsEvidenceList"
})
public class SearchPersonEntry {

    @XmlElement(required = true)
    protected String personURI;
    protected float score;
    @XmlElement(required = true)
    protected String name;
    protected List<TagFreqEvidence> tagFreqsEvidenceList;

    /**
     * Gets the value of the personURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonURI() {
        return personURI;
    }

    /**
     * Sets the value of the personURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonURI(String value) {
        this.personURI = value;
    }

    /**
     * Gets the value of the score property.
     * 
     */
    public float getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     */
    public void setScore(float value) {
        this.score = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the tagFreqsEvidenceList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tagFreqsEvidenceList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTagFreqsEvidenceList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TagFreqEvidence }
     * 
     * 
     */
    public List<TagFreqEvidence> getTagFreqsEvidenceList() {
        if (tagFreqsEvidenceList == null) {
            tagFreqsEvidenceList = new ArrayList<TagFreqEvidence>();
        }
        return this.tagFreqsEvidenceList;
    }

}
