
package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="toRemove" type="{http://mature-ip.eu/MATURE/types}tagAssignment" maxOccurs="unbounded"/>
 *         &lt;element name="agentKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "toRemove",
    "agentKey",
    "agentURI"
})
@XmlRootElement(name = "removeTagAssignmentRequest")
public class RemoveTagAssignmentRequest {

    @XmlElement(required = true)
    protected List<TagAssignment> toRemove;
    @XmlElement(required = true)
    protected String agentKey;
    @XmlElement(required = true)
    protected String agentURI;

    /**
     * Gets the value of the toRemove property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the toRemove property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getToRemove().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TagAssignment }
     * 
     * 
     */
    public List<TagAssignment> getToRemove() {
        if (toRemove == null) {
            toRemove = new ArrayList<TagAssignment>();
        }
        return this.toRemove;
    }

    /**
     * Gets the value of the agentKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentKey() {
        return agentKey;
    }

    /**
     * Sets the value of the agentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentKey(String value) {
        this.agentKey = value;
    }

    /**
     * Gets the value of the agentURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentURI() {
        return agentURI;
    }

    /**
     * Sets the value of the agentURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentURI(String value) {
        this.agentURI = value;
    }

}
