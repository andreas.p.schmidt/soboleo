
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tagURI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agentKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tagURI",
    "agentKey",
    "agentURI"
})
@XmlRootElement(name = "getNarrowerTagsRequest", namespace = "http://mature-ip.eu/MATURE")
public class GetNarrowerTagsRequest {

    protected String tagURI;
    @XmlElement(required = true)
    protected String agentKey;
    @XmlElement(required = true)
    protected String agentURI;

    /**
     * Gets the value of the tagURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagURI() {
        return tagURI;
    }

    /**
     * Sets the value of the tagURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagURI(String value) {
        this.tagURI = value;
    }

    /**
     * Gets the value of the agentKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentKey() {
        return agentKey;
    }

    /**
     * Sets the value of the agentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentKey(String value) {
        this.agentKey = value;
    }

    /**
     * Gets the value of the agentURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentURI() {
        return agentURI;
    }

    /**
     * Sets the value of the agentURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentURI(String value) {
        this.agentURI = value;
    }

}
