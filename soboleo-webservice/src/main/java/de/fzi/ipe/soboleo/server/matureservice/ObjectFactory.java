
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.fzi.ipe.soboleo.server.matureservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetLoggedEventsRequest_QNAME = new QName("http://mature-ip.eu/MATURE", "getLoggedEventsRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.fzi.ipe.soboleo.server.matureservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTagHistoryResponse }
     * 
     */
    public GetTagHistoryResponse createGetTagHistoryResponse() {
        return new GetTagHistoryResponse();
    }

    /**
     * Create an instance of {@link CreateTagAssignmentRequest }
     * 
     */
    public CreateTagAssignmentRequest createCreateTagAssignmentRequest() {
        return new CreateTagAssignmentRequest();
    }

    /**
     * Create an instance of {@link GetMetricHistoryRequest }
     * 
     */
    public GetMetricHistoryRequest createGetMetricHistoryRequest() {
        return new GetMetricHistoryRequest();
    }

    /**
     * Create an instance of {@link TagAssignment }
     * 
     */
    public TagAssignment createTagAssignment() {
        return new TagAssignment();
    }

    /**
     * Create an instance of {@link CreateTagAssignmentResponse }
     * 
     */
    public CreateTagAssignmentResponse createCreateTagAssignmentResponse() {
        return new CreateTagAssignmentResponse();
    }

    /**
     * Create an instance of {@link TagFrequencyEntry }
     * 
     */
    public TagFrequencyEntry createTagFrequencyEntry() {
        return new TagFrequencyEntry();
    }

    /**
     * Create an instance of {@link GetUserModelResponse }
     * 
     */
    public GetUserModelResponse createGetUserModelResponse() {
        return new GetUserModelResponse();
    }

    /**
     * Create an instance of {@link GetMetricHistoryResponse }
     * 
     */
    public GetMetricHistoryResponse createGetMetricHistoryResponse() {
        return new GetMetricHistoryResponse();
    }

    /**
     * Create an instance of {@link GetRelatedPeopleResponse }
     * 
     */
    public GetRelatedPeopleResponse createGetRelatedPeopleResponse() {
        return new GetRelatedPeopleResponse();
    }

    /**
     * Create an instance of {@link GetAssignedTagsByFrequencyResponse }
     * 
     */
    public GetAssignedTagsByFrequencyResponse createGetAssignedTagsByFrequencyResponse() {
        return new GetAssignedTagsByFrequencyResponse();
    }

    /**
     * Create an instance of {@link GetTagAssignmentsRequest }
     * 
     */
    public GetTagAssignmentsRequest createGetTagAssignmentsRequest() {
        return new GetTagAssignmentsRequest();
    }

    /**
     * Create an instance of {@link GetRelatedResourcesResponse }
     * 
     */
    public GetRelatedResourcesResponse createGetRelatedResourcesResponse() {
        return new GetRelatedResourcesResponse();
    }

    /**
     * Create an instance of {@link GetLoggedEventsRequest }
     * 
     */
    public GetLoggedEventsRequest createGetLoggedEventsRequest() {
        return new GetLoggedEventsRequest();
    }

    /**
     * Create an instance of {@link SearchDigitalResourceEntry }
     * 
     */
    public SearchDigitalResourceEntry createSearchDigitalResourceEntry() {
        return new SearchDigitalResourceEntry();
    }

    /**
     * Create an instance of {@link SearchPersonsByTagsResponse }
     * 
     */
    public SearchPersonsByTagsResponse createSearchPersonsByTagsResponse() {
        return new SearchPersonsByTagsResponse();
    }

    /**
     * Create an instance of {@link RemoveTagAssignmentResponse }
     * 
     */
    public RemoveTagAssignmentResponse createRemoveTagAssignmentResponse() {
        return new RemoveTagAssignmentResponse();
    }

    /**
     * Create an instance of {@link OverallRatingEntry }
     * 
     */
    public OverallRatingEntry createOverallRatingEntry() {
        return new OverallRatingEntry();
    }

    /**
     * Create an instance of {@link GetUserModelRequest }
     * 
     */
    public GetUserModelRequest createGetUserModelRequest() {
        return new GetUserModelRequest();
    }

    /**
     * Create an instance of {@link SearchPersonEntry }
     * 
     */
    public SearchPersonEntry createSearchPersonEntry() {
        return new SearchPersonEntry();
    }

    /**
     * Create an instance of {@link TagFreqEvidence }
     * 
     */
    public TagFreqEvidence createTagFreqEvidence() {
        return new TagFreqEvidence();
    }

    /**
     * Create an instance of {@link GetTagHistoryRequest }
     * 
     */
    public GetTagHistoryRequest createGetTagHistoryRequest() {
        return new GetTagHistoryRequest();
    }

    /**
     * Create an instance of {@link SearchDigitalResourcesByTagsResponse }
     * 
     */
    public SearchDigitalResourcesByTagsResponse createSearchDigitalResourcesByTagsResponse() {
        return new SearchDigitalResourcesByTagsResponse();
    }

    /**
     * Create an instance of {@link LogEvent }
     * 
     */
    public LogEvent createLogEvent() {
        return new LogEvent();
    }

    /**
     * Create an instance of {@link Tag }
     * 
     */
    public Tag createTag() {
        return new Tag();
    }

    /**
     * Create an instance of {@link InvalidKeyMsg }
     * 
     */
    public InvalidKeyMsg createInvalidKeyMsg() {
        return new InvalidKeyMsg();
    }

    /**
     * Create an instance of {@link GetAssignedTagsByFrequencyRequest }
     * 
     */
    public GetAssignedTagsByFrequencyRequest createGetAssignedTagsByFrequencyRequest() {
        return new GetAssignedTagsByFrequencyRequest();
    }

    /**
     * Create an instance of {@link GetLoggedEventsResponse }
     * 
     */
    public GetLoggedEventsResponse createGetLoggedEventsResponse() {
        return new GetLoggedEventsResponse();
    }

    /**
     * Create an instance of {@link RatingAssignment }
     * 
     */
    public RatingAssignment createRatingAssignment() {
        return new RatingAssignment();
    }

    /**
     * Create an instance of {@link GetRelatedResourcesRequest }
     * 
     */
    public GetRelatedResourcesRequest createGetRelatedResourcesRequest() {
        return new GetRelatedResourcesRequest();
    }

    /**
     * Create an instance of {@link GetTagAssignmentsResponse }
     * 
     */
    public GetTagAssignmentsResponse createGetTagAssignmentsResponse() {
        return new GetTagAssignmentsResponse();
    }

    /**
     * Create an instance of {@link LocalizedString }
     * 
     */
    public LocalizedString createLocalizedString() {
        return new LocalizedString();
    }

    /**
     * Create an instance of {@link RemoveTagAssignmentRequest }
     * 
     */
    public RemoveTagAssignmentRequest createRemoveTagAssignmentRequest() {
        return new RemoveTagAssignmentRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mature-ip.eu/MATURE", name = "getLoggedEventsRequest")
    public JAXBElement<Object> createGetLoggedEventsRequest(Object value) {
        return new JAXBElement<Object>(_GetLoggedEventsRequest_QNAME, Object.class, null, value);
    }

}
