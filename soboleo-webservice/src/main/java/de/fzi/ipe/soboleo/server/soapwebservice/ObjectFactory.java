
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.fzi.ipe.soboleo.server.soapwebservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.fzi.ipe.soboleo.server.soapwebservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateTagAssignmentResponse }
     * 
     */
    public CreateTagAssignmentResponse createCreateTagAssignmentResponse() {
        return new CreateTagAssignmentResponse();
    }

    /**
     * Create an instance of {@link CreateTagAssignmentRequest }
     * 
     */
    public CreateTagAssignmentRequest createCreateTagAssignmentRequest() {
        return new CreateTagAssignmentRequest();
    }

    /**
     * Create an instance of {@link GetTagAssignmentsResponse }
     * 
     */
    public GetTagAssignmentsResponse createGetTagAssignmentsResponse() {
        return new GetTagAssignmentsResponse();
    }

    /**
     * Create an instance of {@link GetAssignedTagsByFrequencyResponse }
     * 
     */
    public GetAssignedTagsByFrequencyResponse createGetAssignedTagsByFrequencyResponse() {
        return new GetAssignedTagsByFrequencyResponse();
    }

    /**
     * Create an instance of {@link GetTagAssignmentsRequest }
     * 
     */
    public GetTagAssignmentsRequest createGetTagAssignmentsRequest() {
        return new GetTagAssignmentsRequest();
    }

    /**
     * Create an instance of {@link GetAssignedTagsByFrequencyRequest }
     * 
     */
    public GetAssignedTagsByFrequencyRequest createGetAssignedTagsByFrequencyRequest() {
        return new GetAssignedTagsByFrequencyRequest();
    }

    /**
     * Create an instance of {@link TagAssignment }
     * 
     */
    public TagAssignment createTagAssignment() {
        return new TagAssignment();
    }

    /**
     * Create an instance of {@link TagFrequencyEntry }
     * 
     */
    public TagFrequencyEntry createTagFrequencyEntry() {
        return new TagFrequencyEntry();
    }

    /**
     * Create an instance of {@link LocalizedString }
     * 
     */
    public LocalizedString createLocalizedString() {
        return new LocalizedString();
    }

}
