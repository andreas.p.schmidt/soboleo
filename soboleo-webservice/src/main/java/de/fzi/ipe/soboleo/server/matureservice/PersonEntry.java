
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * represents one person in the list with personURI  (mailto:emailAddress) and name
 * 
 * <p>Java class for personEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="personEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "personEntry", namespace = "http://mature-ip.eu/MATURE", propOrder = {
    "personURI",
    "name"
})
public class PersonEntry {

    @XmlElement(required = true)
    protected String personURI;
    @XmlElement(required = true)
    protected String name;

    /**
     * Gets the value of the personURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonURI() {
        return personURI;
    }

    /**
     * Sets the value of the personURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonURI(String value) {
        this.personURI = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

}
