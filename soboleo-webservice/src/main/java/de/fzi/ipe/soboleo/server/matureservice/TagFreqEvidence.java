
package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * list of tags and how often the person is associated with the tag; this is grouped by evidence that indicates if the person is annotated by the tag, or has used the tag etc.
 * 
 * <p>Java class for tagFreqEvidence complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tagFreqEvidence">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="evidence" type="{http://mature-ip.eu/MATURE/types}evidenceType"/>
 *         &lt;element name="tagFreqsList" type="{http://mature-ip.eu/MATURE/types}tagFrequencyEntry" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tagFreqEvidence", namespace = "http://mature-ip.eu/MATURE/types", propOrder = {
    "evidence",
    "tagFreqsList"
})
public class TagFreqEvidence {

    @XmlElement(required = true)
    protected EvidenceType evidence;
    @XmlElement(required = true)
    protected List<TagFrequencyEntry> tagFreqsList;

    /**
     * Gets the value of the evidence property.
     * 
     * @return
     *     possible object is
     *     {@link EvidenceType }
     *     
     */
    public EvidenceType getEvidence() {
        return evidence;
    }

    /**
     * Sets the value of the evidence property.
     * 
     * @param value
     *     allowed object is
     *     {@link EvidenceType }
     *     
     */
    public void setEvidence(EvidenceType value) {
        this.evidence = value;
    }

    /**
     * Gets the value of the tagFreqsList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tagFreqsList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTagFreqsList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TagFrequencyEntry }
     * 
     * 
     */
    public List<TagFrequencyEntry> getTagFreqsList() {
        if (tagFreqsList == null) {
            tagFreqsList = new ArrayList<TagFrequencyEntry>();
        }
        return this.tagFreqsList;
    }

}
