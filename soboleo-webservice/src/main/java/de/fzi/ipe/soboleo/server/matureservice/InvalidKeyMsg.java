
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="invalidKeyMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "invalidKeyMsg"
})
@XmlRootElement(name = "invalidKeyMsg", namespace = "http://mature-ip.eu/MATURE/types")
public class InvalidKeyMsg {

    @XmlElement(required = true)
    protected String invalidKeyMsg;

    /**
     * Gets the value of the invalidKeyMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvalidKeyMsg() {
        return invalidKeyMsg;
    }

    /**
     * Sets the value of the invalidKeyMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvalidKeyMsg(String value) {
        this.invalidKeyMsg = value;
    }

}
