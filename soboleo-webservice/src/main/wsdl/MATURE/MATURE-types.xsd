<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:t="http://mature-ip.eu/MATURE/types" attributeFormDefault="unqualified" elementFormDefault="unqualified" targetNamespace="http://mature-ip.eu/MATURE/types">
   <xsd:complexType name="tagFrequencyEntry">
        <xsd:annotation>
        	<xsd:documentation>tag that is used, e.g. for annotation, and how often it is used</xsd:documentation>
        </xsd:annotation>
        <xsd:sequence>
      		<xsd:element name="tag" type="t:tag" maxOccurs="1"
      			minOccurs="1">
      		</xsd:element>
			<xsd:element name="frequency" type="xsd:int" maxOccurs="1"
      			minOccurs="1">
      		</xsd:element>
      	</xsd:sequence>
      </xsd:complexType>
	  
	    <xsd:complexType name="searchDigitalResourceEntry">
  	<xsd:sequence>
  		<xsd:element name="digitalResourceURI" type="xsd:string"
  			maxOccurs="1" minOccurs="1">
  			<xsd:annotation>
  				<xsd:documentation>URI = URL of the digital resource</xsd:documentation>
  			</xsd:annotation>
  		</xsd:element>
  		<xsd:element name="score" type="t:searchScoreType"
  			maxOccurs="1" minOccurs="1">
                <xsd:annotation>
                	<xsd:documentation>score that indicates the how well the digital resource matches the query; range [0-1]</xsd:documentation>
                </xsd:annotation>
  		</xsd:element>
  		<xsd:element name="title" type="xsd:string" maxOccurs="1"
  			minOccurs="1">
                <xsd:annotation>
                	<xsd:documentation>title of the digital resource</xsd:documentation>
                </xsd:annotation>
  		</xsd:element>
  		<xsd:element name="tagFreqList" type="t:tagFrequencyEntry"
  			maxOccurs="unbounded" minOccurs="0">
                <xsd:annotation>
                	<xsd:documentation>list of tags with which the digital resource is annotated and how often it's annotated with that tag</xsd:documentation>
                </xsd:annotation>
  		</xsd:element>
  		<xsd:element name="overallRating" type="t:overallRatingEntry" maxOccurs="1" minOccurs="1">
  			<xsd:annotation>
  				<xsd:documentation></xsd:documentation>
  			</xsd:annotation></xsd:element>
  	</xsd:sequence>
	    </xsd:complexType>
 
	<xsd:simpleType name="searchScoreType">
        <xsd:annotation>
        	<xsd:documentation>score that indicates the how well the digital resource matches the query; range [0-1]</xsd:documentation></xsd:annotation>
        <xsd:restriction base="xsd:float">
  					<xsd:minInclusive value="0"></xsd:minInclusive>
  					<xsd:maxInclusive value="1"></xsd:maxInclusive>
  				</xsd:restriction>
  			</xsd:simpleType>

  				<xsd:simpleType name="operator">
  					<xsd:restriction base="xsd:string">
  						<xsd:enumeration value="AND"></xsd:enumeration>
  						<xsd:enumeration value="OR"></xsd:enumeration>
  						<xsd:enumeration value="NOT"></xsd:enumeration>
  					</xsd:restriction>
  				</xsd:simpleType>

  				<xsd:simpleType name="ratingScoreType">
  					<xsd:annotation>
  						<xsd:documentation>
  							0 = no rating, 1-5 explicit rating being 0
  							the worst and 5 the best
  						</xsd:documentation>
  					</xsd:annotation>
  					<xsd:restriction base="xsd:int">
  						<xsd:minInclusive value="0"></xsd:minInclusive>
  						<xsd:maxInclusive value="5"></xsd:maxInclusive>
  					</xsd:restriction>
  				</xsd:simpleType>
	<xsd:complexType name="overallRatingEntry">
        <xsd:annotation>
        	<xsd:documentation>score that aggregates all ratings for this digital resource and the total number of ratings that are the aggregation basis</xsd:documentation></xsd:annotation>
        <xsd:sequence>
  			<xsd:element name="ratingScore" type="t:ratingScoreType" maxOccurs="1" minOccurs="1">
  			</xsd:element>
  			<xsd:element name="ratingFreq" type="xsd:int" maxOccurs="1" minOccurs="1"></xsd:element>
  		</xsd:sequence>
  	</xsd:complexType>
	
  				<xsd:element name="invalidKeyMsg">
      	<xsd:complexType>
      		<xsd:sequence>
      			<xsd:element name="invalidKeyMsg" type="xsd:string"></xsd:element>
      		</xsd:sequence>
      	</xsd:complexType>
      </xsd:element>

 <xsd:complexType name="searchPersonEntry">
  	<xsd:sequence>
  		<xsd:element name="personURI" type="xsd:string" maxOccurs="1"
  			minOccurs="1">
  			<xsd:annotation>
  				<xsd:documentation>
  					mailto:agent@example.com
  				</xsd:documentation>
  			</xsd:annotation>
  		</xsd:element>
  		<xsd:element name="score" type="t:searchScoreType" maxOccurs="1" minOccurs="1">
                <xsd:annotation>
                	<xsd:documentation>score that indicates the how well the digital resource matches the query; range [0-1]</xsd:documentation></xsd:annotation>
  		</xsd:element>
  		<xsd:element name="name" type="xsd:string" maxOccurs="1"
  			minOccurs="1">
                <xsd:annotation>
                	<xsd:documentation>name of the person</xsd:documentation>
                </xsd:annotation>
  		</xsd:element>
  		<xsd:element name="tagFreqsEvidenceList" type="t:tagFreqEvidence" maxOccurs="unbounded" minOccurs="0">
  			<xsd:annotation>
  				<xsd:documentation>list of tags and how often the person is associated with the tag; this is grouped by if the person is annotated by the tag, or has used the tag etc.</xsd:documentation>
  			</xsd:annotation></xsd:element>
  	</xsd:sequence>
  </xsd:complexType>

    <xsd:complexType name="tagFreqEvidence">
        <xsd:annotation>
        	<xsd:documentation>list of tags and how often the person is associated with the tag; this is grouped by evidence that indicates if the person is annotated by the tag, or has used the tag etc.</xsd:documentation>
        </xsd:annotation>
        <xsd:sequence>
  		<xsd:element name="evidence" type="t:evidenceType" maxOccurs="1"
  			minOccurs="1">
  			<xsd:annotation>
  				<xsd:documentation>
  					assigned by other persons automatically assigned
  					person searched for, looked at, used for
  					assignments etc.
  				</xsd:documentation>
  			</xsd:annotation>
  		
  		</xsd:element>
  		<xsd:element name="tagFreqsList" type="t:tagFrequencyEntry" maxOccurs="unbounded" minOccurs="1"></xsd:element>
  	</xsd:sequence>
  </xsd:complexType>
  
  	<xsd:simpleType name="evidenceType">
  				<xsd:restriction base="xsd:string">
  					<xsd:enumeration value="person-tagged-with"></xsd:enumeration>
  					<xsd:enumeration value="person-uses"></xsd:enumeration>
  				</xsd:restriction>
  			</xsd:simpleType>
  
   <xsd:complexType name="ratingAssignment">
  	<xsd:sequence>
  		<xsd:element name="makerURI" type="xsd:string" maxOccurs="1"
  			minOccurs="1">
  		</xsd:element>
  		<xsd:element name="digitalResourceURI" type="xsd:string"
  			maxOccurs="1" minOccurs="1">
  		</xsd:element>
  		<xsd:element name="ratingScore" type="t:ratingScoreType" maxOccurs="1" minOccurs="1">
  		</xsd:element>
  		<xsd:element name="ratingDate" type="xsd:dateTime" maxOccurs="1" minOccurs="1"></xsd:element>
  	</xsd:sequence>
  </xsd:complexType>
  
  <xsd:complexType name="tagAssignment">
      	<xsd:sequence>
      		<xsd:element name="tag" type="t:tag" maxOccurs="1"
      			minOccurs="1">
                        <xsd:annotation>
                        	<xsd:documentation>local tag URI
</xsd:documentation>
                        </xsd:annotation>
      		</xsd:element>

      		<xsd:element name="resourceURI" type="xsd:string"
      			maxOccurs="1" minOccurs="1">
                        <xsd:annotation>
                        	<xsd:documentation>local URI
</xsd:documentation>
                        </xsd:annotation>
      		</xsd:element>
      		<xsd:element name="makerURI" type="xsd:string"
      			maxOccurs="1" minOccurs="1">
                        <xsd:annotation>
                        	<xsd:documentation>expected format:

mailto:agent@example.com
</xsd:documentation>
                        </xsd:annotation>
      		</xsd:element>
      		<xsd:element name="taggingDate" type="xsd:dateTime"
      			maxOccurs="1" minOccurs="1">
      		</xsd:element>
      		<xsd:element name="proxyURL" type="xsd:string"
      			maxOccurs="1" minOccurs="0">
                        <xsd:annotation>
                        	<xsd:documentation>website URL or person proxy URL</xsd:documentation>
                        </xsd:annotation>
      		</xsd:element>
      	</xsd:sequence>
      </xsd:complexType>
	  
	  
  <xsd:complexType name="LocalizedString">
      	<xsd:simpleContent>
      		<xsd:extension base="xsd:string">
      			<xsd:attribute name="lang"
      				type="xsd:language">
      			</xsd:attribute>
      		</xsd:extension>
      	</xsd:simpleContent>
      </xsd:complexType>
	 
     <xsd:complexType name="tag">
                <xsd:annotation>
                	<xsd:documentation>tag according to SKOS concept</xsd:documentation>
                </xsd:annotation>
                <xsd:sequence>
                	<xsd:element name="tagURI" type="xsd:string"></xsd:element>
                	<xsd:element name="prefLabel"
                		type="t:LocalizedString" maxOccurs="unbounded"
                		minOccurs="1">
                	</xsd:element>
                	<xsd:element name="altLabel"
                		type="t:LocalizedString" maxOccurs="unbounded"
                		minOccurs="0">
                	</xsd:element>
                	<xsd:element name="hiddenLabel"
                		type="t:LocalizedString" maxOccurs="unbounded"
                		minOccurs="0">
                	</xsd:element>
                	<xsd:element name="broader" type="xsd:string"
                		maxOccurs="unbounded" minOccurs="0">
                		<xsd:annotation>
                			<xsd:documentation>
                				URI (in SOBOLEO) of the broader tag
                			</xsd:documentation>
                		</xsd:annotation>
                	</xsd:element>
                	<xsd:element name="narrower" type="xsd:string"
                		maxOccurs="unbounded" minOccurs="0">
                		<xsd:annotation>
                			<xsd:documentation>
                				URI (in SOBOLEO) of the narrower tag
                			</xsd:documentation>
                		</xsd:annotation>
                	</xsd:element>
                	<xsd:element name="related" type="xsd:string"
                		maxOccurs="unbounded" minOccurs="0">
                		<xsd:annotation>
                			<xsd:documentation>
                				URI (in SOBOLEO) of the realted tag
                			</xsd:documentation>
                		</xsd:annotation>
                	</xsd:element>
                	<xsd:element name="description" type="t:LocalizedString" maxOccurs="unbounded" minOccurs="0"></xsd:element>
                </xsd:sequence>
     </xsd:complexType>	 
	  
	  <xsd:complexType name="logEvent">
  	<xsd:sequence>
  		<xsd:element name="makerURI" type="xsd:string" maxOccurs="1"
  			minOccurs="1">
  		</xsd:element>
  		<xsd:element name="actionType" type="xsd:string" maxOccurs="1"
  			minOccurs="1">
  		</xsd:element>
  		<xsd:element name="content" type="xsd:string" maxOccurs="1"
  			minOccurs="1">
  		</xsd:element>
  		<xsd:element name="objectURI" type="xsd:string" maxOccurs="1" minOccurs="1"></xsd:element>
  	</xsd:sequence>
  </xsd:complexType>  
</xsd:schema>