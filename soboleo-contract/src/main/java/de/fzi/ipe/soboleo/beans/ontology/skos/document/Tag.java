package de.fzi.ipe.soboleo.beans.ontology.skos.document;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Tag implements IsSerializable{

	private String tagID, userID, conceptID, docURI, pageURL;
	private Date date;
	
	//TODO: Tag as abstract class for PersonTag and WebDocumentTag etc. 
	public Tag(String tagID, String userID, String conceptID, Date date) {
		this.tagID = tagID;
		this.userID = userID;
		this.conceptID = conceptID;
		this.date = date;
	}
	
	@SuppressWarnings("unused")
	private Tag() {;}
	
	public String getTagID() {
		return tagID;
	}
	
	public String getUserID() {
		return userID;
	}
	
	public String getConceptID() {
		return conceptID;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDocumentURI(String docURI){
		this.docURI = docURI;
	}
	
	public String getDocumentURI(){
		return docURI;
	}
	
	
	public void setPageURL(String pageURL){
		this.pageURL = pageURL;
	}
	
	public String getPageURL(){
		return pageURL;
	}
}
