/*
 * A trie that is used to find references to concepts in text. 
 * 
 * Ontoprise code for project ksi_underground
 * Created on 23.07.2006 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;

public class SynonymsTrie implements IsSerializable{
	
	
	private static class Node {
		
		static final Set<ClientSideConcept> EMPTY_SET = new HashSet<ClientSideConcept>();
		
		Set<ClientSideConcept> concepts = null;
		Map<Character,Node> children = new HashMap<Character,Node>();
		
		
		Set<ClientSideConcept> getConcepts() {
			return (concepts != null) ? concepts : EMPTY_SET;
		}
		
		void addConcept(ClientSideConcept c) {
			if (concepts == null) concepts = new HashSet<ClientSideConcept>();
			concepts.add(c);
		}
		
		void add(String synonym, int index, ClientSideConcept c) {
			if (synonym.length() == index) {
				addConcept(c);
			}
			else {
				Node child = children.get(synonym.charAt(index));
				if (child == null) {
					child = new Node();
					children.put(synonym.charAt(index),child);
				}
				child.add(synonym,index+1,c);
			}
		}		

		void get(String searchString, int index, Set<ClientSideConcept> foundConcepts) {
			foundConcepts.addAll(getConcepts());
			if (searchString.length() > index) {
				Node child = children.get(searchString.charAt(index));
				if (child != null) {
					child.get(searchString,index+1,foundConcepts);
				}
			}
		}
		
	}
		
	private Node root = new Node();
	
	
	public SynonymsTrie(ClientSideTaxonomy tax) {
		for (ClientSideConcept c: tax.getConcepts()) {
			for (LocalizedString alt : c.getTexts(SKOS.ALT_LABEL)) root.add(alt.getString().toLowerCase()+" ",0,c);
			for (LocalizedString pref : c.getTexts(SKOS.PREF_LABEL)) root.add(pref.getString().toLowerCase()+" ",0,c);
		}
	}

	public Set<ClientSideConcept> getConcepts(String searchString) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (int i=0;i<searchString.length();i++) {
			root.get(searchString.toLowerCase()+" ",i,toReturn);
		}
		return toReturn;
	}

}
