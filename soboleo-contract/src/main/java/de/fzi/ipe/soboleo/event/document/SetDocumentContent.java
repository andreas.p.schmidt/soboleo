/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.document;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public class SetDocumentContent extends CommandEvent implements IsSerializable{
	
	private String docURI;
	private String docContent;
	
	public SetDocumentContent(String docURI, String docContent) {
		this.docURI = docURI;
		this.docContent = docContent;
	}
	
	protected SetDocumentContent() {}
	
	public String getDocURI() {
		return docURI;
	}
	
	public String getContent() {
		return docContent;
	}

	
	
}
