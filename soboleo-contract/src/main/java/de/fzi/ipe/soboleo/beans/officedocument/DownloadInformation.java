package de.fzi.ipe.soboleo.beans.officedocument;

import java.io.InputStream;

/** stores the download information for servlet .*/
public class DownloadInformation {

	
	private String fileName;	
	private transient InputStream is;
	private String contentType;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public InputStream getIs() {
		return is;
	}
	public void setIs(InputStream is) {
		this.is = is;
	}
	
	public void setContentType(String type)
	{
		if (type.equals("doc"))
		{
			contentType="application/msword";
		}
	}
	
	public String getContentType()
	{
		return contentType;
	}
	
	
}
