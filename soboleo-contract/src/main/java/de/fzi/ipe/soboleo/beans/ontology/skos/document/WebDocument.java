package de.fzi.ipe.soboleo.beans.ontology.skos.document;

import java.util.Set;


/**
 * A client side copy of a document representation in the tripleStore. Contains the information
 * about the document and all its annotations (tags). This is an immutable object - changes should
 * be performed using commands send over the event bus. 
 */
public class WebDocument extends Document {

	private String url;
	
	public WebDocument(String uri, String url, String title, Set<Tag> tags) {
		super(uri, title, tags);
		this.url = url;
		//TODO pageURL should not be set here!!!
		for(Tag t : getAllTags()) t.setPageURL(url);
	}
	
	@SuppressWarnings("unused")
	private WebDocument() {;}	
	
	public String getURL() {
		return url;
	} 
}
