package de.fzi.ipe.soboleo.event.logging;

import de.fzi.ipe.soboleo.event.EventImpl;


public class BrowseDialog extends EventImpl {
	
	private String dialogURI;
	
	public BrowseDialog(String dialogURI){
		this.dialogURI = dialogURI;
	}
	
	@SuppressWarnings("unused")
	private BrowseDialog() {;}
	
	public String getDialogURI(){
		return dialogURI;
	}

}
