
package de.fzi.ipe.soboleo.event.peopletagging;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

/**
 * @author FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
public class RemovePersonTag extends CommandEvent{

	private String personTagURI;
	private String taggedPersonURI;
	private String conceptURI;
	
	/**
	 * Command event for removing the given person tag from the store.
	 * @param personTagURI
	 */
	public RemovePersonTag(String personTagURI, String taggedPersonURI, String conceptURI) 
	{
		this.personTagURI = personTagURI;
		this.taggedPersonURI = taggedPersonURI;
		this.conceptURI = conceptURI;
	}
	
	@SuppressWarnings("unused")
	private RemovePersonTag() { ; }
	
	public String getPersonTagURI() {
		return personTagURI;
	}
	
	public String getConceptURI(){
		return conceptURI;
	}
	
	public String getTaggedPersonURI(){
		return taggedPersonURI;
	}
}
