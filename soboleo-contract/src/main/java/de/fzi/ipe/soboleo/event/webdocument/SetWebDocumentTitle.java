/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.webdocument;

import de.fzi.ipe.soboleo.event.document.SetDocumentTitle;

public class SetWebDocumentTitle extends SetDocumentTitle{
	
	public SetWebDocumentTitle(String docURI, String docTitle) {
		super(docURI,docTitle);
	}
	
	@SuppressWarnings("unused")
	private SetWebDocumentTitle(){;}
}
