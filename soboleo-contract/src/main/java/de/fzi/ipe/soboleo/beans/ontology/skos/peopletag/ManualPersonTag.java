/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos.peopletag;

import java.util.Date;

/** 
 * A data transfer and model object for person tags created manually. 
 */
public class ManualPersonTag extends PersonTag{

	private String tagID, userID, conceptID,pageURL, taggedPersonURI;
	private Date date;
	
	public ManualPersonTag(String tagID, String userID, String conceptID, String pageURL,Date date) {
		this.tagID = tagID;
		this.userID = userID;
		this.conceptID = conceptID;
		this.pageURL = pageURL;
		this.date = date;
	}
	
	@SuppressWarnings("unused")
	private ManualPersonTag() {; }
	
	public String getUserID() {
		return userID;
	}
	
	@Override
	public String getConceptID() {
		return conceptID;
	}

	public Date getDate() {
		return date;
	}
	
	public String getPageURL() {
		return pageURL;
	}

	@Override
	public String getTagID() {
		return tagID;
	}

	@Override
	public String getTaggedPersonURI() {
		return taggedPersonURI;
	}

	public void setTaggedPersonURI(String taggedPersonURI){
		this.taggedPersonURI = taggedPersonURI;
	}
}
