/*
 * FZI - Information Process Engineering 
 * Created on 15.01.2009 by zach
 */
package de.fzi.ipe.soboleo.ontology.triplestore;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;

/**
 * @author  FZI - Information Process Engineering 
 * Created on 15.01.2009 by zach
 *
 */
public interface TripleStore {

	/**
	 * 
	 */
	public static final String SOBOLEO_NS = "http://soboleo.com/ns/1.0#";

	/**
	 * Returns the factory that can be used to create Resources, URIs, Values etc. 
	 * @return ValueFactory
	 */
	public ValueFactory getValueFactory();

	/**
	 * Adds a triple. Use the valueFactory (@see getValueFactory) to create Resources,URIS etc.
	 */
	public void addTriple(Resource subject, URI predicate, Value object, Resource context) throws IOException;	

	/**
	 * Convenience method - all strings are interpreted as uris. ContextURI can be null.
	 */
	public void addTriple(String subjectURI, String predicateURI, String objectURI, String contextURI) throws IOException;
	
	/**
	 * Convenience method - all strings are interpreted as uris. Context URI can be null. 
	 */
	public void addTriple(String subjectURI, String predicateURI, int objectValue, String contextURI) throws IOException;
	
	/**
	 * Convenience methods - sets the current time as timestamp for the given subject and predicate. Any 
	 * previous value is overwritten. 
	 */
	public void setTimestamp(Resource subject, URI predicate, String contextURI) throws IOException;
	
	/**
	 * Removes all triples satisfying the pattern. Parameters can be null, specifying wildcards
	 */
	public void removeTriples(Resource subject, URI predicate , Value object, Resource context) throws IOException;
	
	/**
	 * Convenience methods for removeTriples, all strings present are interpreted as uris. 
	 */
	public void removeTriples(String subjectURI, String predicateURI, String objectURI, String contextURI) throws IOException;
	
	/**
	 * Returns all statements for the query (use null as wildcard). Statements are returned for all contexts. 
	 */
	public List<Statement> getStatementList(Resource subject, URI predicate, Value object) throws IOException; 

	/**
	 * Returns all statements for the query (use null as wildcard). Statements are returned for all contexts. 
	 * All Strings are understood as uri's
	 */
	public List<Statement> getStatementList(String subject, String predicate, String object) throws IOException; 

	/**
	 * Returns true if at least one triple exists that matches the given pattern (use null as wildcard)
	 * @param subject 
	 * @param predicate 
	 * @param object 
	 * @return boolean
	 * @throws IOException 
	 */
	public boolean hasStatement(Resource subject, URI predicate, Value object) throws IOException; 
	
	/**
	 * Executes a SELECT SPARQL query. Returns set of results, each containing a Hashmap of bindings <String, Value>
	 * @param queryString 
	 * @return Set
	 * @throws IOException 
	 */
	public Set<HashMap<String,Value>> executeSelectSPARQLQuery(String queryString) throws IOException; 
	
	/**
	 * @param queryString
	 * @param paramName
	 * @return Set<Value>
	 * @throws IOException
	 */
	public Set<Value> executeSelectSPARQLQueryForParameter(String queryString, String paramName) throws IOException;
	
	/**
	 * Creates a new unique uri
	 */
	public URI getUniqueURI() throws IOException;
	
	/**
	 * Returns the datastore's URI
	 * @return
	 */
	public URI getDatastoreURI();
	
	/**
	 * Returns a string with the entire content of the triple store (as quads)
	 */
	public String getContentDump () throws IOException;	

	/**
	 * Returns a string with the entire taxonomy as RDF/XML
	 */
	public String getSKOSTaxonomyAsXML() throws IOException;
	
	/**
	 * Closes the triple store and saves all unsaved information. The tripleStore object cannot be used after calling close. 
	 */
	public void close() throws IOException;

	public String getCompleteRDFDump() throws IOException ;
}
