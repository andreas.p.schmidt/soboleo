/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos.peopletag;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

public abstract class TaggedPerson implements IsSerializable{

	private String uri, name, email;
	private Set<PersonTag> tags;
	private Map<String,AggregatedConceptTags> conceptTags = null;
	
	public TaggedPerson(String uri, String email, String name, Set<PersonTag> tags) {
		this.uri = uri;
		this.email = email;
		this.name = name;
		//TODO taggedPersonURI should not be set here!!!
		for(PersonTag t : tags) t.setTaggedPersonURI(uri);
		this.tags = tags;
	}
	
	@Deprecated // only for GWT stuff. 
	TaggedPerson() {}

	public Set<String> getURLs() {
		Set<String> toReturn = new HashSet<String>();
		for (PersonTag tag:tags) {
			if (tag instanceof ManualPersonTag) {
				toReturn.add(((ManualPersonTag)tag).getPageURL());
			}
		}
		return toReturn;
	}
	
	//TODO ggf. sortiert?
	public Collection<AggregatedConceptTags> getAggregatedTags() {
		if (conceptTags == null) calculateAggregatedTags();
		return conceptTags.values();
	}
	
	public AggregatedConceptTags getAggregatedTags(String conceptURI){
		if(conceptTags == null) calculateAggregatedTags();
		return conceptTags.get(conceptURI);
	}

	private void calculateAggregatedTags() {
		conceptTags = new HashMap<String,AggregatedConceptTags>(); 
		for (PersonTag tag: tags) {
			AggregatedConceptTags aggregatedTags = conceptTags.get(tag.getConceptID());
			if (aggregatedTags == null) {
				aggregatedTags = new AggregatedConceptTags(tag.getConceptID());
				conceptTags.put(tag.getConceptID(), aggregatedTags);
			}
			aggregatedTags.addTag(tag);
		}
	}
	
	public Collection<AggregatedConceptTags> getManualAggregatedTags(){
		return getAggregatedTags();
	}
	
	public Set<ManualPersonTag> getUserTags(String userID) {
		Set<ManualPersonTag>  toReturn = new HashSet<ManualPersonTag>();
		for (PersonTag currentTag: getTags()) {
			if (currentTag instanceof ManualPersonTag) {
				ManualPersonTag manualTag = (ManualPersonTag) currentTag;
				if (manualTag.getUserID()!= null && manualTag.getUserID().equals(userID)) toReturn.add(manualTag);
			}
		}
		return toReturn;
	}
	
	public Set<ManualPersonTag> getManualTags() {
		Set<ManualPersonTag>  toReturn = new HashSet<ManualPersonTag>();
		for (PersonTag currentTag: getTags()) {
			if (currentTag instanceof ManualPersonTag) {
				toReturn.add((ManualPersonTag)currentTag);
			}
		}
		return toReturn;
	}
	
	public Set<ManualPersonTag> getManualTags(String conceptURI) {
		Set<ManualPersonTag>  toReturn = new HashSet<ManualPersonTag>();
		for (PersonTag currentTag: getTags()) {
			if (currentTag instanceof ManualPersonTag && currentTag.getConceptID()!= null && currentTag.getConceptID().equals(conceptURI)) {
				toReturn.add((ManualPersonTag)currentTag);
			}
		}
		return toReturn;
	}
	
	public ManualPersonTag hasUserTag(String userID, String conceptID){
		for(PersonTag currentTag: getTags()){
			if(currentTag instanceof ManualPersonTag){
				ManualPersonTag manualTag = (ManualPersonTag) currentTag;
				if(manualTag.getUserID()!= null && manualTag.getConceptID()!= null && manualTag.getUserID().equals(userID) && manualTag.getConceptID().equals(conceptID)) return manualTag;
			}
		}
		return null;
	}
	
	public Set<PersonTag> getTags(String conceptURI){
		if (conceptTags == null) calculateAggregatedTags();
		return (conceptTags.get(conceptURI) != null) ? ((AggregatedConceptTags) conceptTags.get(conceptURI)).getTags() : null;
	}
	
	public Set<String> getTagConceptURIs(){
		if(conceptTags == null) calculateAggregatedTags();
		return conceptTags.keySet();
	}
 
	public String getURI() {
		return uri;
	}

	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public Set<PersonTag> getTags() {
		return tags;
	}
	
	@Override
	public boolean equals(Object taggedPerson)
	{
	    if(taggedPerson==null)
	    {
		return false;
	    }
		if(taggedPerson instanceof TaggedPerson)
		{
			TaggedPerson compareTaggedPerson = (TaggedPerson) taggedPerson;
			if(this.getName().toLowerCase().contentEquals(compareTaggedPerson.getName().toLowerCase()))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	

}
