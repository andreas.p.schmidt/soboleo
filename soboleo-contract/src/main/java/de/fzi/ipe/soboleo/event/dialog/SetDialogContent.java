/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;

import de.fzi.ipe.soboleo.event.document.SetDocumentContent;

public class SetDialogContent extends SetDocumentContent{
	
	public SetDialogContent(String docURI, String docContent) {
		super(docURI,docContent);
	}
	
	@SuppressWarnings("unused")
	private SetDialogContent(){;}
}
