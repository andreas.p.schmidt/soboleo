package de.fzi.ipe.soboleo.beans.ontology.skos;

import java.util.Comparator;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;

public class ClientSideConceptComparator implements	IsSerializable, Comparator<ClientSideConcept> {
	private final Language[] languages;
	
	public ClientSideConceptComparator(Language... languages) {
		this.languages = languages;
	}
	
	public int compare(ClientSideConcept one, ClientSideConcept two) {
		LocalizedString oneLabel = one.getBestFitText(SKOS.PREF_LABEL, languages);
		LocalizedString twoLabel = two.getBestFitText(SKOS.PREF_LABEL, languages);
		return (oneLabel.getString().compareTo(twoLabel.getString()));					
	}
}

