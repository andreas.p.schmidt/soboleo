/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos.peopletag;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * A convenience class that enables easy access to all annotations
 * that use one concept. 
 */
public class AggregatedConceptTags implements IsSerializable{
	
	private String conceptID;
	private int weight = 0;
	private Set<PersonTag> tags = new HashSet<PersonTag>();
	
	
	public AggregatedConceptTags(String conceptID) {
		this.conceptID = conceptID;
	}

	@SuppressWarnings("unused")
	private AggregatedConceptTags() { ; }
	
	public void addTag(PersonTag personTag) {
		if (personTag instanceof ManualPersonTag) {
			weight++;
		}
		else {
			//TODO AutomaticPersonTag
		}
		tags.add(personTag);
	}
	
	public String getConceptID() {
		return conceptID;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public Set<PersonTag> getTags() {
		return tags;
	}
	
	public boolean addAll(AggregatedConceptTags aggregated){
		if(aggregated.getConceptID().equals(conceptID)){
			if(tags.addAll(aggregated.getTags())){
				weight = weight + aggregated.getWeight();
				return true;
			}
		}
		return false;
	}
	
}
