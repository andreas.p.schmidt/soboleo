package de.fzi.ipe.soboleo.event.logging;

import de.fzi.ipe.soboleo.event.EventImpl;


public class BrowseProfile extends EventImpl {
	
	private String taggedPersonURI;
	
	public BrowseProfile(String taggedPersonURI){
		this.taggedPersonURI = taggedPersonURI;
	}
	
	@SuppressWarnings("unused")
	private BrowseProfile() {;}
	
	public String getPersonURI(){
		return taggedPersonURI;
	}

}
