/*
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;

public class AddConnectionCmd  extends ComplexTaxonomyChangeCommand implements ReadableEditorEvent {

	private String fromURI,toURI;
	private SKOS connection;

	
	public AddConnectionCmd(String fromURI, SKOS connection, String toURI) {
		this.fromURI = fromURI;
		this.toURI = toURI;
		this.connection = connection;
	}

	@SuppressWarnings("unused") //only used for gwt serialization
	private AddConnectionCmd() { ;}
	
	public String getFromURI() {
		return fromURI;
	}

	public String getToURI() {
		return toURI;
	}

	public SKOS getConnection() {
		return connection;
	}
	
	@Override
	public String getReadable(Language lan, ClientSideTaxonomy tax) {
		String conceptNameFrom ="";
		String spacesForName = "   ";
		ClientSideConcept conceptFrom = tax.get(getFromURI());
		if (conceptFrom != null) conceptNameFrom = conceptFrom.getBestFitText(SKOS.PREF_LABEL, lan).getString();
		String conceptNameTo ="";
		ClientSideConcept conceptTo = tax.get(getToURI());
		if (conceptTo != null) conceptNameTo= conceptTo.getBestFitText(SKOS.PREF_LABEL, lan).getString();

		if (lan == Language.de) {
			if (connection == SKOS.HAS_BROADER) return spacesForName + getSenderName() + " hat " + conceptNameTo + " als ein weiteres Konzept von "+conceptNameFrom+" eingefügt.";
			if (connection == SKOS.HAS_NARROWER) return spacesForName + getSenderName() + " hat " + conceptNameFrom + " als ein engeres Konzept von "+conceptNameTo+" eingefügt.";			
			if (connection == SKOS.RELATED) return spacesForName + getSenderName() + " hat " + conceptNameFrom + " als ein verwandtes Konzept von "+conceptNameTo+" eingefügt.";			
		}
		else if (lan == Language.es){
			if (connection == SKOS.HAS_BROADER) return spacesForName + getSenderName() + " 	añadía " + conceptNameTo + " como tema más amplios que "+conceptNameFrom+".";
			if (connection == SKOS.HAS_NARROWER) return spacesForName + getSenderName() + " añadía " + conceptNameFrom + " como tema más restringidos que "+conceptNameTo+".";			
			if (connection == SKOS.RELATED) return spacesForName + getSenderName() + " 	añadía " + conceptNameFrom + " como tema relacionado con "+conceptNameTo+".";
		}
		else {
			if (connection == SKOS.HAS_BROADER) return spacesForName + getSenderName() + " added " + conceptNameTo + " as a broader topic to "+conceptNameFrom+".";
			if (connection == SKOS.HAS_NARROWER) return spacesForName + getSenderName() + " added " + conceptNameFrom + " as a narrower topic to "+conceptNameTo+".";			
			if (connection == SKOS.RELATED) return spacesForName + getSenderName() + " added " + conceptNameFrom + " as a related topic to "+conceptNameTo+".";			
		}
		return "";
	}
	
	
}
