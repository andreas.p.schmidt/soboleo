/*
 * FZI - Information Process Engineering 
 * Created on 01.11.2007 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology;

import com.google.gwt.user.client.rpc.IsSerializable;


public enum RDF implements IsSerializable{

	TYPE("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
	
	private final String uri;
	
	RDF(String uri) {
		this.uri = uri;
	}
	
	public String toString() {
		return uri;
	}
	
}
