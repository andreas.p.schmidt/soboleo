/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos.peopletag;

import java.util.Date;
import java.util.Set;


public class AutomaticPersonTag extends PersonTag{

	public enum Operation { 
		BROWSE, SEARCH, EDIT;
	}

	public static class OperationCount {
		private int count;
		private Operation operation;
		
		public int getCount() { return count; }
		public Operation getOperation() { return operation; }
	}
	
	
	public Set<OperationCount> getOperationCounts() {
		//TODO
		return null;
	}
	
	@Override
	public String getConceptID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTagID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUserID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTaggedPersonURI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTaggedPersonURI(String taggedPersonURI) {
		// TODO Auto-generated method stub
		;
	}
}
