/*
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology.primitive;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
/**
 * 
 * @author zach
 * @since 01.02.2009
 */
public class PrimitiveCreateConcept extends PrimitiveTaxonomyChangeCommand{

	private String newURI;
	private LocalizedString initialName;
	
	/**
	 * Constructor
	 * @param initialName as LocalizedString
	 */
	public PrimitiveCreateConcept(LocalizedString initialName) {
		this.initialName = initialName;
	}
	
	@SuppressWarnings("unused") //only used for gwt serialization
	private PrimitiveCreateConcept() { ; }
	
	/**
	 * Only meant to be used by the class that actually creates the concept
	 *  @param newURI String
	 */
	public void setURI(String newURI) {
		this.newURI = newURI;
	}
	
	/**
	 * @return LocalizedString 
	 */
	public LocalizedString getInitialName() {
		return initialName; 
	}
	
	/**
	 * @return String which contains the new URI
	 */
	public String getURI(){
		return newURI;
	}
	
	public String getFromURI() {
		return newURI; 
	}
	
}
