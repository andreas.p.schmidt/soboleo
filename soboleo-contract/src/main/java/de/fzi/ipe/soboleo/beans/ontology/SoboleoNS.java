/*
 * FZI - Information Process Engineering 
 * Created on 02.11.2007 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * @author FZI - Information Process Engineering 
 * Created on 02.11.2007 by zach
 *
 */
public enum SoboleoNS implements IsSerializable{

	/**
	 * This is the URI for the User type. It is the function which a user can have (manager, user,..). Use toString to get the URI!
	 */
	USER_TYPE("http://soboleo.fzi.de/soboleo#User"),
	/**
	 * This is the URI for the default User type. Use toString to get the URI!
	 */
	USER_TYPE_DEFAULT_USER("http://soboleo.fzi.de/soboleo#DefaultUser"),
	/**
	 * This is the URI for the User email address. Use toString to get the URI!
	 */
	USER_EMAIL("http://soboleo.fzi.de/soboleo#has-email"),
	/**
	 * This is the URI for the UserName. Use toString to get the URI!
	 */
	USER_NAME("http://soboleo.fzi.de/soboleo#has-name"),
	/**
	 * This is the URI for the Link of an user. Use toString to get the URI!
	 */
	USER_LINK("http://soboleo.fzi.de/soboleo#has-link"),
	/**
	 * This is the URI for the user password. Use toString to get the URI!
	 */
	USER_PASSWORD("http://soboleo.fzi.de/soboleo#has-password"),
	/**
	 * This is the URI for the User key. Use toString to get the URI!
	 */
	USER_KEY("http://soboleo.fzi.de/soboleo#has-key"),
	/**
	 * This is the URI for the default space of an user. Use toString to get the URI!
	 */
	USER_DEFAULT_SPACE("http://soboleo.fzi.de/soboleo#default-space"),
	/**
	 * This is the URI for the default language of an user. Use toString to get the URI!
	 */
	USER_DEFAULT_LANGUAGE("http://soboleo.fzi.de/soboleo#default-language"),
	
	USER_TAXONOMY("http://soboleo.fzi.de/soboleo#has-taxonomy"),
	USER_LANGUAGE("http://soboleo.fzi.de/soboleo#has-language"),
	
	PEOPLE_PERSON_TAG("http://soboleo.fzi.de/soboleo#PersonTag"),
	PEOPLE_MANUAL_PERSON_TAG("http://soboleo.fzi.de/soboleo#ManualPersonTag"),
	PEOPLE_PERSON_TAGGED_WITH("http://soboleo.fzi.de/soboleo#person-tagged-with"),
	PEOPLE_PERSON_TAG_MEANS("http://soboleo.fzi.de/soboleo#person-tag-means"),
	PEOPLE_PERSON_TAG_DATE("http://soboleo.fzi.de/soboleo#has-tagging-date"),
	PEOPLE_PERSON_TAG_URL("http://soboleo.fzi.de/soboleo#has-url"),
	PEOPLE_EXTERNAL_TAGGED_PERSON("http://soboleo.fzi.de/soboleo#ExternalTaggedPerson"),
	PEOPLE_EXTERNAL_PERSON_CREATED_DATE("http://soboleo.fzi.de/soboleo#has-created-date"),
	PEOPLE_PERSON_NAME("http://soboleo.fzi.de/soboleo#has-name"),
	PEOPLE_PERSON_EMAIL("http://soboleo.fzi.de/soboleo#has-email"),
	
	USER_NS("http://soboleo.fzi.de/soboleo/user#"),
	LOOKED_AT("http://soboleo.fzi.de/soboleo#lookedAt"),
	SEARCHED_FOR("http://soboleo.fzi.de/soboleo#searchedFor"),
	
	TYPE_DATASTORE("http://soboleo.fzi.de/soboleo#Datastore"),
	LAST_ID("http://soboleo.fzi.de/soboleo#lastID"),
	LAST_CONCEPT_ID("http://soboleo.fzi.de/soboleo#lastConceptID"),
	LAST_ANNOTATION_ID("http://soboleo.fzi.de/soboleo#lastAnnotationID"),
	
	DOC_NS("http://soboleo.fzi.de/soboleo/doc#"),
	DOCUMENT("http://soboleo.fzi.de/soboleo#Document"),
	DIALOG("http://soboleo.fzi.de/soboleo#Dialog"),
	DIALOG_WEBDOCUMENT("http://soboleo.fzi.de/soboleo#WebDocumentDialog"),
	DIALOG_CONCEPT("http://soboleo.fzi.de/soboleo#ConceptDialog"),
	
	HAS_TITLE("http://soboleo.fzi.de/soboleo#hasTitle"),
	HAS_URL("http://soboleo.fzi.de/soboleo#hasURL"),
	DATE_ADDED("http://soboleo.fzi.de/soboleo#dateAdded"),
	DATE_LAST_MODIFIED("http://soboleo.fzi.de/soboleo#dateLastModified"),
	IS_ABOUT("http://soboleo.fzi.de/soboleo#isAbout"), 
	HAS_INITIATOR("http://soboleo.fzi.de/soboleo#hasInitiator"),
	HAS_PARTICIPANT("http://soboleo.fzi.de/soboleo#hasParticipant"),
	HAS_CONTENT("http://soboleo.fzi.de/soboleo#hasContent"),
	
	ADDED_BY("http://soboleo.fzi.de/soboleo#addedBy"),
	MODIFIED_BY("http://soboleo.fzi.de/soboleo#modifiedBy"),
	USER_USES_TOPIC("http://soboleo.fzi.de/soboleo#uses");
	
	
	private final String uri;
	
	SoboleoNS(String uri) {
		this.uri = uri;
	}
	
	public String toString() {
		return uri;
	}
	
	public static String makeUserID(String username) {
		return SoboleoNS.USER_NS.toString()+username;
	}
}
