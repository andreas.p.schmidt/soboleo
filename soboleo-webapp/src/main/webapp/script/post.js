document.onload = postDocument();

function postDocument() { 
  var conceptUri = getUrlParameters();
	if(conceptUri != "") { 
	  document.postDocForm.docUri.value = conceptUri;
	}
}
	
function getUrlParameters() {
	var myUrl = window.document.URL.toString();
	
	if (myUrl.indexOf("?") > 0) {
		var line = myUrl.split("?");
		var urlParam = line[1];
		return urlParam;
	}
	else return "";
}