	package de.fzi.ipe.soboleo.test.otherTests;


	import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.Statement;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;

import de.fzi.ipe.soboleo.data.tripleStore.tools.Exporter;

public class Test_Exporter {
	
	static final File tempFile = new File("ExportRDF.rdf");
	static final File tempFileText = new File("ExportText.txt");

		@Test
		public void exportRDF() throws Exception {
			long time = System.currentTimeMillis();
			
			String taxonomyFilePath = "C://test/tax/admin-default";
			MemoryStore memStore = new MemoryStore(new File(taxonomyFilePath));
			memStore.setSyncDelay(1000L);
			
			Repository repository = new SailRepository(memStore);
			repository.initialize();
			RepositoryConnection con = repository.getConnection();
			FileWriter f = new FileWriter(tempFile);
			f.write(Exporter.exportRDFwithContexts(con));
			
			StringBuffer toReturn = new StringBuffer();
			RepositoryResult<Statement> statements = con.getStatements(null, null, null, true);
			while (statements.hasNext()){
				Statement next = statements.next();
				toReturn.append(next.getSubject() + ", "+ next.getPredicate() + ", " +  next.getObject() +", "+next.getContext());
				toReturn.append(System.getProperty("line.separator"));
			}
			statements.close();
			FileWriter f2 = new FileWriter(tempFileText);
			f2.write(toReturn.toString());
			
			long duration = System.currentTimeMillis() - time;
			System.out.println("Duration exportRDF (<20000): "+duration);
			
				
		}
		
		@BeforeClass
		public static void setUpBeforeClass() throws Exception {
			//check if temp directory already exist - fail if it does becomes we want to be 
			//able to simply delete the directory after we're done
			if (tempFile.exists()) {
				tempFile.delete();
			}
			if (tempFileText.exists()){
				tempFileText.delete();
			}
		}

		@AfterClass
		public static void tearDownAfterClass() throws Exception {
		}

		@Before
		public void setUp() throws Exception {
		}

		@After
		public void tearDown() throws Exception {
		}

	
}
