/*
 * FZI - Information Process Engineering 
 * Created on 20.02.2009 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;


import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString.Language;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddTextCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.CreateConceptCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.queries.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceImpl;
import de.fzi.ipe.soboleo.space.eventBus.EventBus;
import de.fzi.ipe.soboleo.space.events.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;
import de.fzi.ipe.soboleo.tests.regressionTests.util.RandomChangeGenerator;

public class Test_ClientSideTaxonomy extends Assert {

	
	static final File tempDirectory = new File("ClientSideTaxonomy-testDirectory");

	private Space space; 


	/**
	 * Stress test - just very quickly generates 2000 random changes in 
	 * 10 threads to see if anything brakes. 
	 */
	@Test
	public void stressTestTaxonomy() throws IOException, EventPermissionDeniedException, InterruptedException {
		long timeBegin = System.currentTimeMillis();
		
		EventBus eb = space.getEventBus();
		EventSenderCredentials cred = EventSenderCredentials.SERVER;
		
		RandomChangeGenerator[] generators = new RandomChangeGenerator[10];
		for (int i=0;i<generators.length;i++) generators[i] = new RandomChangeGenerator(i,eb,180);
		
		ClientSideTaxonomy tax = (ClientSideTaxonomy) eb.executeQuery(new GetClientSideTaxonomy(false), cred);
		ClientSideTaxonomy tax2 = (ClientSideTaxonomy) eb.executeQuery(new GetClientSideTaxonomy(true), cred);
		long timeBeginChanges = System.currentTimeMillis();
		for (RandomChangeGenerator r: generators) r.start();
		ClientSideTaxonomy tax3 = (ClientSideTaxonomy) eb.executeQuery(new GetClientSideTaxonomy(true), cred);
		for (RandomChangeGenerator r: generators) r.join();
		long durationChangeExecution = System.currentTimeMillis()-timeBeginChanges;
		System.out.println("Duration Taxonomy Stress Test - only execution (<1000): "+durationChangeExecution);
		space.close();
		
		setUp();
		eb = space.getEventBus();
		
		ClientSideTaxonomy tax4 = (ClientSideTaxonomy) eb.executeQuery(new GetClientSideTaxonomy(true), cred);
		
		assertTrue(tax3.hasEqualValues(tax2));
		assertTrue(tax4.hasEqualValues(tax3));
		assertFalse(tax4.hasEqualValues(tax));
		assertTrue(tax4.getConcepts().size() == 302);

		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration Taxonomy Stress Test (<2000): "+duration); //well, its ~ 2000 changes to the taxonomy + closing and reopening
	}
	
	@Test
	public void testTaxonomyMethods() throws IOException, EventPermissionDeniedException, InterruptedException{
		long timeBegin = System.currentTimeMillis();
		EventBus eb = space.getEventBus();
		String uri1 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("1")), EventSenderCredentials.SERVER);
		String uri2 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("2")), EventSenderCredentials.SERVER);
		String uri3 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("3")), EventSenderCredentials.SERVER);
		String uri4 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("4")), EventSenderCredentials.SERVER);
		
		
		eb.executeCommandNow(new AddConnectionCmd(uri2,SKOS.HAS_BROADER,uri1), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri4, SKOS.HAS_BROADER, uri2), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri3, SKOS.HAS_BROADER, uri1), EventSenderCredentials.SERVER);
	
		eb.executeCommandNow(new AddTextCmd(uri1, SKOS.PREF_LABEL, new LocalizedString("PrefLabel",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri2, SKOS.PREF_LABEL, new LocalizedString("AltLabel",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri3, SKOS.PREF_LABEL, new LocalizedString("PrefLabel",Language.es)), EventSenderCredentials.SERVER);

		eb.executeCommandNow(new AddTextCmd(uri1, SKOS.ALT_LABEL, new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri1, SKOS.ALT_LABEL, new LocalizedString("AltLabel-de",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri1, SKOS.ALT_LABEL, new LocalizedString("AltLabel2")), EventSenderCredentials.SERVER);
		
		eb.executeCommandNow(new AddTextCmd(uri2, SKOS.ALT_LABEL, new LocalizedString("AltLabel-de",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri2, SKOS.ALT_LABEL, new LocalizedString("AltLabel2")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri2, SKOS.ALT_LABEL, new LocalizedString("PrefLabel", Language.es)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri2, SKOS.ALT_LABEL, new LocalizedString("PrefLabel", Language.de)), EventSenderCredentials.SERVER);
		
		eb.executeCommandNow(new AddTextCmd(uri1, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri1, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel-de",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri1, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel2")), EventSenderCredentials.SERVER);
		
		eb.executeCommandNow(new AddTextCmd(uri2, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel2")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri4, SKOS.HIDDEN_LABEL, new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);
		
		eb.executeCommandNow(new AddTextCmd(uri1, SKOS.NOTE, new LocalizedString("note")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri1, SKOS.NOTE, new LocalizedString("Notiz",Language.de)), EventSenderCredentials.SERVER);
		
		eb.executeCommandNow(new AddTextCmd(uri2, SKOS.NOTE, new LocalizedString("Notiz",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri3, SKOS.NOTE, new LocalizedString("AltLabel",Language.de)), EventSenderCredentials.SERVER);
		
		ClientSideTaxonomy tax = (ClientSideTaxonomy) eb.executeQuery(new GetClientSideTaxonomy(false), EventSenderCredentials.SERVER);
		//created 4 concepts
		assertTrue(tax.getConcepts().size()==4);
		//only uri1 is root concept
		assertTrue(tax.getRootConcepts().size()==1);
		//uri2 has uri4 as subconcept
		assertTrue(tax.getAllSubconceptURIs(uri2).size()==1);
		//uri2 has uri1 as superconcept
		assertTrue(tax.getAllSuperconceptURIs(uri2).size()==1);
		//uri2 has "AltLabel" as German prefLabel and uri1 has "AltLabel" as English altLabel; hiddenlabels and notes should not be considered
		assertTrue(tax.getExtractedConcepts("AltLabel").size()==2);
		Set<String> extracted = tax.getExtractedConceptURIs("AltLabel");
		assertTrue(extracted.size()==2);
		assertTrue(extracted.contains(uri1) && extracted.contains(uri2));
		//uri1 has "PrefLabel" as German prefLabel and uri3 has "PrefLabel" as Spanish prefLabel; Spanish altLabel and German hiddenLabel of uri2 should not be considered
		assertTrue(tax.getConceptsForPrefLabel("PrefLabel").size()==2);
		//only uri1 has "PrefLabel" as German prefLabel
		assertTrue(tax.getConceptForPrefLabel(new LocalizedString("PrefLabel",Language.de)).getURI().equals(uri1));
		//only uri2 has "AltLabel" as prefLabel
		assertTrue(tax.getConceptsForPrefLabel("AltLabel").size()==1);	
		//none has "AltLabel" as English prefLabel
		assertTrue(tax.getConceptForPrefLabel(new LocalizedString("AltLabel",Language.en))==null);
		//uri1 has "AltLabel" as English altLabel, uri2 has it as German prefLabel, uri3 has it as German note, uri4 has it as English hiddenLabel
		assertTrue(tax.getConceptsForLabel("AltLabel").size()==4);
		//uri2 has it as German prefLabel, uri3 as German note
		assertTrue(tax.getConceptsForLabel(new LocalizedString("AltLabel",Language.de)).size()==2);
		
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testTaxonomyMethods(<50): "+duration);
	}
	
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//check if temp directory already exist - fail if it does becomes we want to be 
		//able to simply delete the directory after we're done
		if (tempDirectory.exists()) {
			throw new IOException("Temp Directory already exists!");
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		;
	}
	
	//careful with this method .. pass the file system root and it can really clean a hard-drive in no time ;)
	private static void deleteRecursive(File file) {
		if (file.isDirectory()) {
			for (File f: file.listFiles()) deleteRecursive(f);
		}
		file.delete();
	}
	
	@Before
	public void setUp() throws IOException, EventPermissionDeniedException {
		space = new SpaceImpl(tempDirectory,"testSpace",null);
	}

	@After
	public void tearDown() throws Exception {
		space.close();
		try {
			if (tempDirectory.exists()) {
				deleteRecursive(tempDirectory);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	

}
