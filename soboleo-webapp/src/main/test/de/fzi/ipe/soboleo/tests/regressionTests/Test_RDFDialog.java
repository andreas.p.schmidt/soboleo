/*
 * FZI - Information Process Engineering 
 * Created on 23.07.2009 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.Resource;

import de.fzi.ipe.soboleo.data.tripleStore.TripleStore;
import de.fzi.ipe.soboleo.data.tripleStore.TripleStoreFactory;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.SesameTripleStoreFactory;
import de.fzi.ipe.soboleo.dialog.clientSide.Dialog;
import de.fzi.ipe.soboleo.dialog.clientSide.WebdocumentDialog;
import de.fzi.ipe.soboleo.dialog.eventBusAdaptor.RDFConceptDialog;
import de.fzi.ipe.soboleo.dialog.eventBusAdaptor.RDFDialog;
import de.fzi.ipe.soboleo.dialog.eventBusAdaptor.RDFDialogDatabase;
import de.fzi.ipe.soboleo.dialog.eventBusAdaptor.RDFWebDocumentDialog;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SoboleoNS;

public class Test_RDFDialog extends Assert{

	static final File tempDirectory = new File("RDFDialog-testDirectory");

	private RDFDialogDatabase documents; 
	private TripleStore tripleStore;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//check if temp directory already exist - fail if it does becomes we want to be 
		//able to simply delete the directory after we're done
		if (tempDirectory.exists()) throw new IOException("Temp Directory already exists!");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (tempDirectory.exists()) {
			for (File f:tempDirectory.listFiles()) {
				f.delete();
			}
			tempDirectory.delete();
		}
	}
	
	@Before
	public void setUp() throws Exception {
		TripleStoreFactory factory = new SesameTripleStoreFactory();
		tripleStore = factory.getTripleStore(tempDirectory,"space-test");
		documents = new RDFDialogDatabase(tripleStore);
	}

	@After
	public void tearDown() throws Exception {
		for (RDFDialog d:documents.getAllDocuments()) documents.deleteDocument(d.getURI().toString());
		tripleStore.close();
	}

	@Test 
	public void testAddDeleteCache() throws IOException {
		long timeBegin = System.currentTimeMillis();
		RDFDialog document1 = documents.createDocument(SoboleoNS.DIALOG_WEBDOCUMENT.toString());
		assertTrue(document1 instanceof RDFWebDocumentDialog);
		
		RDFDialog document2 = documents.getDocument(document1.getURI()); 
		assertTrue(document1 == document2);	
		
		document2.addParticipant("http://someUser");
		document2.addParticipant("http://anotherUser");
		assertTrue(document1.getParticipants().size() == 2);
		assertTrue(document1.getParticipants().contains("http://someUser"));
		document1.removeParticipant("http://someUser");
		assertTrue(document2.getParticipants().size() == 1);
		assertFalse(document2.getParticipants().contains("http://someUser"));
		
		document2.setInitiator("http://someUser");
		assertTrue(document1.getInitiator().equals("http://someUser"));
		
		assertNull(document1.getAbout());
		document1.setAbout("http://docUri");
		assertTrue(document2.getAbout().equals("http://docUri"));
		
		assertNull(document1.getContent());
		document1.setContent("testContent");
		assertTrue(document2.getContent().equals("testContent"));
		
		assertNull(document2.getTitle());
		document2.setTitle("Titel");
		assertTrue(document1.getTitle().equals("Titel"));
		
		RDFDialog document3 = documents.createDocument(SoboleoNS.DIALOG_CONCEPT.toString());
		assertTrue(document3 instanceof RDFConceptDialog);
		
		documents.deleteDocument(document1.getURI().toString());
		assertTrue(documents.getAllDocuments().size()==1);
		documents.deleteDocument(document3.getURI().toString());
		assertTrue(documents.getAllDocuments().size()==0);
		assertNull(documents.getDocument(document1.getURI().toString()));
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration AddDeleteCache(<50): "+duration);	
	}
	
	@Test
	public void testAddRemoveTags() throws IOException {
		long timeBegin = System.currentTimeMillis();
		int statementCountEmpty = getStatementCount();
		
		RDFDialog doc = documents.createDocument(SoboleoNS.DIALOG_WEBDOCUMENT.toString());
		doc.addTag("http://someUser", "http://id1");
		doc.addTag("http://someUser", "http://id2");
		doc.addTag("http://someOtherUser","http://id2");
		assertTrue(doc.getTags().size()==3);

		int withoutTag = getStatementCount();
		doc.addTag("http://toRemoveUser","http://id2");
		assertTrue(getStatementCount()>withoutTag);
		Dialog cDoc = doc.getClientSideDocument();
		assertTrue(cDoc instanceof WebdocumentDialog);
		String tagID = cDoc.getUserTags("http://toRemoveUser").iterator().next().getTagID();
		doc.removeTag(tagID);
		assertTrue(getStatementCount()==withoutTag);
		cDoc = doc.getClientSideDocument();
		assertTrue(cDoc.getUserTags("http://toRemoveUser").size()==0);
		
		documents.deleteDocument(cDoc.getURI());
		assertTrue("Delete document seems to leave stuff",getStatementCount()==statementCountEmpty); 			
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testAddTags(<50): "+duration);	
	}
	
	@Test
	public void query() throws IOException{
		RDFDialog document1 = documents.createDocument(SoboleoNS.DIALOG_WEBDOCUMENT.toString());
		document1.setAbout("http://docUri");
		
		RDFDialog document2 = documents.createDocument(SoboleoNS.DIALOG_WEBDOCUMENT.toString());
		document2.setAbout("http://doc");
		
		RDFDialog document3 = documents.createDocument(SoboleoNS.DIALOG_CONCEPT.toString());
		document3.setAbout("http://docUri");
		
		RDFDialog document4 = documents.createDocument(SoboleoNS.DIALOG_WEBDOCUMENT.toString());
		document4.setAbout("http://docUri");
		
		assertTrue(documents.getDialogsByAbout("http://docUri", SoboleoNS.DIALOG_WEBDOCUMENT.toString()).size() == 2);
		
		}
	
	private int getStatementCount() throws IOException{
		return tripleStore.getStatementList((Resource)null, null, null).size();
	}
	
}
