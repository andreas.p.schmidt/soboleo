/*
 * FZI - Information Process Engineering 
 * Created on 04.03.2009 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.data.tripleStore.TripleStoreFactory;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.SesameTripleStoreFactory;
import de.fzi.ipe.soboleo.server.user.User;
import de.fzi.ipe.soboleo.server.user.UserDatabase;

public class Test_UserDB extends Assert{

	static final File tempDirectory = new File("userTest");
	
	UserDatabase udb;
	
	@Test
	public void userDBTest() throws Exception {
		long time = System.currentTimeMillis();
		
		//create users
		User u1 = udb.createNewUser();
		User u2 = udb.createNewUser();
		
		//basic properties
		assertNull(u1.getName());
		u1.setName("a name!");
		assertTrue(u1.getName().equals("a name!"));
		assertNull(u2.getName());
		
		assertNull(u1.getEmail());
		u1.setEmail("zach@fzi.de");
		assertTrue(u1.getEmail().equals("zach@fzi.de"));

		assertNull(u2.getLink());
		u2.setLink("http://zach");
		assertTrue(u2.getLink().equals("http://zach"));
		
		//password hash
		u2.setPassword("voll geheim?");
		assertTrue(u2.isPassword("voll geheim?"));
		assertFalse(u2.isPassword("Voll geheim?"));

		//retrieve users
		User u3 = udb.getUserForEmail("zach@fzi.de");
		assertTrue(u3==u1);
		Set<User> allUser = udb.getAllUsers();
		assertTrue(allUser.contains(u1));
		assertTrue(allUser.contains(u2));

		//keys
		String key = u1.getKey();
		User u4 = udb.getUserForKey(key);
		assertTrue(u1 == u4);
		u4 = udb.getUserForKey(key);
		assertTrue(u1 == u4);
		assertNull(udb.getUserForKey(key+" "));
		
		//spaces
		u1.addSpaceIdentifier("space1");
		u2.addSpaceIdentifier("space2");
		u2.addSpaceIdentifier("space1");
		assertTrue(udb.getUserForSpace("space2").contains(u2));
		assertTrue(udb.getUserForSpace("space1").size() == 2);
		u2.removeSpaceIdentifier("space1");
		assertTrue(udb.getUserForSpace("space1").size() == 1);
		
		//delete
		udb.deleteUser(u1);
		assertTrue(udb.getUserForSpace("space1").size() == 0);
		assertNull(udb.getUserForKey(key));
		
		long duration = System.currentTimeMillis() - time;
		System.out.println("Duration userDBTest (<50): "+duration);
	}
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//check if temp directory already exist - fail if it does becomes we want to be 
		//able to simply delete the directory after we're done
		if (tempDirectory.exists()) {
			throw new IOException("Temp Directory already exists!");
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		;
	}
	
	//careful with this method .. pass the file system root and it can really clean a hard-drive in no time ;)
	private static void deleteRecursive(File file) {
		if (file.isDirectory()) {
			for (File f: file.listFiles()) deleteRecursive(f);
		}
		file.delete();
	}
	
	@Before
	public void setUp() throws IOException {
		TripleStoreFactory f = new SesameTripleStoreFactory();
		udb = new UserDatabase(tempDirectory,f);
	}

	@After
	public void tearDown() throws Exception {
		udb.close();
		try {
			if (tempDirectory.exists()) {
				deleteRecursive(tempDirectory);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
}
