/*
 * FZI - Information Process Engineering 
 * Created on 05.02.2010 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;

import java.io.File;
import java.io.IOException;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideConcept;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.CreateConceptCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.RemoveConceptCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.queries.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceImpl;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.space.eventBus.EventBus;
import de.fzi.ipe.soboleo.space.events.CommandEvent;
import de.fzi.ipe.soboleo.space.events.Event;
import de.fzi.ipe.soboleo.space.events.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;
import de.fzi.ipe.soboleo.space.events.ReadableEvent;
import de.fzi.ipe.soboleo.space.logging.queries.GetRecentEvents;
import de.fzi.ipe.soboleo.webdocument.lucene.queries.GetSearchResult;
import de.fzi.ipe.soboleo.webdocument.lucene.LuceneResult;

public class Test_LoggingHistory extends Assert {

	static final File tempDirectory = new File("LoggingHistory-testDirectory");

	private Space space; 

		
	@Test
	public void testLoggingHistory() throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
				
		//first: enable logging
		space.setProperty(SpaceProperties.EVENT_HISTORY_LOGGING,"true"); 
		space.close();
		space = new SpaceImpl(tempDirectory,"testSpace",null);
		
		EventBus eb = space.getEventBus();
		
		List<Event> events = (List<Event>) eb.executeQuery(new GetRecentEvents(0), EventSenderCredentials.SERVER);		
		assertTrue(events.size() == 0);
		
		aCoupleOfChanges(eb);

		events = (List<Event>) eb.executeQuery(new GetRecentEvents(0), EventSenderCredentials.SERVER);
		assertTrue(events.size() == 20);

		events = (List<Event>) eb.executeQuery(new GetRecentEvents(20), EventSenderCredentials.SERVER);
		assertTrue(events.get(0).getId() == 30);
		assertTrue(events.size() == 13);
		
		events = (List<Event>) eb.executeQuery(new GetRecentEvents(0,CommandEvent.class), EventSenderCredentials.SERVER);
		assertTrue(events.size() == 15);
		
		events = (List<Event>) eb.executeQuery(new GetRecentEvents(10,GetSearchResult.class), EventSenderCredentials.SERVER);
		assertTrue(events.size() == 8);

		events = (List<Event>) eb.executeQuery(new GetRecentEvents(10,ReadableEvent.class), EventSenderCredentials.SERVER);

		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testConceptConnection(<1500): "+duration);
	}

	private void aCoupleOfChanges(EventBus eb) throws EventPermissionDeniedException {
		String uri1 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("1")), EventSenderCredentials.SERVER);
		String uri2 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("2")), EventSenderCredentials.SERVER);
		String uri3 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("3")), EventSenderCredentials.SERVER);
		String uri4 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("4")), EventSenderCredentials.SERVER);

		eb.executeCommandNow(new AddConnectionCmd(uri2,SKOS.HAS_BROADER,uri1), EventSenderCredentials.SERVER);			
		eb.executeCommandNow(new AddConnectionCmd(uri4, SKOS.HAS_BROADER, uri2), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri3, SKOS.HAS_BROADER, uri1), EventSenderCredentials.SERVER);
	
		eb.executeCommandNow(new AddConnectionCmd(uri4,SKOS.HAS_BROADER,uri3), EventSenderCredentials.SERVER);
		getConcept(eb, uri1);
		eb.executeCommandNow(new RemoveConnectionCmd(uri4,SKOS.HAS_BROADER,uri3), EventSenderCredentials.SERVER);
		
		//add a few related connections
		eb.executeCommandNow(new AddConnectionCmd(uri2,SKOS.RELATED,uri3), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri4,SKOS.RELATED,uri1), EventSenderCredentials.SERVER);
		ClientSideConcept c1 = getConcept(eb,uri1);
		assertTrue(c1.getConnected(SKOS.RELATED).size()==1);
		
		//attempt a few cycles
		eb.executeCommandNow(new RemoveConnectionCmd(uri4,SKOS.HAS_BROADER,uri2), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri1, SKOS.HAS_BROADER,uri4), EventSenderCredentials.SERVER); 

		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);

		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
		eb.executeQuery(new GetSearchResult("egal",LuceneResult.ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);

		
		//test delete
		eb.executeCommandNow(new RemoveConceptCmd(uri4),EventSenderCredentials.SERVER);
		eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("4")), EventSenderCredentials.SERVER);
		c1 = getConcept(eb, uri1);
	
	}
		
		
		
	private static ClientSideConcept getConcept(EventBus eb, String uri) throws EventPermissionDeniedException {
		ClientSideTaxonomy tax = (ClientSideTaxonomy) eb.executeQuery(new GetClientSideTaxonomy(false), EventSenderCredentials.SERVER);
		ClientSideConcept clientSideConcept = tax.get(uri);
		return clientSideConcept;
	}	
		
		
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		if (tempDirectory.exists()) {
			deleteRecursive(tempDirectory);
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		;
	}
	
	//carefull with this method .. it can really clean a harddrive in no time ;)
	private static void deleteRecursive(File file) {
		if (file.isDirectory()) {
			for (File f: file.listFiles()) deleteRecursive(f);
		}
		file.delete();
	}
	
	@Before
	public void setUp() throws Exception {
		space = new SpaceImpl(tempDirectory,"testSpace",null);
	}

	@After
	public void tearDown() throws Exception {
		space.close();
		try {
			if (tempDirectory.exists()) {
				deleteRecursive(tempDirectory);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
