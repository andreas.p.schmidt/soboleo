/*
 * Class for doing some basic testing on the gardening recommendations stuff. 
 * 
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;


import java.io.File;
import java.io.IOException;
import java.util.Collection;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.gardening.events.GetGardeningRecommendationsForConcept;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptLabelIdenticalProblem;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptWithoutDescription;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideConcept;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddTextCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.CreateConceptCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.queries.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceImpl;
import de.fzi.ipe.soboleo.space.eventBus.EventBus;
import de.fzi.ipe.soboleo.space.events.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;

public class Test_GardeningRecommendations extends Assert{

	static final File tempDirectory = new File("GardeningRecommendations-testDirectory");

	private Space space; 


	@SuppressWarnings("unchecked")
	@Test 
	public void testSimpleHeuristics() throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
		
		EventBus eb = space.getEventBus();
		String uri1 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("1")), EventSenderCredentials.SERVER);
		String uri2 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("2")), EventSenderCredentials.SERVER);
		String uri3 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("3")), EventSenderCredentials.SERVER);
		String uri4 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("4")), EventSenderCredentials.SERVER);

		eb.executeCommandNow(new AddConnectionCmd(uri2,SKOS.HAS_BROADER,uri1), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri4, SKOS.HAS_BROADER, uri2), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri3, SKOS.HAS_BROADER, uri1), EventSenderCredentials.SERVER);
	
		eb.executeCommandNow(new AddConnectionCmd(uri4,SKOS.HAS_BROADER,uri3), EventSenderCredentials.SERVER);
		getConcept(eb, uri1);
		eb.executeCommandNow(new RemoveConnectionCmd(uri4,SKOS.HAS_BROADER,uri3), EventSenderCredentials.SERVER);
		
		//add a few related connections
		eb.executeCommandNow(new AddConnectionCmd(uri2,SKOS.RELATED,uri3), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri4,SKOS.RELATED,uri1), EventSenderCredentials.SERVER);
		
		//add a few labels
		eb.executeCommandNow(new AddTextCmd(uri2,SKOS.ALT_LABEL,new LocalizedString("1")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri3,SKOS.HIDDEN_LABEL,new LocalizedString("1")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(uri4,SKOS.ALT_LABEL,new LocalizedString("1",LocalizedString.Language.fr)), EventSenderCredentials.SERVER);

		Collection<GardeningRecommendation> recommendation1 = (Collection<GardeningRecommendation>) eb.executeQuery(new GetGardeningRecommendationsForConcept(uri1), EventSenderCredentials.SERVER);
		Collection<GardeningRecommendation> recommendation2 = (Collection<GardeningRecommendation>) eb.executeQuery(new GetGardeningRecommendationsForConcept(uri2), EventSenderCredentials.SERVER);
		Collection<GardeningRecommendation> recommendation3 = (Collection<GardeningRecommendation>) eb.executeQuery(new GetGardeningRecommendationsForConcept(uri3), EventSenderCredentials.SERVER);
		Collection<GardeningRecommendation> recommendation4 = (Collection<GardeningRecommendation>) eb.executeQuery(new GetGardeningRecommendationsForConcept(uri4), EventSenderCredentials.SERVER);
		
		assertTrue(recContainClass(recommendation1, ConceptWithoutDescription.class));
		assertTrue(recContainClass(recommendation2, ConceptWithoutDescription.class));
		assertTrue(recContainClass(recommendation3, ConceptWithoutDescription.class));
		assertTrue(recContainClass(recommendation4, ConceptWithoutDescription.class));

		assertTrue(recContainClass(recommendation1, ConceptLabelIdenticalProblem.class));
		assertTrue(recContainClass(recommendation2, ConceptLabelIdenticalProblem.class));
		assertTrue(recContainClass(recommendation3, ConceptLabelIdenticalProblem.class));
		assertFalse(recContainClass(recommendation4, ConceptLabelIdenticalProblem.class));
		
//		Collection<GardeningRecommendation> recommendations = (Collection<GardeningRecommendation>) eb.executeQuery(new GetGardeningRecommendations(true), EventSenderCredentials.SERVER);
//		for (GardeningRecommendation rec: recommendations) {
//			System.out.println(rec.toString());
//		}
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testSimpleHeuristics(<50): "+duration);
	}
	
	@SuppressWarnings("unchecked")
	private static boolean recContainClass(Collection<GardeningRecommendation> recommendations, Class recClass) {
		for (GardeningRecommendation rec: recommendations) {
			if (rec.getClass().equals(recClass)) return true;
		}
		return false;
	}
	
	private static ClientSideConcept getConcept(EventBus eb, String uri) throws EventPermissionDeniedException {
		ClientSideTaxonomy tax = (ClientSideTaxonomy) eb.executeQuery(new GetClientSideTaxonomy(false), EventSenderCredentials.SERVER);
		ClientSideConcept clientSideConcept = tax.get(uri);
		return clientSideConcept;
	}	
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//check if temp directory already exist - fail if it does becomes we want to be 
		//able to simply delete the directory after we're done
		if (tempDirectory.exists()) {
			throw new IOException("Temp Directory already exists!");
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		;
	}
	
	//carefull with this method .. it can really clean a harddrive in no time ;)
	private static void deleteRecursive(File file) {
		if (file.isDirectory()) {
			for (File f: file.listFiles()) deleteRecursive(f);
		}
		file.delete();
	}
	
	@Before
	public void setUp() throws Exception {
		space = new SpaceImpl(tempDirectory,"testSpace",null);
	}

	@After
	public void tearDown() throws Exception {
		space.close();
		try {
			if (tempDirectory.exists()) {
				deleteRecursive(tempDirectory);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	

}
