package de.fzi.ipe.soboleo.annotate.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;


public class AnnotatePanel implements EntryPoint {

    public void onModuleLoad() {

    	
    	AnnotateMain am=new AnnotateMain(null);    	
    	RootPanel.get("annotate").add(am);	
    	
    	focusWindow();
    }

    private static native void focusWindow() /*-{
    	return $wnd.focus();
	}-*/;

}
