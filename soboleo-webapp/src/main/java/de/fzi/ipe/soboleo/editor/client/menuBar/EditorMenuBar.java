package de.fzi.ipe.soboleo.editor.client.menuBar;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.MenuBar;

import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EditorEventListener;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.editor.client.components.CreateConceptPopup;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.editor.CopyConceptEvent;
import de.fzi.ipe.soboleo.event.editor.CutConceptEvent;
import de.fzi.ipe.soboleo.event.editor.PasteConceptEvent;
import de.fzi.ipe.soboleo.event.editor.PredeleteConceptEvent;
import de.fzi.ipe.soboleo.event.editor.ShowAllRecommendationsEvent;
import de.fzi.ipe.soboleo.event.editor.ShowDialogsEvent;
import de.fzi.ipe.soboleo.event.editor.ShowRecommendationsForConceptEvent;
import de.fzi.ipe.soboleo.event.editor.StartDialogEvent;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class EditorMenuBar extends MenuBar implements EditorEventListener {

	private EventDistributor eventDistrib;
	private MenuBar open,file,edit;
	private Set<String> currentTaxonomies = new HashSet<String>();
	
	// Internationalisierung
	private EditorConstants constants = GWT.create(EditorConstants.class);
	
	
	public EditorMenuBar(EventDistributor eventDistrib) {
		super();
		this.eventDistrib = eventDistrib;
		eventDistrib.addListener(this);

//		file = makeFileMenuBar();
//		addItem("File", file);
		edit = makeEditMenuBar();
		addItem(constants.edit(), edit);
		
		if(ClientUtil.hasSpaceDialogSupport()){
			MenuBar dialog = makeDialogMenuBar();
			addItem(constants.discuss(), dialog);
		}
		MenuBar recs = makeRecommendationMenuBar();
		addItem(constants.improveIt(), recs);
	}


	private MenuBar makeEditMenuBar() {
		edit = new MenuBar(true);
		
		makeCutConceptItem();
		makeCopyConceptItem();
		makePasteConceptItem();
		makeAddConceptItem();
		makeDeleteConceptItem();
		
		return edit;
	}
	
	private MenuBar makeRecommendationMenuBar(){
		MenuBar recommendation = new MenuBar(true);
		Command allRecsCmd = new Command(){
			@Override
			public void execute() {
				eventDistrib.notify(new ShowAllRecommendationsEvent());
			}
		};
		recommendation.addItem(constants.showAllRecs(), allRecsCmd);
		Command conceptRecsCmd = new Command(){
			@Override
			public void execute() {
				eventDistrib.notify(new ShowRecommendationsForConceptEvent());
			}
		};
		recommendation.addItem(constants.showConceptRecs(), conceptRecsCmd);
		
		return recommendation;
	}

	private MenuBar makeDialogMenuBar(){
		MenuBar dialog = new MenuBar(true);
		Command startCmd = new Command(){
			public void execute(){
				eventDistrib.notify(new StartDialogEvent());
			}
		};
		dialog.addItem(constants.startMaturingDialog(), startCmd);
		Command openDialogsCmd = new Command(){
			public void execute(){
				eventDistrib.notify(new ShowDialogsEvent());
			}
		};
		dialog.addItem(constants.showPerformedDialogs(), openDialogsCmd);
		return dialog;
	}
	
	private MenuBar makeFileMenuBar() {
		Command cmd = new Command() {
		      public void execute() {
		        ;
		      }
		 };
		
		file = new MenuBar(true);
		open = new MenuBar(true);
		open.addItem(constants.loading(),cmd);
		file.addItem(constants.open(),open);
		return file;
	}


		
	private void makePasteConceptItem() {
		Command newCmd = new Command() {
			public void execute() {
				eventDistrib.notify(new PasteConceptEvent());
			}
		};
		edit.addItem(constants.pasteConcepts(),newCmd);
	}

	private void makeCutConceptItem() {
		Command newCmd = new Command() {
			public void execute() {
				eventDistrib.notify(new CutConceptEvent());
			}
		};
		edit.addItem(constants.cutConcepts(),newCmd);
	}
	
	private void makeCopyConceptItem() {
		Command newCmd = new Command() {
			public void execute() {
				eventDistrib.notify(new CopyConceptEvent());
			}
		};
		edit.addItem(constants.copyConcepts(),newCmd);
	}
	
	private void makeAddConceptItem() {
		Command newCmd = new Command() {
			public void execute() {
				new CreateConceptPopup();
			}
		};
		edit.addItem(constants.addConcept(),newCmd);
	}
	

	private void makeDeleteConceptItem() {
		Command newCmd = new Command() {
			public void execute() {
				EventDistributor.getInstance().notify(new PredeleteConceptEvent());
			}
		};
		edit.addItem(constants.deleteConcept(),newCmd);
	}
	
	private class LoadTaxonomyCommand implements Command {
		
		private String taxonomyUri;

		public LoadTaxonomyCommand(String taxonomyUri) {
			this.taxonomyUri = taxonomyUri;
		}
		
		public void execute() {
			//TODO right now this is not implemented
			taxonomyUri.toString(); //or something
		}		
		
	}

	public void notify(Event event) {
		//TODO load list of taxonomies?
	}
	
	
}
