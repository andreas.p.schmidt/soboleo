package de.fzi.ipe.soboleo.servlets.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.server.user.UserInput;
import de.fzi.ipe.soboleo.user.UserDatabase;

@Controller
@RequestMapping("/admin/user")
public class UserManagementController {
	@Autowired
	protected Server server;

	@RequestMapping("spaceDatabaseDump")
	protected ModelAndView spaceDBDump() throws IOException {
		return new ModelAndView("admin/dump", "dump", server.getUserDatabase()
				.getTripleStoreDump().replace("\n", "<br/>"));
	}

	@RequestMapping("userDelete")
	protected String deleteUser(@RequestParam("uri") String uri,
			@RequestParam(value = "really", required = false) String really) throws IOException {

		if (really != null) {
			UserDatabase userdb = server.getUserDatabase();
			de.fzi.ipe.soboleo.user.User user = userdb.getUser(uri);
			userdb.deleteUser(user);
		}

		return "admin/user/userDelete";
	}

	@RequestMapping("userList")
	protected ModelAndView listUser() throws IOException
	{
		ModelMap m = new ModelMap();
		m.put("userCount", server.getUserDatabase().getAllUsers().size());
		m.put("users", server.getUserDatabase().getAllUsers());
		
		return new ModelAndView("admin/user/userList",m);
	}

	@RequestMapping("userEdit")
	protected ModelAndView editUser(
			@RequestParam(value="uri",required=false) String uri,
			@RequestParam(value="save",required=false) String save,
			HttpServletRequest request) throws IOException
	{
		  UserDatabase userDatabase = server.getUserDatabase();
		  UserInput userInput = new UserInput();
		  String validation = null;
		  
		  if (uri != null) {
		     userInput.load(userDatabase.getUser(uri),server);
		  }
		  if (save!=null) {
		     userInput.load(request);
		     validation = userInput.validate(userDatabase);
		     if (validation == null) {
		        userInput.save(userDatabase);
		        return new ModelAndView("redirect:" + server.getServerRelativeURL());
		     }
		  }
		  
		 ModelMap m = new ModelMap();
		 m.put("validation",validation);
		 m.put("userInput", userInput);
		  
		 return new ModelAndView("admin/user/userEdit",m);
	}
}
