package de.fzi.ipe.soboleo.editor.client.taxonomyArea;

import java.util.List;

import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.allen_sauer.gwt.dnd.client.drop.AbsolutePositionDropController;
import com.allen_sauer.gwt.dnd.client.drop.DropController;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MouseListener;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.TreeListener;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EditorEventListener;
import de.fzi.ipe.soboleo.editor.client.EditorUtil;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.editor.client.components.CreateConceptPopup;
import de.fzi.ipe.soboleo.editor.client.components.EditorPopup;
import de.fzi.ipe.soboleo.editor.client.components.StartDialogPopup;
import de.fzi.ipe.soboleo.editor.client.recommendationArea.RecommendationPopUp;
import de.fzi.ipe.soboleo.editor.client.taxonomyArea.DragNDropTaxonomyTree.DragNDropListener;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.editor.ConceptSelectionChangedEvent;
import de.fzi.ipe.soboleo.event.editor.CopyConceptEvent;
import de.fzi.ipe.soboleo.event.editor.CutConceptEvent;
import de.fzi.ipe.soboleo.event.editor.LoadTaxonomyEvent;
import de.fzi.ipe.soboleo.event.editor.PasteConceptEvent;
import de.fzi.ipe.soboleo.event.editor.PredeleteConceptEvent;
import de.fzi.ipe.soboleo.event.editor.ShowAllRecommendationsEvent;
import de.fzi.ipe.soboleo.event.editor.ShowDialogsEvent;
import de.fzi.ipe.soboleo.event.editor.ShowRecommendationsForConceptEvent;
import de.fzi.ipe.soboleo.event.editor.StartDialogEvent;
import de.fzi.ipe.soboleo.event.editor.TreeLoadedEvent;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.AddTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ChangeTextCmd;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveTextCmd;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;
import de.fzi.ipe.soboleo.server.client.ClientUtil;
import de.fzi.ipe.soboleo.server.client.ConceptMultiWordSuggestion;
import de.fzi.ipe.soboleo.server.client.ConceptNameSuggestOracle;
import de.fzi.ipe.soboleo.server.client.util.WindowUtils;

public class TaxonomyPanel extends VerticalPanel implements TreeListener, KeyboardListener, MouseListener, EditorEventListener,DragNDropListener {
	
	private static final int CTRL_C = 0;
	private static final int CTRL_X = 1;
		
	private DragNDropTaxonomyTree taxTree;
	private HorizontalPanel buttonPanel;
	private Button add,remove;
	
	private EventDistributor eventDistrib;
	
	private EditorSuggestBehavior editorBehavior ;
	private SuggestBox searchBox;
	private ConceptNameSuggestOracle conceptOracle;
	private ConceptMultiWordSuggestion conceptSuggestion;
	private boolean oracleUpdateNeccessary = false;
	private ClientSideConcept currentItem;
	private ClientSideConcept currentSuperItem;

	private ClientSideConcept cutConcept;
	private ClientSideConcept cutSuperConcept;
	private boolean ctrlWasPressed = false;
	
	private int action;
	private boolean hasFocus = false;
	
	// Internationalisation
	private EditorConstants constants = GWT.create(EditorConstants.class);
	
	public TaxonomyPanel(EventDistributor eventDistrib) {
//		setWidth("260px");
//		setSpacing(8);
		setStyleName("taxonomyPanel");
		
		this.eventDistrib = eventDistrib;
		eventDistrib.addListener(this);		

		createConceptSearchBar();
		createButtonPanel();
		createTreePanel(eventDistrib);
	}

	private void createConceptSearchBar() {
		HorizontalPanel conceptSearchBar = new HorizontalPanel();
		conceptSearchBar.setWidth("260px");
//		conceptSearchBar.setSpacing(3);
		conceptSearchBar.setStyleName("headlineConceptSearchBar");
		
		Label headLabel = new Label();
		headLabel.setStyleName("headlineConceptSearch");
		headLabel.setText(constants.search());
//		headLabel.setText("Search:");		
		conceptSearchBar.add(headLabel);
		conceptSearchBar.setCellHorizontalAlignment(headLabel, HorizontalPanel.ALIGN_LEFT);

		editorBehavior = new EditorSuggestBehavior();
		
		conceptOracle =  new ConceptNameSuggestOracle("(, ");
		searchBox = new SuggestBox(conceptOracle);
		searchBox.setStyleName("gwt-SuggestBox");
		searchBox.setWidth("150px");
		
		searchBox.addSelectionHandler(new SelectionHandler<Suggestion>() {
		@Override
		public void onSelection(SelectionEvent<Suggestion> event) {
		   	conceptSuggestion = (ConceptMultiWordSuggestion) event.getSelectedItem();
			if(conceptSuggestion != null){
				editorBehavior.performSelection(getTree(),conceptSuggestion.getConcept().getURI());
				searchBox.setText("");
				editorBehavior.setItemfound(false);
		    }
		}});
		
		searchBox.addKeyUpHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if(oracleUpdateNeccessary){
					updateOracle();
					oracleUpdateNeccessary = false;
				}
				if(event.getNativeKeyCode()== KeyCodes.KEY_ENTER){
					if(conceptSuggestion != null){
						editorBehavior.performSelection(getTree(),conceptSuggestion.getConcept().getURI());
						searchBox.setText("");
						editorBehavior.setItemfound(false);
				    }		
				}
			}
		});		
		
		conceptSearchBar.add(searchBox);
		conceptSearchBar.setCellHorizontalAlignment(searchBox, HorizontalPanel.ALIGN_RIGHT);
		add(conceptSearchBar);
	}
	

	private void createTreePanel(EventDistributor eventDistrib) {
		taxTree = new DragNDropTaxonomyTree(eventDistrib);
		taxTree.setWidth("240px");
		taxTree.addTreeListener(this);
		taxTree.addKeyboardListener(this);
		taxTree.addMouseListener(this);
		taxTree.addDragNDropListener(this);

		
		ScrollPanel treeScrollPanel = new ScrollPanel();
		treeScrollPanel.setHeight("540px");
		treeScrollPanel.setWidth("260px");
		treeScrollPanel.add(taxTree);
		add(treeScrollPanel);
//		try{
//			tryGwtDnd();
//		}catch(Exception e){
//			Window.alert(e.getMessage());
//		}
	}
	
	private void tryGwtDnd() {
		// TODO Auto-generated method stub
		AbsolutePanel treePanel = new AbsolutePanel();
		treePanel.getElement().getStyle().setProperty("position", "relative");
		treePanel.getElement().getStyle().setProperty("border", "1px solid red");
		treePanel.setWidth("260px");
		treePanel.setHeight("540px");
		
		PickupDragController dragcontroller = new PickupDragController(treePanel, true);
		DropController dropController = new AbsolutePositionDropController(RootPanel.get());
		dragcontroller.registerDropController(dropController);
		
		for(int i = 0; i< taxTree.getItemCount(); i++){
			TreeItem itemWidget = taxTree.getItem(i);
			dragcontroller.makeDraggable(itemWidget.getWidget());
			treePanel.add(itemWidget.getWidget());
		}
		add(treePanel);
	}

	private void createButtonPanel() {
		buttonPanel = new HorizontalPanel();
		buttonPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
//		buttonPanel.setWidth("260px");
		buttonPanel.setSpacing(5);
		buttonPanel.setStyleName("buttonPanel");
		add = new Button();
		add.setText(constants.create());
		add.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent arg0) {
				new CreateConceptPopup();
			}
		});
		buttonPanel.add(add);
		
		remove = new Button();
		remove.setText(constants.delete());
		remove.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				removeConcept();
			}
		});
		buttonPanel.add(remove);
		
		add(buttonPanel);
		setCellHorizontalAlignment(buttonPanel,HorizontalPanel.ALIGN_CENTER);
	}
	
	
	private void removeConcept() {
		if (currentItem != null) {
			RemoveConceptCmd cmd = new RemoveConceptCmd(currentItem.getURI());
			EditorUtil.sendCommand(cmd);
			currentItem = null;
		}
	}

	public void onTreeItemSelected(TreeItem item) {
		if (item != null) {
			currentItem = ((ConceptTreeItem) item).getConcept();
			if(item.getParentItem() != null){
				currentSuperItem = ((ConceptTreeItem) item.getParentItem()).getConcept();
			}
			else currentSuperItem = null;
		}
		else {
			currentItem = null;
			currentSuperItem = null;
		}
		eventDistrib.notify(new ConceptSelectionChangedEvent(currentItem));
	}


	
	public void onTreeItemStateChanged(TreeItem item) {
		;
	}
	
	private void updateOracle() {
		conceptOracle.clear();
		  for (ClientSideConcept c: getTaxonomy().getConcepts()) {
			 conceptOracle.add(new ConceptMultiWordSuggestion(c));
			 for (LocalizedString altLabel : c.getTexts(SKOS.ALT_LABEL)) {
				ClientSideConcept newConcept = c;
				conceptOracle.add(new ConceptMultiWordSuggestion(newConcept,altLabel.getString()));
			  }
		  }		 
	}
	
	public void notify(Event event){
		if (event instanceof CutConceptEvent)prepareCutConcept();
		else if(event instanceof CopyConceptEvent)prepareCopyConcept();
		else if(event instanceof PasteConceptEvent)pasteConcept();
		else if(event instanceof PredeleteConceptEvent) removeConcept();
		else if(event instanceof StartDialogEvent) startDialog();
		else if(event instanceof ShowDialogsEvent) showDialogs();
		else if(event instanceof ShowAllRecommendationsEvent) showAllRecommendations();
		else if(event instanceof ShowRecommendationsForConceptEvent) showRecommendationsForConcept();
		else if (event instanceof LoadTaxonomyEvent) oracleUpdateNeccessary = true;
		else if (event instanceof CreateConceptCmd) oracleUpdateNeccessary = true;
		else if (event instanceof ChangeTextCmd)oracleUpdateNeccessary = true;
		else if (event instanceof RemoveConceptCmd)oracleUpdateNeccessary = true;
		else if (event instanceof AddTextCmd)oracleUpdateNeccessary = true;
		else if (event instanceof RemoveTextCmd)oracleUpdateNeccessary = true;		
		else if (event instanceof TreeLoadedEvent)checkOpenConcept();
	}
	
	private void checkOpenConcept(){
		String showConcept = Window.Location.getParameter("tagURI");
		if(showConcept != null && !showConcept.equalsIgnoreCase("undefined")){
			ClientSideConcept c = getTaxonomy().get(showConcept);
			if(c != null){
				editorBehavior.performSelection(getTree(),c.getURI());
				editorBehavior.setItemfound(false);
			}
		}
	}


	private void updateSelection(ClientSideConcept c) {
		editorBehavior.performSelection(getTree(),c.getURI());
		editorBehavior.setItemfound(false);
	}

	private void pasteConcept() {
		if (currentItem != null && cutConcept != null && (!currentItem.getURI().equals(cutConcept.getURI()))) {
			if (cutSuperConcept != null) {
				if (!currentItem.getURI().equals(cutSuperConcept.getURI())) {
					if (action == TaxonomyPanel.CTRL_X) {
						RemoveConnectionCmd cmd = new RemoveConnectionCmd(cutConcept.getURI(),SKOS.HAS_BROADER,cutSuperConcept.getURI());
						EditorUtil.sendCommand(cmd);
					}
					AddConnectionCmd cmd = new AddConnectionCmd(cutConcept.getURI(), SKOS.HAS_BROADER, currentItem.getURI());
					EditorUtil.sendCommand(cmd);
				}
			} else {
				AddConnectionCmd cmd = new AddConnectionCmd(cutConcept.getURI(), SKOS.HAS_BROADER, currentItem.getURI());
				EditorUtil.sendCommand(cmd);
			}
			cutConcept = null;
		}
	}

	private void prepareCopyConcept() {
		cutConcept = currentItem;
		action = TaxonomyPanel.CTRL_C;
	}

	private void prepareCutConcept() {
		cutConcept = currentItem;
		cutSuperConcept = currentSuperItem;
		action = TaxonomyPanel.CTRL_X;
	}
	
	private void startDialog(){
		if(currentItem != null){
			StartDialogPopup startPopup = new StartDialogPopup(currentItem.getURI(), currentItem.getBestFitText(SKOS.PREF_LABEL,ClientUtil.getUserLanguage()).getString());
		}
		else{
			EditorPopup popup = new EditorPopup(constants.pleaseSelectConceptToDiscuss(), "");
			popup.show();
		}
	}
	
	private void showDialogs(){
		if(currentItem != null){
			String url = ClientUtil.getAbsURL() + "taxonomy?space=" + ClientUtil.getSpaceName() +"&concept=" +currentItem.getURI().substring(currentItem.getURI().indexOf("#")+1) +"#dialogs";
			WindowUtils.redirect(url);
		}
		else{
			EditorPopup popup = new EditorPopup(constants.pleaseSelectConceptToDiscuss(), "");
			popup.show();
		}
	}
	
	private void showAllRecommendations(){
		EditorUtil.getEditorService().getRecommendations(ClientUtil.getSpaceName(), ClientUtil.getEventSenderCredentials(), new AsyncCallback<List<GardeningRecommendation>>(){
			public void onSuccess(List<GardeningRecommendation> result) {
				if (result != null && result.size() != 0) {
					new RecommendationPopUp(result,eventDistrib).show();
				} else {
					EditorPopup popup = new EditorPopup(constants.noRecommendations(), "");
					popup.show();
				}
			}
			public void onFailure(Throwable caught) {
				EditorUtil.handleException(caught);
			}});		
	}
	
	private void showRecommendationsForConcept(){
		if(currentItem != null){
			EditorUtil.getEditorService().getRecommendationsForConcept(currentItem.getURI(),ClientUtil.getSpaceName(), ClientUtil.getEventSenderCredentials(), new AsyncCallback<List<GardeningRecommendation>>(){
				public void onSuccess(List<GardeningRecommendation> result) {
					if (result != null && result.size() != 0) {
						new RecommendationPopUp(result,eventDistrib).show();
					} else {
						EditorPopup popup = new EditorPopup(constants.noRecommendations(), "");
						popup.show();
					}
				}
				
				public void onFailure(Throwable caught) {
					EditorUtil.handleException(caught);
				}});
		}
		else{
			EditorPopup popup = new EditorPopup(constants.recsSelectConcept(), "");
			popup.show();
		}
	}

	public void onKeyDown(Widget sender, char keyCode, int modifiers) {
		handleKey(keyCode, modifiers);
	}
	
	public void onKeyPress(Widget sender, char keyCode, int modifiers) {
		handleKey(keyCode, modifiers);
	}


	public void onKeyUp(Widget sender, char keyCode, int modifiers) {
		handleKey(keyCode, modifiers);
	}

	private void handleKey(char keyCode, int modifiers) {
		if (hasFocus) {
			if (keyCode == 17) ctrlWasPressed = true;
			if (keyCode == 'C' && (modifiers == MODIFIER_CTRL || ctrlWasPressed)) {
				GWT.log("erkannt:ctrl+c", new Throwable());
				prepareCopyConcept();
				ctrlWasPressed = false;
			} else if (keyCode == 'V' && (modifiers == MODIFIER_CTRL || ctrlWasPressed)) {
				GWT.log("erkannt:ctrl+v", new Throwable());
				pasteConcept();
				ctrlWasPressed = false;
			} else if (keyCode == 'X' && (modifiers == MODIFIER_CTRL || ctrlWasPressed)) {
				GWT.log("erkannt:ctrl+x", new Throwable());
				prepareCutConcept();
				ctrlWasPressed = false;
			}
			else if (keyCode == 'D' && (modifiers == MODIFIER_CTRL || ctrlWasPressed)) {
				removeConcept();
				ctrlWasPressed = false;
			} else if (keyCode == 'N' && (modifiers == MODIFIER_CTRL || ctrlWasPressed)) {
				new CreateConceptPopup();
				ctrlWasPressed = false;
			}
		}
	}

	public ClientSideTaxonomy getTaxonomy() {
		return taxTree.getTaxonomy();
	}

	public void onMouseDown(Widget sender, int x, int y) {
		;
	}

	public void onMouseEnter(Widget sender) {
		hasFocus = true;	
	}

	public void onMouseLeave(Widget sender) {
		hasFocus = false;
	}

	public void onMouseMove(Widget sender, int x, int y) {
		;
	}

	public void onMouseUp(Widget sender, int x, int y) {
		;
	}
	
	
	public void drop(ConceptTreeItem dragged, ConceptTreeItem droppedOn, int modifier) {
		String fromURI = dragged.getConcept().getURI();
		String toURI = droppedOn.getConcept().getURI();
		if (modifier == DragNDropTaxonomyTree.MODIFIER_NONE) {
			AddConnectionCmd command = new AddConnectionCmd(fromURI,SKOS.HAS_BROADER,toURI);
			EditorUtil.sendCommand(command);
		}
		else if (modifier == DragNDropTaxonomyTree.MODIFIER_SHIFT){
			RemoveConnectionCmd command = new RemoveConnectionCmd(fromURI,SKOS.HAS_BROADER,toURI);
			EditorUtil.sendCommand(command);
		}
		
	}
	
	public TaxonomyTree getTree(){
		return taxTree;
	}
	
	public EventDistributor getEventDistributor(){
		return this.eventDistrib;
	}
	/**
	 * Sets the focus of the cursor to the searchBox
	 */
	public void setFocusSearchBox()
	{
		this.searchBox.setFocus(true);
	}
}
