package de.fzi.ipe.soboleo.annotate.client.components;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.annotate.client.AnnotateConstants;
import de.fzi.ipe.soboleo.annotate.client.AnnotateMain;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;

/**
 * Class where all PopUps were generated for the Annotate side. 
 * @author FZI
 *
 */
public class AnnotatePopup extends PopupPanel {
	private AnnotateConstants constants = GWT.create(AnnotateConstants.class);
	private AnnotateMain caller;
	
	/**
	 * Constructor to generate the annotate side as a PopUp window.
	 * @param caller
	 */
	public AnnotatePopup(AnnotateMain caller) 
	{
		super(false);
		this.caller = caller;
	}
	
	/**
	 * Constructor to generate a Exception Window as a PopUp.
	 * @param caught
	 */
	public AnnotatePopup(Throwable caught) 
	{
	    super(true);
	    if (caught instanceof EventPermissionDeniedException) 
	    {
		String reason = ((EventPermissionDeniedException) caught).getReason();
		reason = reason.replaceAll("\\[error_|\\].*","");
		try
		{
		    switch(Integer.parseInt(reason))
		    {
		    case 200: setContent(constants.error_200(),""); break;
		    case 201: setContent(constants.error_201(),""); break;
		    case 300: setContent(constants.error_300(),""); break;
		    default: setContent(((EventPermissionDeniedException) caught).getReason(), ""); break;
		    }
		} 
		catch(NumberFormatException e)
		{
		    setContent(((EventPermissionDeniedException) caught).getReason(), "");
		}
	    } 
	    else 
	    {
		setContent(constants.exceptionOccured(), constants.closeAndReload()+ caught.getMessage()+",<br/>,<br/>"+caught.toString());
	    }
	}

	/**
	 * Constructor to generate a PopUp Window with a Title and a Message.
	 * It also generates a OK button to close this window.
	 * @param title
	 * @param message
	 */
	public AnnotatePopup(String title, String message) 
	{
		super(true);
		setContent(title,message);
	}
	
	
	private void setContent(String title, String message) 
	{
	    VerticalPanel vPanel = new VerticalPanel();
	    vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
	    HTML headline = new HTML("<h3>"+title+"</h3><br/>"+message);
	    vPanel.add(headline);
	    final Button okButton = new Button("OK");
	    okButton.setStyleName("buttonPannel");
	    okButton.addClickHandler(new ClickHandler() 
	    {
		@Override
		public void onClick(ClickEvent ce) 
		{
				
				 AnnotatePopup.this.hide();
			
		}
	    });
	    okButton.addMouseOverHandler(new MouseOverHandler() 
	    {
		public void onMouseOver(MouseOverEvent event) 
		{
		    okButton.setStyleName("buttonPannelOver");
		}
	    });
	    okButton.addMouseOutHandler(new MouseOutHandler() 
	    {
		public void onMouseOut(MouseOutEvent event) 
		{
		    okButton.setStyleName("buttonPannel");
		}
	    });
	    vPanel.add(okButton);
	    setStyleName("ks-popups-Popup");
	    add(vPanel);
	}
	/**
	 * Displays the success message, that the data was saved successfully. 
	 * @param title
	 * @param message
	 */
	public void showSuccessDialog(String title, String message)
	{
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HTML headline = new HTML("<h3>"+title+"</h3><br/>"+message);
		headline.setStyleName("gwtLabelAnnotate");
		vPanel.add(headline);
		final Button okButton = new Button("OK");
		okButton.setStyleName("buttonPannel");
		okButton.addClickHandler(new ClickHandler() 
		{
			@Override
			public void onClick(ClickEvent ce) 
			{
				AnnotatePopup.this.hide();			
				String windowLocation = "";
				try {
					windowLocation = Window.Location.getHref();
				} catch (Exception ex) {
					windowLocation = "";
				}
				// if on soboleo page, reload annotatemain with empty values
				if (windowLocation.contains("annotation.jsp"))
				{
					caller.resetWindow();
				}
				else
				{
					AnnotateMain.closeWindow();
				}					
			}
		});
		okButton.addMouseOverHandler(new MouseOverHandler() 
		{
			public void onMouseOver(MouseOverEvent event) 
			{
			    okButton.setStyleName("buttonPannelOver");
			}
		});
		okButton.addMouseOutHandler(new MouseOutHandler() 
		{
			public void onMouseOut(MouseOutEvent event) 
			{
			    okButton.setStyleName("buttonPannel");
			}
		});
		vPanel.add(okButton);
		setStyleName("ks-popups-Popup");
		add(vPanel);
		show();
	}
	
	public void show() {
		setPopupPosition(150, 150);
		super.show();
	}
	
    
  }