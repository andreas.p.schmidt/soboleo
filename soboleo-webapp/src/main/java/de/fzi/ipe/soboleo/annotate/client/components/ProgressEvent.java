package de.fzi.ipe.soboleo.annotate.client.components;

import com.google.gwt.event.shared.GwtEvent;

/**
 * ProgressEvent is an event, which sends an integer.
 * This integer should represent the actual process of the upload.
 * 
 * @author kluge
 *
 */
public class ProgressEvent extends GwtEvent<ProgressEventHandler> 
{
    /**
     * TYPE  Type<ProgressEventHandler>
     */
    public static Type<ProgressEventHandler> TYPE = new Type<ProgressEventHandler>();
    private final int progress;
    
    /**
     * The given integer sets the actual state of the progress.
     * @param progress
     */
    public ProgressEvent(int progress) 
    {
	this.progress = progress;
    }
    
    @Override
    public Type<ProgressEventHandler> getAssociatedType() 
    {
	return TYPE;
    }

    @Override
    protected void dispatch(ProgressEventHandler handler) 
    {
	handler.onProgressEventReceived(this);
    }

    /**
     * To get the set progress from the actual event
     * @return Integer
     */
    public int getProgress()
    {
	return this.progress;
    }
}
