package de.fzi.ipe.soboleo.editor.client.conceptDetails;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.DecoratedTabPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;

public class ConceptDetailsPanel extends VerticalPanel{
private EventDistributor eventDistrib;

// Internationalization
private EditorConstants constants = GWT.create(EditorConstants.class);
	
	public ConceptDetailsPanel(EventDistributor eventDistrib){
		
		this.eventDistrib = eventDistrib;
		this.setStyleName("editConceptPanel");
		
		Label headline = new Label(constants.editConcept());
//		Label headline = new Label("Edit Concept");
		headline.setStyleName("headline");
		this.add(headline);
		
		DecoratedTabPanel conceptDetailsTabs = new DecoratedTabPanel();
		conceptDetailsTabs.setStyleName("conceptDetailsTabPanel");
		this.add(conceptDetailsTabs);
		
		ConceptLabelsDescriptionPanel labelsDescription = new ConceptLabelsDescriptionPanel(eventDistrib);
		conceptDetailsTabs.add(labelsDescription, constants.labelsAndDescription());
//		conceptDetailsTabs.add(labelsDescription, "Labels & Description");
		
		ConceptRelationsPanel relationsPanel = new ConceptRelationsPanel(eventDistrib);
		conceptDetailsTabs.add(relationsPanel, constants.relations());
//		conceptDetailsTabs.add(relationsPanel, "Relations");
		
		conceptDetailsTabs.selectTab(0);
	}
}
