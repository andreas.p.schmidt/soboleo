package de.fzi.ipe.soboleo.editor.client.recommendationArea;

import java.util.Set;

import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptLabelIdenticalProblem;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptLabelIdenticalProblem.LabelTupel;

public class IdenticalLabelCaptionPanel extends RecommendationCaptionPanel<ConceptLabelIdenticalProblem>{

	public IdenticalLabelCaptionPanel(EventDistributor eventDistrib, String caption) {
		super(eventDistrib, caption);
	}

	@Override
	public void addRecommendation(GardeningRecommendation rec) {
		String preText = constants.recsConcept();
		String fillingText = "";
		String sufText = "";
		LabelTupel primary = ((ConceptLabelIdenticalProblem) rec).getLabelTupel();
		Set<LabelTupel> colliding = ((ConceptLabelIdenticalProblem) rec).getProblems();
		preText = getTextForType(primary.labelType);
		fillingText = constants.recsIsIdentical();
		for(LabelTupel other : colliding){				
			createDisplay(preText, fillingText + getTextForType(other.labelType) +" ", sufText, rec.getConcept(), other.concept);
		}
	}	
	
	@Override
	public void removeRecommendation() {
		// TODO Auto-generated method stub
	}	
}
