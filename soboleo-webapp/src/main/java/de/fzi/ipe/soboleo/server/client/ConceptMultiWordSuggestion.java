package de.fzi.ipe.soboleo.server.client;

import com.google.gwt.user.client.ui.MultiWordSuggestOracle.MultiWordSuggestion;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;

public class ConceptMultiWordSuggestion extends MultiWordSuggestion {
	private ClientSideConcept concept = null;
	
	public ConceptMultiWordSuggestion(ClientSideConcept conceptToCreate){
		// PREF_LABEL only
		super(conceptToCreate.getBestFitText(SKOS.PREF_LABEL, ClientUtil.getSpaceLanguage()).getString(),
				conceptToCreate.getBestFitText(SKOS.PREF_LABEL, ClientUtil.getSpaceLanguage()).getString());
		this.concept = conceptToCreate;
	}
	
	public ConceptMultiWordSuggestion(ClientSideConcept conceptToCreate, String altLabel) {
		// ALT_LABEL (PREF_LABEL)
		super(altLabel, altLabel + " (" + conceptToCreate.getBestFitText(SKOS.PREF_LABEL, ClientUtil.getSpaceLanguage()).getString() + ")");
		this.concept = conceptToCreate;
	}
	
	public ConceptMultiWordSuggestion(ClientSideConcept conceptToCreate, String replacementText, String displayText){
		super(replacementText, displayText);
		this.concept = conceptToCreate;
	}

	/*
	 * @return the ClientSideConcept
	 */
	public ClientSideConcept getConcept(){
		return concept;
	}
}
