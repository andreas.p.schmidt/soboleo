package de.fzi.ipe.soboleo.editor.client.components;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.server.client.ClientUtil;
import de.fzi.ipe.soboleo.server.client.util.WindowUtils;

public class EditorPopup extends PopupPanel {

	private EditorConstants constants = GWT.create(EditorConstants.class);
	public EditorPopup(Throwable caught) {
      super(true);
		setStyleName("ks-popups-Popup");
//		Window.alert("caught: " +caught);
      if (caught instanceof EventPermissionDeniedException) {
    	  String reason = ((EventPermissionDeniedException) caught).getReason();
    	  reason = reason.replaceAll("\\[error_|\\].*","");
    	  System.out.println(reason + "  " +Integer.parseInt(reason));
//    	  Window.alert("reason: " +reason);
    	  try{
			switch(Integer.parseInt(reason)){
			case 100: setContent(constants.error_100()); break;
			case 101: setContent(constants.error_101()); break;
			case 102: setContent(constants.error_102()); break;
			case 103: setContent(constants.error_103()); break;
			case 104: setContent(constants.error_104()); break;
			case 105: setContent(constants.error_105()); break;
			case 106: setContent(constants.error_106()); break;
			case 107: setContent(constants.error_107()); break;
			case 108: setContent(constants.error_108()); break;
			case 109: setContent(constants.error_109()); break;
			case 110: setContent(constants.error_110()); break;
			case 111: setContent(constants.error_111()); break;
			case 112: setContent(constants.error_112()); break;
			case 113: setContent(constants.error_113()); break;
			case 114: setContent(constants.error_114()); break;
			case 115: setContent(constants.error_115()); break;
			case 116: setContent(constants.error_116()); break;
			case 200: setContent(constants.error_200()); break;
			case 201: {setContent(constants.error_201()); WindowUtils.redirect(ClientUtil.getAbsURL() +"login?redirectURL=" + URL.encode(WindowUtils.getLocation().getHref())); break;}
			default: setContent(((EventPermissionDeniedException) caught).getReason()); break;
			}
    	  } catch(NumberFormatException e){
    		  setContent(((EventPermissionDeniedException) caught).getReason());
    	  }
      }
      else {
    	  setContent(constants.exceptionOccured(), constants.closeAndReload()+ caught.getMessage()+",<br/>"+
    			  ",<br/>"+caught.toString());
    	  WindowUtils.redirect(ClientUtil.getAbsURL() +"login?redirectURL=" + URL.encode(WindowUtils.getLocation().getHref()));
      }
	}

	public EditorPopup(String title, String message) {
		super(true);
		setStyleName("ks-popups-Popup");
		setContent(title,message);
	}
	
	
	private void setContent(String title, String message) {
		HTML headline = new HTML("<h3>"+title+"</h3><br/>"+message);
		createPanel(headline);
	}
	
	private void setContent(String text){
		HTML message = new HTML("<h3>" +text +"</h3>");
		createPanel(message);
	}
	
	private void createPanel(HTML message){
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		vPanel.setSpacing(5);
		add(vPanel);
		vPanel.add(message);
		final Button ok = new Button("OK");
		ok.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent arg0) {
				EditorPopup.this.hide();
			}
		});
		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		buttonPanel.add(ok);
		vPanel.add(buttonPanel);
	}
	
	public void show() {
		setPopupPosition(250, 250);
		super.show();
	}
	
    
  }