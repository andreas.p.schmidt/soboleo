package de.fzi.ipe.soboleo.server.client;

import java.util.HashMap;


import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import de.fzi.ipe.soboleo.beans.client.ClientConfiguration;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.client.rpc.LoginService;
import de.fzi.ipe.soboleo.server.client.rpc.LoginServiceAsync;
import de.fzi.ipe.soboleo.server.client.util.ImageProvider;

public class ClientUtil {

	private static ClientConfiguration clientConfiguration;
	private static EventSenderCredentials credentials = new EventSenderCredentials(
			"http://ClientDummy", "Dummy", "");
	private static String spaceName;
	private static String key;
	private static long currentVersion = -2;
	private static HashMap<String, String> urlParameters = new HashMap<String, String>();
	private static ImageProvider imageProvider;


	// Init Methods
	public static void initConfig(final AsyncCallback<Void> calledWhenDone) {

		initURLParameters();
		initImageProvider();
		key = Cookies.getCookie("SoboleoCookie");
		spaceName = urlParameters.get("space");

		LoginServiceAsync loginService = (LoginServiceAsync) GWT
				.create(LoginService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) loginService;
		endpoint.setServiceEntryPoint(getServerURL() + "user/service"); 
		
		loginService.getConfig(key, spaceName,
				new AsyncCallback<ClientConfiguration>() {
					public void onFailure(Throwable caught) {
						calledWhenDone.onFailure(caught);
					}

					public void onSuccess(ClientConfiguration result) {
						clientConfiguration = result;
						if (spaceName == null)
							spaceName = clientConfiguration.getDefaultSpace();
						credentials = new EventSenderCredentials(
								clientConfiguration.getUserID(),
								clientConfiguration.getUserName(),
								clientConfiguration.getUserKey());
						calledWhenDone.onSuccess(null);
					}
				});
	}

	private static void initURLParameters() {
		if (!getParameters().isEmpty()) {
			String[] parameters = getParameters().substring(1).split("&");
			for (String currentParam : parameters) {
				String[] parts = currentParam.split("=");
				urlParameters.put(parts[0], parts[1]);
			}
		}
	}

	private static void initImageProvider() {
		imageProvider = (ImageProvider) GWT.create(ImageProvider.class);
	}

	// Get Methods

	public static String getUserName() {
		return clientConfiguration.getUserName();
	}

	public static String getSpaceName() {
		return spaceName;
	}

	public static String getPrototypicalConceptsName() {
		return clientConfiguration.getPrototypicalConceptsName();
	}

	public static EventSenderCredentials getEventSenderCredentials() {
		return credentials;
	}

	public static Language getUserLanguage() {
		return clientConfiguration.getDefaultUserLanguage();
	}

	public static Language getSpaceLanguage() {
		return clientConfiguration.getDefaultSpaceLanguage();
	}

	public static long getCurrentVersion() {
		return currentVersion;
	}

	public static native String getHostName() /*-{
												return $wnd.location.hostname;
												}-*/;

	public static native String getRelURL() /*-{
											return $doc.getElementById("relURL").content
											}-*/;

	public static native String getAbsURL() /*-{
											return $doc.getElementById("absURL").content
											}-*/;

	public static native String getParameters() /*-{
												return $wnd.location.search;
												}-*/;

	public static String getServerURL() {

		return getAbsURL();
	}

	public static ImageProvider getImageProvider() {
		return imageProvider;
	}

	public static boolean hasSpaceDialogSupport() {
		return clientConfiguration.hasSpaceDialogSupport();
	}

	/**
	 * Gets the configuration of the AnnotatePeopleDeleteTopic. If it is set to
	 * true, the person can delete annotations made by others and a red cross
	 * appears after a topic. If it is false, the red cross is not available and
	 * it is not possible to delete topics. Used for AnnotatePeople.
	 * 
	 * @return hasAnnotatePeopleDeleteTopic boolean
	 */
	public static boolean getAnnotatePeopleDeleteTopic() {
		return clientConfiguration.getAnnotatePeopleDeleteTopic();
	}

	/**
	 * Returns the boolean value of the Annotate Web Page configuration. If it
	 * is true, the web side can be rated.
	 * 
	 * @return boolean
	 */
	public static boolean getRatingWebsides() {
		return clientConfiguration.getRatingWebsides();
	}

	// Set methods
	/**
	 * Sets the client configuration
	 * 
	 * @param clientConfiguration
	 */
	public static void setContext(ClientConfiguration clientConfiguration) {
		ClientUtil.clientConfiguration = clientConfiguration;
	}

	/**
	 * @param version
	 */
	public static void setCurrentVersion(long version) {
		currentVersion = version;
	}

}
