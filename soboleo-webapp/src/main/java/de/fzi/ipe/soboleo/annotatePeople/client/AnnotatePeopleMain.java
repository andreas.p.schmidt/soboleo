package de.fzi.ipe.soboleo.annotatePeople.client;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.annotatePeople.client.components.AnnotatePeoplePanel;
import de.fzi.ipe.soboleo.annotatePeople.client.components.HorizontalFlowPanel;
import de.fzi.ipe.soboleo.annotatePeople.client.components.TopicLink;
import de.fzi.ipe.soboleo.annotatePeople.client.components.TopicLinkWithOptionalDelete;
import de.fzi.ipe.soboleo.annotatePeople.client.rpc.AnnotatePeopleService;
import de.fzi.ipe.soboleo.annotatePeople.client.rpc.AnnotatePeopleServiceAsync;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.ManualPersonTag;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.PersonTag;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.event.logging.StartAnnotatePeople;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.client.ClientUtil;
import de.fzi.ipe.soboleo.server.client.ConceptMultiWordSuggestion;
import de.fzi.ipe.soboleo.server.client.ConceptNameSuggestOracle;
import de.fzi.ipe.soboleo.server.client.LoginDialog;
import de.fzi.ipe.soboleo.server.client.util.Location;
import de.fzi.ipe.soboleo.server.client.util.WindowUtils;

/**
 * This class creates the annotate form. It is used to add Tags to people. It
 * implements the class EntryPoint, so this is used in AnnotatePeople.gwt.xml
 * for the EntryPoint. onModuleLoad() will be executed first. createAnnotate()
 * initializes the form with the fields and buttons.
 * 
 * @author FZI
 * 
 */
public class AnnotatePeopleMain extends VerticalPanel {

	private static AnnotatePeopleServiceAsync annoService;
	private FlexTable flexTable;
	private FlexCellFormatter cellFormatter;
	private VerticalPanel annotatePanel;
	private HorizontalPanel inputPanel;
	// suggestBox;
	private HTML email;
	private HTML name;
	private TextBox urlInput;
	private Button removeFromIndexButton = new Button();
	private HashMap<String, String> myAnnotationsList = new HashMap<String, String>();// Strings
	// for
	// the
	// myTopicsPanel
	// TopicLinks
	private HorizontalFlowPanel extractedConceptsPanel = new HorizontalFlowPanel();
	private HorizontalFlowPanel othersManualTagsPanel = new HorizontalFlowPanel();
	private String spacename;
	private LocalizedString.Language userLanguage;
	private LocalizedString.Language spaceLanguage;
	private EventSenderCredentials creds;
	private int rowPos = 0;
	private TaggedPerson taggedPerson;
	private String webURL;
	private static AnnotatePeopleConstants constants = GWT
			.create(AnnotatePeopleConstants.class);
	private ConceptNameSuggestOracle conceptOracle = new ConceptNameSuggestOracle(
			"(, ");
	private SuggestBox conceptSuggestBox;
	private ConceptMultiWordSuggestion conceptSuggestion;
	private String userName = "";
	private Set<String> conceptsToDeleteOtherTopics; // stores all concepts
	// which should be
	// deleted from other
	// Topics
	private HorizontalFlowPanel pannelMyTopics = new HorizontalFlowPanel(); // Flow
	// panel
	// for
	// TopicLinks
	// MyTopics
	private AbsolutePanel spacePanelForEmptyTopicsPanel = new AbsolutePanel();
	private AbsolutePanel spacePanelForEmptySuggestedTopicsPanel = new AbsolutePanel();

	private AnnotatePeopleController controller;
	
	
	
	public AnnotatePeopleController getController() {
		return controller;
	}


	public AnnotatePeopleMain(AnnotatePeopleController controller) {

		this.controller=controller;
		
		ClientUtil.initConfig(new AsyncCallback<Void>() {

			public void onFailure(Throwable caught) {
				
				Window.alert("error: " +caught.getLocalizedMessage());
				//new AnnotatePeoplePopup(caught);
			}

			public void onSuccess(Void result) {
				spacename = ClientUtil.getSpaceName();
				creds = ClientUtil.getEventSenderCredentials();
				userLanguage = ClientUtil.getUserLanguage();
				spaceLanguage = ClientUtil.getSpaceLanguage();
				userName = ClientUtil.getUserName();
				getAnnotatePeopleService().sendEvent(new StartAnnotatePeople(),
						ClientUtil.getSpaceName(),
						ClientUtil.getEventSenderCredentials(),
						new AsyncCallback<Void>() {
							public void onFailure(Throwable caught) {
								focusWindow();
								handleException(caught);
							}

							public void onSuccess(Void arg0) {
								;
							}
						});
				createAnnotate();
			}
		});
	}


	private void initFlexTable()
	{
		flexTable = new FlexTable();
		cellFormatter = flexTable.getFlexCellFormatter();
		flexTable.setWidth("320px");
		flexTable.setCellSpacing(8);

		cellFormatter.setHorizontalAlignment(0, 0,
				HasHorizontalAlignment.ALIGN_CENTER);
		cellFormatter.setColSpan(4, 0, 2);
		cellFormatter.setColSpan(5, 0, 2);
		cellFormatter.setColSpan(6, 0, 2);
		cellFormatter.setColSpan(0, 0, 2);
		HTML html = new HTML("<h3>" + constants.annotatePeople() + "</h3>");
		html.setStyleName("gwtLabelAnnotate");
		flexTable.setWidget(0, 0, html);

	}
	
	
	String taggedPersonEmail=null;
	String conceptLabelToAdd=null;
	
	/**
	 * Creates the form with labels, fields and buttons.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void createAnnotate() {
		this.conceptsToDeleteOtherTopics = new HashSet<String>();
		webURL = Window.Location.getParameter("furi");
		taggedPersonEmail = Location.decodeURIComponent(Window.Location
				.getParameter("taggedPersonEmail"));
		conceptLabelToAdd = Location.decodeURIComponent(Window.Location
				.getParameter("conceptLabel"));

		if (Location.decodeURIComponent(Window.Location
				.getParameter("conceptLabel")) != null) {
			conceptLabelToAdd = Location.decodeURIComponent(Window.Location
					.getParameter("conceptLabel"));
			if (conceptLabelToAdd.equals("null")) {
				conceptLabelToAdd = "undefined";
			}
		}

		
		// check if a person is available via email adress of url

		boolean personAvailable = false;

		if (taggedPerson == null) {
			if (taggedPersonEmail != null || !taggedPersonEmail.equals("")
					|| !taggedPersonEmail.equalsIgnoreCase("undefined")
					|| !taggedPersonEmail.equalsIgnoreCase("null")) 
			{
				getTaggedPersonByEmail(taggedPersonEmail);
			}

			if (taggedPerson == null) {
				if (webURL != null && !webURL.equalsIgnoreCase("undefined")) 
				{		
					getTaggedPersonByURL(webURL);
				}
			}
		}

		if (taggedPerson != null) {
			personAvailable = true;
		}
		initFlexTable();
		if (!personAvailable) {

			if (conceptLabelToAdd != null
					&& !conceptLabelToAdd.equalsIgnoreCase("undefined"))
				updateMyAnnotationsBox(conceptLabelToAdd, null);

			// change this to be a panel

			AnnotatePeoplePanel panelAddEmail = new AnnotatePeoplePanel(AnnotatePeopleMain.this);

			askEmailPanel = panelAddEmail.showEnterEmailDialog();
			askEmailPanel.setWidth("300px");
			
			flexTable.setWidget(1, 0, askEmailPanel);
		} else {
			createNamePanel(1, "200px");
			createEmailPanel(2, "200px");
			createTopicInputPanel(3, "200px");
			createOthersManualTagsPanel(4, "310px");
			createRecommendationPanel(5, "310px");
			createMyAnnotationListPanel(6, "310px");
			createRemoveAnnotationButton(8);
	
			createButtonPanel(10);

		}

		addFlexTable();
		focusWindow();
	}

	private void addFlexTable()
	{
		add(flexTable);
		flexTable.getCellFormatter().setHorizontalAlignment(4, 0,
				HasHorizontalAlignment.ALIGN_LEFT);
		flexTable.getCellFormatter().setVerticalAlignment(4, 0,
				HasVerticalAlignment.ALIGN_MIDDLE);
		flexTable.getCellFormatter().setHorizontalAlignment(5, 0,
				HasHorizontalAlignment.ALIGN_LEFT);
		flexTable.getCellFormatter().setVerticalAlignment(5, 0,
				HasVerticalAlignment.ALIGN_MIDDLE);
		flexTable.getCellFormatter().setHorizontalAlignment(6, 0,
				HasHorizontalAlignment.ALIGN_LEFT);
		flexTable.getCellFormatter().setVerticalAlignment(6, 0,
				HasVerticalAlignment.ALIGN_MIDDLE);
	}
	
	VerticalPanel askEmailPanel;

	public void setLoadedPerson()
	{
		remove(flexTable);
		remove(askEmailPanel);
		createAnnotate();	
	}
	
	public void loadAvailablePersonByEmailAndProcess(String email) {
		getTaggedPersonByEmail(email);


		remove(flexTable);

		createAnnotate();
	}
	
	public void askForNameOfPerson(String email)
	{
		flexTable.removeRow(1);
		//remove(askEmailPanel);

		// show the ask for name panel
		AnnotatePeoplePanel panelAddEmail = new AnnotatePeoplePanel(
				AnnotatePeopleMain.this);
		
		createEmailPanel(2, "200px");
		this.email.setText(email);
		
		askEmailPanel=panelAddEmail.showEnterNameDialog(email);
		add(askEmailPanel);

	}
	
	
	public void addNewPersonByEnteredNameAndMail(String email, String name)
	{
		remove(askEmailPanel);
		remove(flexTable);
		
		initFlexTable();
		addFlexTable();	
		createTaggedPerson(name, email);
	}
	
	public void initComponents()
	{
		createNamePanel(1, "200px");
		createEmailPanel(2, "200px");
		createTopicInputPanel(3, "200px");
		createOthersManualTagsPanel(4, "310px");
		createRecommendationPanel(5, "310px");
		createMyAnnotationListPanel(6, "310px");
		createRemoveAnnotationButton(8);

		createButtonPanel(10);
	}
	

	private void getTaggedPersonByURL(String webURL) {
		getAnnotatePeopleService().getTaggedPersonByURL(
				ClientUtil.getSpaceName(),
				ClientUtil.getEventSenderCredentials(),
				ClientUtil.getUserLanguage(), webURL,
				new AsyncCallback<TaggedPerson>() {
					public void onSuccess(TaggedPerson person) {
						if (person != null) {
							setTaggedPerson(person);
							fillData();
						} else {
							//new AnnotatePeoplePopup(AnnotatePeopleMain.this)
							//		.showEnterEmailDialog();
						}
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
	}

	/**
	 * Gets the TaggedPerson from the given E-mail and save it to the class
	 * members with setTagedPerson. It also triggers the initialization of the
	 * AnnotatePopup and fills in the data of the person.
	 * 
	 * @param email
	 */
	public void getTaggedPersonByEmail(final String email) {
		getAnnotatePeopleService().getTaggedPersonByEmail(
				ClientUtil.getSpaceName(),
				ClientUtil.getEventSenderCredentials(), email,
				new AsyncCallback<TaggedPerson>() {

					public void onSuccess(TaggedPerson person) {
						if (person != null) {
							if (!person.getEmail().equals("null")){
								setTaggedPerson(person);
								//fillData();
								setLoadedPerson();
								fillData();
							}
			
						} else {
							// not good...
							/*
							new AnnotatePeoplePopup(AnnotatePeopleMain.this)
									.showEnterNameDialog(email);
									*/
						}
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
	}

	/**
	 * Creates a new TaggedPerson and sets the member value and fills the data
	 * 
	 * @param name
	 * @param email
	 */
	public void createTaggedPerson(String name, String email) {
		getAnnotatePeopleService().createTaggedPerson(
				ClientUtil.getSpaceName(),
				ClientUtil.getEventSenderCredentials(), email, name,
				new AsyncCallback<TaggedPerson>() {
					public void onSuccess(TaggedPerson person) {
						
						//Window.alert("succes:" +person.getURI());
						setTaggedPerson(person);
						initComponents();
						fillData();
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
	}

	private void createMyAnnotationListPanel(int pos, String width) {
		String captionForCurrentTopicsCaptionPannel = "<font color=\"#705132\">"
				+ constants.myTopics() + "</font>";
		CaptionPanel myTopicsCaptionPanel = new CaptionPanel(
				captionForCurrentTopicsCaptionPannel, true);
		myTopicsCaptionPanel.setStyleName("gwt-myTopicsCaptionPanel");
		flexTable.setWidget(pos, 0, myTopicsCaptionPanel);
		spacePanelForEmptyTopicsPanel.setHeight("100px");
		pannelMyTopics.add(spacePanelForEmptyTopicsPanel);
		pannelMyTopics.setStyleName("myTopicsPannel");
		myTopicsCaptionPanel.setContentWidget(pannelMyTopics);
		// myTopicsCaptionPanel.setHeight("100px");
		myTopicsCaptionPanel.setWidth(width);
	}

	private void createButtonPanel(int pos) {
		final Button saveButton = new Button();
		saveButton.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				saveButton.setStyleName("buttonPannel");
			}
		});
		saveButton.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				saveButton.setStyleName("buttonPannelOver");
			}
		});
		saveButton.setStyleName("buttonPannel");
		saveButton.setText(constants.save());
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
//				blurWindow();
				String current = conceptSuggestBox.getText().trim();
				if (!current.equals("")
						&& !myAnnotationsList.containsKey(current)) {
					updateMyAnnotationsBox(current, new AsyncCallback<Void>() {
						public void onSuccess(Void result) {
							saveAnnotation();
						}

						public void onFailure(Throwable caught) {
							handleException(caught);
						}
					});
				} else
					saveAnnotation();
			}
		});

		final Button cancelButton = new Button();
		cancelButton.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				cancelButton.setStyleName("buttonPannel");
			}
		});
		cancelButton.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				cancelButton.setStyleName("buttonPannelOver");
			}
		});
		cancelButton.setStyleName("buttonPannel");
		cancelButton.setText(constants.cancel());
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				blurWindow();
				Window.Location.reload();
				if (!(Window.Location.getHref()
						.contains("/annotatePeople-ext.jsp"))) {
					closeWindow();
				}
				reloadOpenerWindow();
			}
		});
		removeFromIndexButton.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				removeFromIndexButton.setStyleName("buttonPannelOver");
			}
		});
		removeFromIndexButton.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				removeFromIndexButton.setStyleName("buttonPannel");
			}
		});

		removeFromIndexButton.setStyleName("buttonPannel");
		removeFromIndexButton.setText(constants.delete());
		// removeFromIndexButton.setWidth("50px");
		removeFromIndexButton.addClickHandler(new ClickHandler() {
			
			
	
			public void onClick(ClickEvent event) {
	
				
				boolean deleteConfirmation = Window
				.confirm(constants.reallyDeletePerson());				
				
				if (deleteConfirmation)
				{
					blurWindow();
					getAnnotatePeopleService().removeUser(
							ClientUtil.getSpaceName(),
							ClientUtil.getEventSenderCredentials(),
							taggedPerson.getURI(), new AsyncCallback<Boolean>() {
								public void onFailure(Throwable caught) {
									focusWindow();
									handleException(caught);
								}
	
								public void onSuccess(Boolean status) {
									
									System.out.println("overall status :" +status);
									
									if (status)
									{
										try
										{
											if (!(Window.Location.getHref()
													.contains("/annotatePeople-ext.jsp"))) {
												closeWindow();
											}
										}
										catch(Exception e)
										{
											
										}
										reloadOpenerWindow();
									}
								}
							});
						}
				}
		});

		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.add(saveButton);
		buttonPanel.add(removeFromIndexButton);
		buttonPanel.add(cancelButton);

		cellFormatter.setColSpan(pos, 0, 2);
		cellFormatter.setHorizontalAlignment(pos, 0,
				HasHorizontalAlignment.ALIGN_CENTER);
		conceptSuggestBox = new SuggestBox(conceptOracle);
		conceptSuggestBox.setStyleName("gwt-SuggestBox");
		
		conceptSuggestBox.setWidth("260px");
		
		conceptSuggestBox.setLimit(6);

		conceptSuggestBox.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				String current = conceptSuggestBox.getText();
				// if comma or semicolon is entered
				if (event.getNativeKeyCode() == 188
						|| event.getNativeKeyCode() == 110
						|| event.getNativeKeyCode() == 186) {
					current = current.replaceAll(",*;*", "").trim();
					if (!current.equals("")
							&& !myAnnotationsList.containsKey(current)) {
						// Add new myTopicsLink
						createMyTopicLinkPannel(current);
						if (conceptSuggestion != null)
							myAnnotationsList.put(current, conceptSuggestion
									.getConcept().getURI());
						else
							myAnnotationsList.put(current, "");
					}
					conceptSuggestBox.setText("");
					return;
				}
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					current = current.trim();
					if (!current.equals("")
							&& !myAnnotationsList.containsKey(current)) {
						createMyTopicLinkPannel(current);
						if (conceptSuggestion != null)
							myAnnotationsList.put(current, conceptSuggestion
									.getConcept().getURI());
						else
							myAnnotationsList.put(current, "");

					}
					conceptSuggestBox.setText("");
				}
			}
		});

		conceptSuggestBox
				.addSelectionHandler(new SelectionHandler<Suggestion>() {
					@Override
					public void onSelection(SelectionEvent<Suggestion> event) {
						conceptSuggestion = (ConceptMultiWordSuggestion) event
								.getSelectedItem();
						String current = conceptSuggestBox.getText().trim();
						if (!current.equals("")
								&& !myAnnotationsList.containsKey(current)) {
							createMyTopicLinkPannel(current);
							if (conceptSuggestion != null)
								myAnnotationsList
										.put(current, conceptSuggestion
												.getConcept().getURI());
							else
								myAnnotationsList.put(current, "");
						}
						conceptSuggestBox.setText("");
					}
				});
		HTML html = new HTML(constants.newTopic());
		html.setStyleName("gwtLabelAnnotate");
		flexTable.setWidget(7, 0, html);
		flexTable.setWidget(7, 1, conceptSuggestBox);
		flexTable.setWidget(pos, 0, buttonPanel);

	}

	private void createRemoveAnnotationButton(int pos) {
		cellFormatter.setHorizontalAlignment(8, 1,
				HasHorizontalAlignment.ALIGN_CENTER);
	}

	private void createOthersManualTagsPanel(int pos, String width) {
		String captionForCurrentTopicsCaptionPannel = "<font color=\"#705132\">"
				+ constants.peoplesTopics() + "</font>";
		CaptionPanel peoplesTopicsCaptionPanel = new CaptionPanel(
				captionForCurrentTopicsCaptionPannel, true);
		peoplesTopicsCaptionPanel.setStyleName("gwt-peoplesTopicsCaptionPanel");
		flexTable.setWidget(4, 0, peoplesTopicsCaptionPanel);
		spacePanelForEmptyTopicsPanel.setHeight("50px");
		othersManualTagsPanel.add(spacePanelForEmptyTopicsPanel);
		othersManualTagsPanel.setStyleName("otherManualTagsPanel");
		peoplesTopicsCaptionPanel.setContentWidget(othersManualTagsPanel);
		peoplesTopicsCaptionPanel.setWidth(width);
	}

	private void createEmailPanel(int pos, String width) {
		HTML html = new HTML(constants.email());
		html.setStyleName("gwtLabelAnnotate");
		flexTable.setWidget(pos, 0, html);
		email = new HTML();
		email.setText(" ");
		email.setWidth(width);
		email.setStyleName("peopleNameEmail");
		flexTable.setWidget(pos, 1, email);
	}

	private void createNamePanel(int pos, String width) {
		HTML html = new HTML(constants.name());
		html.setStyleName("gwtLabelAnnotate");
		flexTable.setWidget(pos, 0, html);
		name = new HTML();
		name.setText(" ");
		name.setStyleName("peopleNameEmail");
		name.setWidth(width);
		flexTable.setWidget(pos, 1, name);
	}

	private void createTopicInputPanel(int pos, String width) {
		// Fill the ClientSideConcept objects
		getAnnotatePeopleService().getConcepts(spacename, creds,
				new AsyncCallback<Collection<ClientSideConcept>>() {
					@Override
					public void onFailure(Throwable caught) {
						handleException(caught);
					}

					@Override
					public void onSuccess(Collection<ClientSideConcept> result) {
						for (ClientSideConcept c : result) {
							conceptOracle
									.add(new ConceptMultiWordSuggestion(c));
							for (LocalizedString altLabel : c.getTexts(
									SKOS.ALT_LABEL, userLanguage)) {
								ClientSideConcept newConcept = c;
								conceptOracle
										.add(new ConceptMultiWordSuggestion(
												newConcept, altLabel
														.getString()));
							}
						}
					}
				});
	}

	private void updateMyAnnotationsBox(final String label,
			final AsyncCallback<Void> callback) {
		final String prefLabel = prepareLabel(label);
		getAnnotatePeopleService().getConceptForLabel(spacename, creds,
				new LocalizedString(prefLabel, userLanguage),
				new AsyncCallback<ClientSideConcept>() {
					public void onSuccess(ClientSideConcept concept) {
						createMyTopicLinkPannel(label);
						if (concept != null)
							myAnnotationsList.put(label, concept.getURI());
						else
							myAnnotationsList.put(label, "");
						if (callback != null)
							callback.onSuccess(null);
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
	}
	
	private void updateMyAnnotationsBox(ClientSideConcept toAdd, String label){
		createMyTopicLinkPannel(label);
		myAnnotationsList.put(label, toAdd.getURI());
	}

	/**
	 * Shows the exception popup if an Exception occurred.
	 * 
	 * @param t
	 */
	public static void handleException(Throwable t) {
		
		/*
		AnnotatePeoplePopup popup = new AnnotatePeoplePopup(t);
		popup.show();
		*/
		if(t.getMessage().contains("201")) WindowUtils.redirect(ClientUtil.getAbsURL() +"login?redirectURL=" + URL.encode(WindowUtils.getLocation().getHref()));
//		else Window.alert(t.getLocalizedMessage());
		else WindowUtils.redirect(ClientUtil.getAbsURL() +"login?redirectURL=" + URL.encode(WindowUtils.getLocation().getHref()));
	}

	/**
	 * returns the Annotate People Service used to save Tags ....
	 * 
	 * @return AnnotatePeopleServiceAsync
	 */
	public static AnnotatePeopleServiceAsync getAnnotatePeopleService() {
		if (annoService == null) {
			annoService = (AnnotatePeopleServiceAsync) GWT
					.create(AnnotatePeopleService.class);
			ServiceDefTarget endpoint = (ServiceDefTarget) annoService;
			endpoint.setServiceEntryPoint(ClientUtil.getServerURL()
					+ "annotatePeople/service");
		}
		return annoService;
	}

	private void saveAnnotation() {
		// delete other user tags if it is possible
		if (ClientUtil.getAnnotatePeopleDeleteTopic()) {
			if (!conceptsToDeleteOtherTopics.isEmpty()) {
				String spacename = ClientUtil.getSpaceName();
				EventSenderCredentials eventSenderCredentials = ClientUtil
						.getEventSenderCredentials();
				for (final String concept : conceptsToDeleteOtherTopics) {
					Window.alert(concept);
					getAnnotatePeopleService().removeTag(spacename,
							eventSenderCredentials, taggedPerson, concept,
							new AsyncCallback<Void>() {
								public void onFailure(Throwable caught) {
									handleException(caught);
								}
								public void onSuccess(Void arg0) {
									if (myAnnotationsList.containsKey(concept)) {
										myAnnotationsList.remove(concept);										
									}
								}
							});
				}
			}
		}
		getAnnotatePeopleService().saveAnnotationData(
				ClientUtil.getSpaceName(),
				ClientUtil.getEventSenderCredentials(),
				ClientUtil.getUserLanguage(), taggedPerson, myAnnotationsList,
				webURL, new AsyncCallback<Void>() {
					public void onSuccess(Void result) {
						
						/*
						new AnnotatePeoplePopup(AnnotatePeopleMain.this)
								.showSuccessDialog(constants.success(), "");
						*/
						
						Window.alert(constants.success());
						updateWindow();
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});

	}

	private void updateWindow()
	{
		String windowLocation = "";
		try {
			windowLocation = Window.Location.getHref();
		} catch (Exception ex) {
			windowLocation = "";
		}
		// if on soboleo page, reload annotatemain with empty values
		if (windowLocation.contains("annotation.jsp"))
		{
			resetWindow();
		}
		else
		{
			AnnotatePeopleMain.closeWindow();
			
			// update the caller window
			try
			{
				AnnotatePeopleMain.reloadOpenerWindow();
			}
			catch(Exception e)
			{
				
			}
		}			
	}
	
	
	public void resetWindow()
	{
		this.remove(flexTable);
		
		this.taggedPerson=null;
		taggedPersonEmail=null;
		conceptLabelToAdd=null;
		conceptOracle = new ConceptNameSuggestOracle(
		"(, ");
		createAnnotate();
	}
	
	
	private String prepareLabel(String label) {
		if (label.indexOf("(") != -1 && label.indexOf(")") != -1) {
			int index1 = label.indexOf("(");
			int index2 = label.lastIndexOf(")");
			return label.substring(index1 + 1, index2);
		} else
			return label;
	}

	private void createOthersManualTagsList(Set<ManualPersonTag> manualTags,
			final FlowPanel panel) {
		final Map<String, Integer> othersConceptURIs = new HashMap<String, Integer>();
		for (ManualPersonTag tag : manualTags) {
			if (!tag.getUserID().equals(creds.getSenderURI())) {
				Integer count = new Integer(0);
				if (othersConceptURIs.get(tag.getConceptID()) != null)
					count = othersConceptURIs.get(tag.getConceptID());
				othersConceptURIs.put(tag.getConceptID(), count + 1);
			}
		}

		getAnnotatePeopleService().getConceptsForURIs(spacename, creds,
				new HashSet<String>(othersConceptURIs.keySet()),
				new AsyncCallback<Set<ClientSideConcept>>() {
					public void onSuccess(Set<ClientSideConcept> concepts) {
						if (!concepts.isEmpty()) {
							for (ClientSideConcept c : concepts) {
								// Insert new component
								boolean deletable = false;
								if (ClientUtil.getAnnotatePeopleDeleteTopic()) {
									if (userName.toLowerCase().contentEquals(
											taggedPerson.getName()
													.toLowerCase())) {
										deletable = true;
									}
								}
								final ClientSideConcept currentConcept = c;
								final String currentLabel = c.getBestFitText(
										SKOS.PREF_LABEL, userLanguage,
										ClientUtil.getSpaceLanguage(),
										Language.en).getString();
								final TopicLinkWithOptionalDelete topicLinkWithOptionalDelete = new TopicLinkWithOptionalDelete(
										currentLabel,
										othersConceptURIs.get(c.getURI())
												.toString()
												+ "x",
										new ClickHandler() {
											// add to myAnnotateTopics
											public void onClick(ClickEvent event) {
												String conceptName = currentLabel;
												if (!conceptName.equals("")
														&& !myAnnotationsList
																.containsKey(conceptName)) {
													updateMyAnnotationsBox(currentConcept, conceptName);
												}
											}
										}, taggedPerson,
										conceptsToDeleteOtherTopics, c,
										deletable);
								panel.remove(spacePanelForEmptyTopicsPanel);
								panel.add(topicLinkWithOptionalDelete);
							}
						} else {
							HTML empty = new HTML(constants
									.noTopicsAssociated());
							empty.setStyleName("recommendationLables");
							panel.remove(spacePanelForEmptyTopicsPanel);
							panel.add(empty);
						}
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
	}

	private void setTaggedPerson(TaggedPerson taggedPerson) {
		this.taggedPerson = taggedPerson;
	}

	private void fillData() {
		this.email.setText(taggedPerson.getEmail());
		this.name.setText(taggedPerson.getName());
		if (this.webURL == null)
			this.webURL = ClientUtil.getAbsURL()
					+ "people?space="
					+ spacename
					+ "&taggedPerson="
					+ taggedPerson.getURI().substring(
							taggedPerson.getURI().indexOf("#") + 1);
		Set<ManualPersonTag> myTags = taggedPerson.getUserTags(creds
				.getSenderURI());
		Set<String> myConceptURIs = new HashSet<String>();
		for (PersonTag tag : myTags)
			myConceptURIs.add(tag.getConceptID());
		getAnnotatePeopleService().getConceptsForURIs(spacename, creds,
				myConceptURIs, new AsyncCallback<Set<ClientSideConcept>>() {
					public void onSuccess(Set<ClientSideConcept> concepts) {
						for (ClientSideConcept c : concepts) {
							String currentLabel = c.getBestFitText(
									SKOS.PREF_LABEL, userLanguage,
									ClientUtil.getSpaceLanguage(), Language.en)
									.getString();
							myAnnotationsList.put(currentLabel, c.getURI());
							// Add TopicLink panel to flex panel pannelMyTopics
							createMyTopicLinkPannel(currentLabel);
						}
						if (concepts.size() > 0)
							removeFromIndexButton.setEnabled(true);
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
		Set<ManualPersonTag> allTags = taggedPerson.getManualTags();
		createOthersManualTagsList(allTags, othersManualTagsPanel);
	}

	private static native void focusWindow() /*-{
												return $wnd.focus();
												}-*/;

	private static native void blurWindow() /*-{
											return $wnd.blur();
											}-*/;

	public static native void closeWindow()/*-{
											return $wnd.close();
											}-*/;

	/**
	 * This is a native JSNI function. This allows to execute java script code
	 * in java code. It execute the window.opener.location.reload() command and
	 * reloads the window, which has opened the annotate window. Used to update
	 * the edited data.
	 */
	private static native void reloadOpenerWindow() /*-{
													return $wnd.opener.location.reload();
													}-*/;

	private void createRecommendationPanel(int pos, String width) {
		
		String captionForSuggestedTopicsCaptionPannel = "<font color=\"#705132\">"
			+ constants.suggestedTopics() + "</font>";
	CaptionPanel suggestedTopicsCaptionPanel = new CaptionPanel(
			captionForSuggestedTopicsCaptionPannel, true);
	suggestedTopicsCaptionPanel.setStyleName("gwt-peoplesTopicsCaptionPanel");
	flexTable.setWidget(pos, 0, suggestedTopicsCaptionPanel);
	spacePanelForEmptySuggestedTopicsPanel.setHeight("50px");
	extractedConceptsPanel.add(spacePanelForEmptySuggestedTopicsPanel);
	extractedConceptsPanel.setStyleName("otherManualTagsPanel");
	suggestedTopicsCaptionPanel.setContentWidget(extractedConceptsPanel);
	suggestedTopicsCaptionPanel.setWidth(width);
		

		 getAnnotatePeopleService().getConceptRecommendation(taggedPerson,spacename, userLanguage,creds, new
		 AsyncCallback<List<ConceptStatistic>>() {
		
					
		 public void onSuccess (List<ConceptStatistic> recommendationResult){
			 createRecommendedLabelsLinkList(recommendationResult, extractedConceptsPanel);
		 }
		 public void onFailure(Throwable caught) {
		 handleException(caught);
		 }
		 });
	}

	// adds a new TopicLinkPanel to the panelMyTopics
	private void createMyTopicLinkPannel(String currentLabel) {
		pannelMyTopics.remove(spacePanelForEmptyTopicsPanel);
		TopicLink myTopicsLinkPannel = new TopicLink(currentLabel,
				myAnnotationsList, true);
		pannelMyTopics.add(myTopicsLinkPannel);
		/*
		 * int width = myTopicsLinkPannel.getOffsetWidth();
		 * myTopicsLinkPannel.setWidth(String.valueOf(width+5)+"px");
		 */
	}
	
	private void createRecommendedLabelsLinkList(List<ConceptStatistic> recommendedConcepts, FlowPanel panel) {
		if(!recommendedConcepts.isEmpty()){
			for (ConceptStatistic aggrConcept : recommendedConcepts) {			
				final String currentLabel = aggrConcept.getConcept().getBestFitText(SKOS.PREF_LABEL, userLanguage,ClientUtil.getSpaceLanguage(),Language.en).getString();
				final ClientSideConcept currentConcept = aggrConcept.getConcept();
				final TopicLinkWithOptionalDelete topicLink = new TopicLinkWithOptionalDelete(
						currentLabel,
						aggrConcept.getCount()+ "x",
						new ClickHandler() {
							// add to myAnnotateTopics
							public void onClick(ClickEvent event) {
								String conceptName = currentLabel;
								if (!conceptName.equals("")	&& !myAnnotationsList.containsKey(conceptName)) {
									updateMyAnnotationsBox(currentConcept, conceptName);
								}
							}
						}, taggedPerson,
						null, aggrConcept.getConcept(),
						false);
				panel.remove(spacePanelForEmptySuggestedTopicsPanel);
				panel.add(topicLink);
			}
		}else{
			HTML empty = new HTML(constants.noSuggestedTopics());
			empty.setStyleName("recommendationLables");
			panel.add(empty);
		}
	}
	
}