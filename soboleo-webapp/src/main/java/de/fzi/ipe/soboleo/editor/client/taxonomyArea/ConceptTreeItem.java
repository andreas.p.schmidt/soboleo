package de.fzi.ipe.soboleo.editor.client.taxonomyArea;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.server.client.ClientUtil;


class ConceptTreeItem extends TreeItem {
	
	private ClientSideConcept concept;
	
	
	public ConceptTreeItem(ClientSideConcept concept, ClientSideTaxonomy clientSideTaxonomy) {
		super();
		LocalizedString prefLabel = concept.getBestFitText(SKOS.PREF_LABEL, ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage());
		this.setHTML(prefLabel.getString());
//		this.setWidget(new Label(prefLabel.getString()));
//		this.getWidget().setStylePrimaryName("gwt-TreeLabel");
		this.concept = concept;
		Iterator<ClientSideConcept> it = concept.getConnectedAsConcepts(SKOS.HAS_NARROWER, clientSideTaxonomy).iterator();
		while(it.hasNext()) {
			ClientSideConcept sub = (ClientSideConcept) it.next();
			this.addItem(new ConceptTreeItem(sub,clientSideTaxonomy));
		}
//		if(prefLabel.getLanguage().equals(ClientUtil.getUserLanguage())) this.getWidget().removeStyleName("gwt-TreeLabelItalic");
//		else this.getWidget().addStyleName("gwt-TreeLabelItalic");
	}
	
	public ClientSideConcept getConcept() {
		return concept;
	}

	
	public void update(ClientSideConcept c, ClientSideTaxonomy clientSideTaxonomy, Tree tree) {
		if (concept == c) {
			LocalizedString prefLabel = concept.getBestFitText(SKOS.PREF_LABEL, ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage());
			this.setHTML(prefLabel.getString());
//			this.setWidget(new Label(prefLabel.getString()));
//			this.getWidget().setStylePrimaryName("gwt-TreeLabel");
//			if(prefLabel.getLanguage().equals(ClientUtil.getUserLanguage())) this.getWidget().removeStyleName("gwt-TreeLabelItalic");
//			else this.getWidget().addStyleName("gwt-TreeLabelItalic");
			Collection<ClientSideConcept> subs = concept.getConnectedAsConcepts(SKOS.HAS_NARROWER,clientSideTaxonomy);
			if (getChildCount() > subs.size()) {
				for (int i = 0; i < getChildCount(); i++) {
					if (!subs.contains(((ConceptTreeItem)getChild(i)).getConcept())) {
						if (getChild(i) == tree.getSelectedItem()) {
							tree.setSelectedItem(null,true);
						}						
						removeItem(getChild(i));
						break;
					}
				}
			}
			else if (getChildCount() < subs.size()) {
				HashSet<ClientSideConcept> temp = new HashSet<ClientSideConcept>();
				for (int i = 0; i < getChildCount(); i++) {
					temp.add(((ConceptTreeItem)getChild(i)).getConcept());
				}
				Iterator<ClientSideConcept> it = subs.iterator();
				while (it.hasNext()) {
					ClientSideConcept current = (ClientSideConcept) it.next();
					if (!temp.contains(current)) {
						addItem(new ConceptTreeItem(current,clientSideTaxonomy));
						break;
					}
				}
			}
		}
		for (int i=0;i<getChildCount();i++) {
			ConceptTreeItem currentChild = (ConceptTreeItem) getChild(i);
			currentChild.update(c,clientSideTaxonomy, tree);
		}
	}

}