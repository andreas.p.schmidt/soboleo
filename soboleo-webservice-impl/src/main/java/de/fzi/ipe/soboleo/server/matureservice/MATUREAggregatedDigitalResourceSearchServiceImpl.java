package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.springframework.beans.factory.annotation.Autowired;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.server.SoboleoServiceUtils;
import de.fzi.ipe.soboleo.space.Space;

/**
 * searching digital resources by tags (tagURIs) and aggregates several
 * digitalResourceSearchServices
 * 
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.6 in JDK 6 Generated
 * source version: 2.1
 * 
 */
@WebService(name = "MATURE-AggregatedDigitalResourceSearchService", targetNamespace = "http://mature-ip.eu/MATURE")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso( { ObjectFactory.class })
public class MATUREAggregatedDigitalResourceSearchServiceImpl extends MATUREService implements
		MATUREAggregatedDigitalResourceSearchService {

	@Autowired protected Server server;
	/**
	 * search digital resources that are, e.g. annotated with given tag
	 * 
	 * @param parameters
	 * @return returns de.fzi.ipe.soboleo.server.matureservice.search.
	 *         SearchDigitalResourcesByTagsResponse
	 * @throws InvalidKeyException
	 */
	@SuppressWarnings("unused")
	@WebMethod(action = NS + "/searchDigitalResourcesByTags")
	@WebResult(name = "searchDigitalResourcesByTagsResponse", targetNamespace = NS + "/extratypes", partName = "parameters")
	public SearchDigitalResourcesByTagsResponse searchDigitalResourcesByTags(
			@WebParam(name = "searchDigitalResourcesByTagsRequest", targetNamespace = "http://mature-ip.eu/MATURE", partName = "parameters") SearchDigitalResourcesByTagsRequest parameters)
			throws InvalidKeyException

	{
		ArrayList<SearchDigitalResourceEntry> list = new ArrayList<SearchDigitalResourceEntry>();
		try {
			EventSenderCredentials creds = SoboleoServiceUtils.getCredentials(server,parameters.agentKey);
			Space space = SoboleoServiceUtils.getDefaultSpace(server,parameters.agentKey);
			list.add(util().convertSearchDigitalResourceEntry());

			SearchDigitalResourcesByTagsResponse response = new SearchDigitalResourcesByTagsResponse();
			response.searchDigitalResourceResult = list;
			return response;

		} catch (EventPermissionDeniedException e) {
			InvalidKeyMsg message = new InvalidKeyMsg();
			message.setInvalidKeyMsg(e.getCause().toString());
			throw new InvalidKeyException("error", message);

		} catch (java.security.InvalidKeyException e) {
			InvalidKeyMsg message = new InvalidKeyMsg();
			message.setInvalidKeyMsg(e.getCause().toString());
			throw new InvalidKeyException("error", message);
		}
	}

	public Server getServer() { return server; }
}
