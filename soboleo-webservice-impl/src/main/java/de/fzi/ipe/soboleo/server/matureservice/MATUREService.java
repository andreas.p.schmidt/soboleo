package de.fzi.ipe.soboleo.server.matureservice;

import de.fzi.ipe.soboleo.server.Server;

public abstract class MATUREService {
	protected ServiceUtil serviceUtil;

	protected static final String NS = "http://mature-ip.eu/MATURE";
	public abstract Server getServer();
	
	protected ServiceUtil util()
	{
		if (serviceUtil == null)
			serviceUtil = new ServiceUtil(this);
		
		return serviceUtil;
	}

}
