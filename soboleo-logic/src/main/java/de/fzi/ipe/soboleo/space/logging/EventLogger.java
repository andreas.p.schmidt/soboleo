package de.fzi.ipe.soboleo.space.logging;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.space.Space;

public interface EventLogger {
	
	static class LogTask {
		Event event;
		Space space;
		
		LogTask(Event event, Space space) {
			this.event = event;
			this.space = space;
		}
	}
	
	public void log(Event event, Space space); 
}
