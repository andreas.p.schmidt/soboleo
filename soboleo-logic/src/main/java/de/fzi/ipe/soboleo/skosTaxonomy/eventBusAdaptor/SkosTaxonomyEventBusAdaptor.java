/*
 * This class is responsible for receiving query and command events related to the 
 * taxonomy from the event bus and realizing these in the TripleStore. 
 * FZI - Information Process Engineering 
 * Created on 23.01.2009 by zach
 */
package de.fzi.ipe.soboleo.skosTaxonomy.eventBusAdaptor;

import java.io.IOException;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openrdf.model.Value;
import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.AddTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ChangeTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ComplexTaxonomyChangeCommand;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveTextCmd;
import de.fzi.ipe.soboleo.event.ontology.TaxonomyChangeCommand;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveAddConnection;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveAddText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveChangeText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveCreateConcept;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveConcept;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveConnection;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveText;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.eventbus.EventBusCommandProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusPermitter;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;
import de.fzi.ipe.soboleo.ontology.rdf.RDFConcept;
import de.fzi.ipe.soboleo.ontology.rdf.RDFTaxonomy;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetWebDocumentsForConcept;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetWebDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersons;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsByNameAndTopic;
import de.fzi.ipe.soboleo.queries.rdf.skos.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.queries.rdf.skos.GetConceptsForLabel;

@Component
public class SkosTaxonomyEventBusAdaptor implements EventBusCommandProcessor, EventBusQueryProcessor, EventBusPermitter {

	private RDFTaxonomy tax;
	private EventBus eventBus;
	private ClientSideTaxonomy clientSideTax=null; 
	protected static Logger logger = Logger.getLogger(SkosTaxonomyEventBusAdaptor.class);
	
	public SkosTaxonomyEventBusAdaptor(TripleStore tripleStore, EventBus eventBus) {
		this.tax = new RDFTaxonomy(tripleStore);
		this.eventBus = eventBus;
	}

	private void initializeClientSideTaxonomy(long eventId) {
		try {
			if (clientSideTax == null) {
				logger.debug("Need to initialize ClientSideTaxonomy...");
				clientSideTax = this.tax.getClientSideTaxonomy(eventId);
				eventBus.addListener(new TaxonomyUpdater(clientSideTax));	
				
				if (clientSideTax == null)
					logger.error("Client side taxonomy in SkosTaxonomyEBAdaptor is null");
				assert clientSideTax!=null;
			}
		} catch (IOException ioe) {
			logger.error("IOException while building client side taxonomy",ioe);
			throw new RuntimeException(ioe);
		}
	}
	
	@Override
	public Object process(CommandEvent command) {
		try {
			//we actually only process complex taxonomy change commands - only for this does prepare and 
			// stuff work. This is to discourage accidential use of primitive commands.
			if (command instanceof ComplexTaxonomyChangeCommand) { 				
				TaxonomyChangeCommand cmd = (TaxonomyChangeCommand) command;
				return processTaxonomyCommand(cmd);
			}
			else return null;
		} catch (IOException io) {
			throw new RuntimeException(io);
		}
	}
	
	private Object processTaxonomyCommand(TaxonomyChangeCommand cmd) throws IOException {
		if (cmd instanceof ComplexTaxonomyChangeCommand) {
			Object toReturn = null;
			ComplexTaxonomyChangeCommand complex = (ComplexTaxonomyChangeCommand) cmd;
			for (TaxonomyChangeCommand implied:complex.getImpliedCommands()) {
				Object result = processTaxonomyCommand(implied);
				if (result != null) toReturn = result;
			}
			return toReturn;
		}
		else if (cmd instanceof PrimitiveCreateConcept) {
			PrimitiveCreateConcept current = (PrimitiveCreateConcept) cmd;
			RDFConcept createdConcept = this.tax.createConcept();
			current.setURI(createdConcept.getURI().toString());
			createdConcept.addText(current.getInitialName(), SKOS.PREF_LABEL);
			return createdConcept.getURI().toString();
		}
		else if (cmd instanceof PrimitiveRemoveConcept) {
			PrimitiveRemoveConcept current = (PrimitiveRemoveConcept) cmd;
			RDFConcept c = this.tax.getConcept(current.getURI());
			this.tax.deleteConcept(c);
		}
		else if (cmd instanceof PrimitiveAddText) {
			PrimitiveAddText addText = (PrimitiveAddText) cmd;
			RDFConcept c = this.tax.getConcept(addText.getFromURI());
			c.addText(addText.getText(), addText.getConnection());
		}
		else if (cmd instanceof PrimitiveRemoveText) {
			PrimitiveRemoveText removeText = (PrimitiveRemoveText) cmd;
			RDFConcept c = this.tax.getConcept(removeText.getFromURI());
			c.removeText(removeText.getText(), removeText.getConnection());
		}
		else if (cmd instanceof PrimitiveChangeText) {
			PrimitiveChangeText changeText = (PrimitiveChangeText) cmd;
			RDFConcept c = this.tax.getConcept(changeText.getFromURI());
			c.removeText(changeText.getOldText(), changeText.getConnection());
			c.addText(changeText.getNewText(), changeText.getConnection());
		}
		else if (cmd instanceof PrimitiveAddConnection) {
			PrimitiveAddConnection addConnection = (PrimitiveAddConnection) cmd;
			RDFConcept from = this.tax.getConcept(addConnection.getFromURI());
			RDFConcept to = this.tax.getConcept(addConnection.getToURI());
			from.addConnection(addConnection.getConnection(), to);
		}
		else if (cmd instanceof PrimitiveRemoveConnection) {
			PrimitiveRemoveConnection removeConnection = (PrimitiveRemoveConnection) cmd;
			RDFConcept from = this.tax.getConcept(removeConnection.getFromURI());
			RDFConcept to = this.tax.getConcept(removeConnection.getToURI());
			from.removeConnection(removeConnection.getConnection(),to);
		}
		else throw new RuntimeException("Taxonomy Change unknown to SkosTaxonomyEventBusAdaptor");
		
		return null;
	}

	@Override
	public CommandEvent prepare(CommandEvent command) {
		initializeClientSideTaxonomy(command.getId());
		try {
			if (command instanceof ComplexTaxonomyChangeCommand) {
				ComplexTaxonomyChangeCommand complexCmd = (ComplexTaxonomyChangeCommand) command;
				return prepareTaxonomyCommand(complexCmd);
			}
			return null;
		} catch (IOException io) {
			throw new RuntimeException(io);
		}
	}

	private CommandEvent prepareTaxonomyCommand(ComplexTaxonomyChangeCommand cmd) throws IOException {
		if (cmd.getImpliedCommands().size() >0) return null;
		else if (cmd instanceof AddConnectionCmd) {
			AddConnectionCmd current = (AddConnectionCmd) cmd;
			current.addImpliedCommand(new PrimitiveAddConnection(current.getFromURI(),current.getConnection(),current.getToURI()));
			SKOS reverseConnection = current.getConnection();
			if (current.getConnection() == SKOS.HAS_BROADER) reverseConnection = SKOS.HAS_NARROWER;
			else if (current.getConnection() == SKOS.HAS_NARROWER) reverseConnection = SKOS.HAS_BROADER;
			current.addImpliedCommand(new PrimitiveAddConnection(current.getToURI(), reverseConnection,current.getFromURI()));
		}
		else if (cmd instanceof RemoveConnectionCmd) {
			RemoveConnectionCmd current = (RemoveConnectionCmd) cmd;
			current.addImpliedCommand(new PrimitiveRemoveConnection(current.getFromConceptURI(),current.getConnection(),current.getToConceptURI()));
			SKOS reverseConnection = current.getConnection();
			if (current.getConnection() == SKOS.HAS_BROADER) reverseConnection = SKOS.HAS_NARROWER;
			else if (current.getConnection() == SKOS.HAS_NARROWER) reverseConnection = SKOS.HAS_BROADER;
			current.addImpliedCommand(new PrimitiveRemoveConnection(current.getToConceptURI(), reverseConnection,current.getFromConceptURI()));
		}
		else if (cmd instanceof AddTextCmd) {
			AddTextCmd current = (AddTextCmd) cmd;
			current.addImpliedCommand(new PrimitiveAddText(current.getFromConceptURI(),current.getConnection(),current.getText()));
		}
		else if (cmd instanceof RemoveTextCmd) {
			RemoveTextCmd current = (RemoveTextCmd) cmd;
			current.addImpliedCommand(new PrimitiveRemoveText(current.getFromConceptURI(),current.getConnection(),current.getText()));
		}
		else if (cmd instanceof ChangeTextCmd) {
			ChangeTextCmd current = (ChangeTextCmd) cmd;
			current.addImpliedCommand(new PrimitiveChangeText(current.getFromConceptURI(),current.getConnection(),current.getOldText(), current.getNewText()));
		}
		else if (cmd instanceof CreateConceptCmd) {
			CreateConceptCmd current = (CreateConceptCmd) cmd;
			current.addImpliedCommand(new PrimitiveCreateConcept(current.getInitialName()));
		}
		else if (cmd instanceof RemoveConceptCmd) {
			RemoveConceptCmd current = (RemoveConceptCmd) cmd;
			RDFConcept concept = this.tax.getConcept(current.getConceptURI());
			if (concept != null) {
				removeConnections(concept,SKOS.RELATED,current);
				removeConnections(concept,SKOS.HAS_BROADER,current);
				removeConnections(concept,SKOS.HAS_NARROWER,current);
				removeTexts(concept,SKOS.NOTE,current);
				removeTexts(concept,SKOS.PREF_LABEL,current);
				removeTexts(concept,SKOS.HIDDEN_LABEL,current);
				removeTexts(concept,SKOS.ALT_LABEL,current);				
			}
			current.addImpliedCommand(new PrimitiveRemoveConcept(current.getConceptURI()));
		}
		else {
			throw new RuntimeException("Unexpected Event Type for SkosTaxonomyEventBusAdaptor");
		}
		return cmd;
	}

	private void removeTexts(RDFConcept from, SKOS connectionType, RemoveConceptCmd cmd) throws IOException {
		Set<LocalizedString> texts = from.getTexts(connectionType);
		for (LocalizedString current: texts) {
			ComplexTaxonomyChangeCommand newCmd = new RemoveTextCmd(from.getURI().toString(), connectionType,current);
			prepareTaxonomyCommand(newCmd);
			cmd.addImpliedCommand(newCmd);
		}
	}
	
	private void removeConnections(RDFConcept from, SKOS connectionType,RemoveConceptCmd cmd) throws IOException {
		Set<RDFConcept> connected = from.getConnected(connectionType);
		for (RDFConcept current: connected) {
			ComplexTaxonomyChangeCommand newCmd = new RemoveConnectionCmd(from.getURI().toString(),connectionType,current.getURI().toString());
			prepareTaxonomyCommand(newCmd);
			cmd.addImpliedCommand(newCmd);
		}
	}
	
	@Override 
	public String permitEvent(Event event, EventSenderCredentials credentials) {
		return null;
	}
	
	@Override
	public String permitCommand(CommandEvent event, EventSenderCredentials credentials) {
		if (event instanceof TaxonomyChangeCommand) {
			try {
				return checkTaxonomyChangeCommand((TaxonomyChangeCommand) event);
			} catch (IOException ioe) {
				return ioe.getMessage();
			}
		}
		else return null;
	}

	private String checkTaxonomyChangeCommand(TaxonomyChangeCommand cmd) throws IOException{
		//note that this method only checks each primitive event on its own
		// this may cause problems if a problem can only be seen after some number of primitive events (part of the same 
		//complex event) are executed already. This is not a problem for all the 'normal' events we had in soboleo1
		if (cmd instanceof ComplexTaxonomyChangeCommand) {
			ComplexTaxonomyChangeCommand complexCmd = (ComplexTaxonomyChangeCommand) cmd;
			for (TaxonomyChangeCommand c: complexCmd.getImpliedCommands()) {
				String temp = checkTaxonomyChangeCommand(c);
				if (temp != null) return temp;
			}
			return null;
		}
		else if (cmd instanceof PrimitiveAddConnection) {
			PrimitiveAddConnection addConnection = (PrimitiveAddConnection) cmd;
			RDFConcept from = this.tax.getConcept(addConnection.getFromURI());
			RDFConcept to = this.tax.getConcept(addConnection.getToURI());
			if (addConnection.getConnection() == SKOS.HAS_BROADER) return from.checkAddConnection(addConnection.getConnection(), to, false);
			else if (addConnection.getConnection() == SKOS.HAS_NARROWER) return from.checkAddConnection(addConnection.getConnection(), to, false);
			else return from.checkAddConnection(addConnection.getConnection(),to,true);
		}
		else if (cmd instanceof PrimitiveRemoveConnection) {
			PrimitiveRemoveConnection removeConnection = (PrimitiveRemoveConnection) cmd;
			RDFConcept from = this.tax.getConcept(removeConnection.getFromURI());
			RDFConcept to = this.tax.getConcept(removeConnection.getToURI());
			return from.checkRemoveConnection(removeConnection.getConnection(), to);
		}
		else if (cmd instanceof PrimitiveAddText) {
			PrimitiveAddText addText = (PrimitiveAddText) cmd;
			RDFConcept from = this.tax.getConcept(addText.getFromURI());
			if(addText.getConnection().equals(SKOS.PREF_LABEL)){
				RDFConcept c = this.tax.getConceptByPrefLabel(addText.getText());
				Set<Value> possibleConcepts = this.tax.checkPrefLabel(addText.getText());// remove this line for case sensitive concepts
				if(!possibleConcepts.isEmpty())// remove this line for case sensitive concepts
				{
					for(Value v  : possibleConcepts)// remove this line for case sensitive concepts
					{// remove this line for case sensitive concepts
						//System.out.println(" Value: " + v.stringValue() + " From URI: " + from.getURI().stringValue());
						if(!v.stringValue().contentEquals(from.getURI().stringValue()))// remove this line for case sensitive concepts
						{
							return "[error_113] A topic with this preferred label already exists";// remove this line for case sensitive concepts
						}
					}
				}
				if(c != null && c != from) return "[error_113] A topic with this preferred label already exists";
			}
			return from.checkAddText(addText.getText(), addText.getConnection(), isUniquePerLanguage(addText.getConnection()));
		}
		else if (cmd instanceof PrimitiveRemoveText) {
			PrimitiveRemoveText removeText = (PrimitiveRemoveText) cmd;
			RDFConcept from = this.tax.getConcept(removeText.getFromURI());
			if (removeText.getConnection().equals(SKOS.PREF_LABEL) && !isPartOfDeleteConcept(cmd)) {
				Set<LocalizedString> texts = from.getTexts(SKOS.PREF_LABEL);
				if (texts.size() <= 1) return "[error_114] Please do not delete the last preferred label for this topic";
			}
			return from.checkRemoveText(removeText.getText(), removeText.getConnection());
		}
		else if (cmd instanceof PrimitiveChangeText) {
			PrimitiveChangeText changeText = (PrimitiveChangeText) cmd;
			RDFConcept from = this.tax.getConcept(changeText.getFromURI());
			String removeOk = from.checkRemoveText(changeText.getOldText(), changeText.getConnection());
			if (removeOk != null) return removeOk;
			else {
				if(changeText.getConnection().equals(SKOS.PREF_LABEL))
				{
					RDFConcept c = this.tax.getConceptByPrefLabel(changeText.getNewText());
					//System.out.println("Entering  CheckPrefLabel");
					Set<Value> possibleConcepts = this.tax.checkPrefLabel(changeText.getNewText());// remove this line for case sensitive concepts
					if(!possibleConcepts.isEmpty())// remove this line for case sensitive concepts
					{
						for(Value v  : possibleConcepts)// remove this line for case sensitive concepts
						{// remove this line for case sensitive concepts
							//System.out.println(" Value: " + v.stringValue() + " From URI: " + from.getURI().stringValue());
							if(!v.stringValue().contentEquals(from.getURI().stringValue()))// remove this line for case sensitive concepts
							{
								return "[error_113] A topic with this preferred label already exists";// remove this line for case sensitive concepts
							}
						}
					}
					if(c != null && c != from) return "[error_113] A topic with this preferred label already exists";
				}
				if (!changeText.getOldText().getLanguage().equals(changeText.getNewText().getLanguage())) 
				{ //the language of the text is changed
					return from.checkAddText(changeText.getNewText(), changeText.getConnection(), isUniquePerLanguage(changeText.getConnection()));
				}			
				else return from.checkChangeText(changeText.getOldText(), changeText.getNewText(), changeText.getConnection());
			}
		}
		else if (cmd instanceof PrimitiveCreateConcept) {
			PrimitiveCreateConcept newConcept = (PrimitiveCreateConcept) cmd;
			if(newConcept.getInitialName() == null || newConcept.getInitialName().getString().trim().isEmpty()) return "[error_104] The given label is blank";
			else if(this.tax.getConceptByPrefLabel(newConcept.getInitialName()) != null) return "[error_113] A topic with this preferred label already exists";
			else 
			{
				//System.out.println("Hallo das ist CheckPrefLabel existiert Conzept schon?");
				Set<Value> possibleConcepts = this.tax.checkPrefLabel(newConcept.getInitialName());// remove this line for case sensitive concepts
				if(!possibleConcepts.isEmpty())// remove this line for case sensitive concepts
				{
					//System.out.println("Existiert!!");
							return "[error_113] A topic with this preferred label already exists";// remove this line for case sensitive concepts
				}
				return null;
			}
		}
		else if (cmd instanceof PrimitiveRemoveConcept) {
			PrimitiveRemoveConcept removeConcept = (PrimitiveRemoveConcept) cmd;
			if (this.tax.getConcept(removeConcept.getURI()) == null) return "[error_115] No such topic to delete.";
			else return null;
		}
		else return "[error_116] Taxonomy Event Bus Adapter encountered unknown taxonomy change event! - Will not allow that!"; 
	}

	private boolean isPartOfDeleteConcept(TaxonomyChangeCommand e) {
		if (e instanceof RemoveConceptCmd) return true;
		else if (e.getParentEvent() == null) return false;
		else if (e.getParentEvent() instanceof TaxonomyChangeCommand) {
			TaxonomyChangeCommand parentCommand = (TaxonomyChangeCommand) e.getParentEvent();
			return isPartOfDeleteConcept(parentCommand);
		}
		else return false;
	
	}


	private boolean isUniquePerLanguage(SKOS connection) {
		return (connection == SKOS.PREF_LABEL || connection ==SKOS.NOTE);
	}
	
	@Override
	public QueryEvent prepare(QueryEvent query) {
		initializeClientSideTaxonomy(query.getId());
		ClientSideTaxonomy clientSideTaxonomy = null;
		try
		{ clientSideTaxonomy  = this.tax.getClientSideTaxonomy(query.getId()); }
		catch (IOException e)
		{throw new RuntimeException(e);}
		assert clientSideTaxonomy != null;
		
		if(query instanceof GetWebDocumentsForConcept){
				GetWebDocumentsForConcept getDocumentsForConcept = (GetWebDocumentsForConcept) query;
				getDocumentsForConcept.setSubconceptURIs(clientSideTaxonomy.getAllSubconceptURIs(getDocumentsForConcept.getConceptURI()));
		}
		else if(query instanceof GetWebDocumentsSearchResult){
				GetWebDocumentsSearchResult getSearchResult = (GetWebDocumentsSearchResult) query;
				getSearchResult.setMustSubconceptURIs(clientSideTaxonomy.getAllConnectedURIs(getSearchResult.getMustConceptURIs(),SKOS.HAS_NARROWER));
				Set<String> extractedConceptURIs = clientSideTaxonomy.getExtractedConceptURIs(getSearchResult.getQuery());
				getSearchResult.setExtractedConceptURIs(extractedConceptURIs);
				getSearchResult.setExtractedSubconceptURIs(clientSideTaxonomy.getAllConnectedURIs(extractedConceptURIs,SKOS.HAS_NARROWER));
		}
		else if(query instanceof GetDocumentsSearchResult){
				GetDocumentsSearchResult getSearchResult = (GetDocumentsSearchResult) query;
				getSearchResult.setMustSubconceptURIs(clientSideTaxonomy.getAllConnectedURIs(getSearchResult.getMustConceptURIs(),SKOS.HAS_NARROWER));
				Set<String> extractedConceptURIs = clientSideTaxonomy.getExtractedConceptURIs(getSearchResult.getQuery());
				getSearchResult.setExtractedConceptURIs(extractedConceptURIs);
				getSearchResult.setExtractedSubconceptURIs(clientSideTaxonomy.getAllConnectedURIs(extractedConceptURIs,SKOS.HAS_NARROWER));
		}
		else if(query instanceof SearchPersonsByNameAndTopic){
				SearchPersonsByNameAndTopic getSearchResult = (SearchPersonsByNameAndTopic) query;
				Set<String> extractedConceptURIs = clientSideTaxonomy.getExtractedConceptURIs(getSearchResult.getSearchString());
				getSearchResult.setExtractedConceptURIs(extractedConceptURIs);
//				getSearchResult.setExtractedSubconceptURIs(clientSideTaxonomy.getAllConnectedURIs(extractedConceptURIs,SKOS.HAS_NARROWER));
		}
		else if(query instanceof SearchPersons){
				SearchPersons getSearchResult = (SearchPersons) query;
				Set<String> extractedConceptURIs = clientSideTaxonomy.getExtractedConceptURIs(getSearchResult.getSearchString());
				getSearchResult.setExtractedConceptURIs(extractedConceptURIs);
//				getSearchResult.setExtractedSubconceptURIs(clientSideTaxonomy.getAllConnectedURIs(extractedConceptURIs,SKOS.HAS_NARROWER));
		}
		return null;
	}

	
	@Override
	public Object process(QueryEvent query) {
		initializeClientSideTaxonomy(query.getId());
		
		if (query instanceof GetClientSideTaxonomy) {
			try {
				GetClientSideTaxonomy getEvent = (GetClientSideTaxonomy) query;
				if (getEvent.getUpdate() == true) return clientSideTax;
				else {
					ClientSideTaxonomy toReturn = this.tax.getClientSideTaxonomy(query.getId());
					if (toReturn == null)
						logger.error("ClientSideTaxonomy from RDFTaxonomy is null");
					return toReturn; 
				}
			} catch (IOException ioe) { logger.error("",ioe); throw new RuntimeException(ioe);}
		}
		else if(query instanceof GetConceptsForLabel){
			GetConceptsForLabel getConceptsForLabel = (GetConceptsForLabel) query;
			if(getConceptsForLabel.getLanguage() != null){
				if(getConceptsForLabel.getProperty() != null)return clientSideTax.getConceptsForLabel(new LocalizedString(getConceptsForLabel.getLabel(), getConceptsForLabel.getLanguage()), getConceptsForLabel.getProperty(), true);
				else return clientSideTax.getConceptsForLabel(new LocalizedString(getConceptsForLabel.getLabel(), getConceptsForLabel.getLanguage()), true);
			}
			else{
				if(getConceptsForLabel.getProperty() != null)return clientSideTax.getConceptsForLabel(getConceptsForLabel.getLabel(), getConceptsForLabel.getProperty(), true);
				return clientSideTax.getConceptsForLabel(getConceptsForLabel.getLabel(), true);
			}
		}
		return null;
	}
}
