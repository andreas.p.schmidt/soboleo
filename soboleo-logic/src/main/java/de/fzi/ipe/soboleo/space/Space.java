/*
 * FZI - Information Process Engineering 
 * Created on 15.01.2009 by zach
 */
package de.fzi.ipe.soboleo.space;

import java.io.File;
import java.io.IOException;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.lucene.SearchingLuceneWrapper;
import de.fzi.ipe.soboleo.lucene.eventubus.LuceneEventBusAdaptor;

/**
 * The main class representing one collaboration space. 
 * Each space is represented by one directory on disk. 
 */
public interface Space {
	
	public static final String TAXONOMY_DIRECTORY = "taxonomy";
	public static final String INDEX_DIRECTORY = "index";
	public static final String STORAGE_DIRECTORY = "storage";
	public static final String LOG_DIRECTORY = "log";
	public static final String PROPERTY_FILE_NAME = "spaceProperties.prp";

	
	/**
	 * Returns the Space file
	 * @return File
	 */
	public File getSpaceDirectory();

	/**
	 * Closes the space and saves any possible changes. After calling this operation the space object should 
	 * be discarded; won't work anymore.
	 */
	public void close() throws IOException;

	public EventBus getEventBus();
	
	public String getURI();
	
	public String getProperty(SpaceProperties propertyName) throws IOException;
	
	public void setProperty(SpaceProperties propertyName, String value) throws IOException;

	
	public String getTripleStoreDump() throws IOException;
	
	public String getTaxonomyRDFDump() throws IOException; 
	
	public String getCompleteRDFDump() throws IOException; 
	
	public String getTripleStoreURI();
	
	public ClientSideTaxonomy getClientSideTaxonomy() ;
	
	public SearchingLuceneWrapper getSearchingLuceneWrapper();
	
	public LuceneEventBusAdaptor getLuceneEventBus();


}
