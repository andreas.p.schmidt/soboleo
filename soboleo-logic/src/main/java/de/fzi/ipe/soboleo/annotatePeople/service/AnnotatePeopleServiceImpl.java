package de.fzi.ipe.soboleo.annotatePeople.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import de.fzi.ipe.soboleo.annotatePeople.client.rpc.AnnotatePeopleService;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.ManualPersonTag;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.PersonTag;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.event.peopletagging.AddPersonTag;
import de.fzi.ipe.soboleo.event.peopletagging.AddTaggedPerson;
import de.fzi.ipe.soboleo.event.peopletagging.RemovePerson;
import de.fzi.ipe.soboleo.event.peopletagging.RemovePersonTag;
import de.fzi.ipe.soboleo.event.peopletagging.RemovePersonTagsByUser;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.execution.lucene.webdocument.SearchResult;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetAllTaggedPersons;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetTaggedPersonByURL;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetTaggedPersonForEmail;
import de.fzi.ipe.soboleo.server.AutowiringRemoteServiceServlet;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.user.User;

public class AnnotatePeopleServiceImpl extends AutowiringRemoteServiceServlet 
	implements AnnotatePeopleService {

	public final static int SUGESSTIONS_MAX_SIZE = 10;
	
	private final Logger logger = Logger.getLogger(this.getClass());
	private static final long serialVersionUID = 1L;

	@Autowired
	private Server server;
	
	public TaggedPerson getTaggedPersonByURL(String spaceName, EventSenderCredentials cred, Language userLanguage, String webURL) throws EventPermissionDeniedException{
		try{
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			TaggedPerson person = (TaggedPerson) currentSpace.getEventBus().executeQuery(new GetTaggedPersonByURL(webURL), cred);
//			System.out.println(person);
			return person;
		} catch (IOException e) {
			logger.error(e);
			return null; //TODO real error handling
		} 
	}
	
	public List<ConceptStatistic> getConceptRecommendation(TaggedPerson taggedPerson, String spaceName, Language userLanguage,EventSenderCredentials cred){
		try {
			logger.info("get space: " + spaceName);
			Space space = server.getSpaces().getSpace(spaceName);
			User userOfTaggedPerson = server.getUserDatabase().getUserForEmail(taggedPerson.getEmail());
			List<ConceptStatistic> statistic=new ArrayList<ConceptStatistic>();
			if(userOfTaggedPerson != null){
				GetDocumentsSearchResult gu = new GetDocumentsSearchResult("");
				
				gu.setAnnotationOwner(userOfTaggedPerson.getKey());
				
				SearchResult result = (SearchResult) space.getEventBus().executeQuery(gu, cred);
				
				ArrayList<String> extractedConcepts = new ArrayList<String>();
				Hashtable<String, Integer> conceptsCount = new Hashtable<String, Integer>();
				
				logger.info("result size: " +result.getLuceneResult().length());			
				// count the annotations
				for (IndexDocument doc:result.getLuceneResult()){
					logger.info("doc owner: " +doc.getDocumentOwner());
					
					// search the semantic annotations
					for (SemanticAnnotation sia:doc.getSemanticAnnotations()){
						if (sia.getUserId().equals(cred.getKey())){
							logger.info("> user is annotation owner for : " +sia.getUri());
							
							// add them to statistic
							if (conceptsCount.get(sia.getUri()) == null) {
								logger.info("add new concept: " + sia.getUri());
								conceptsCount.put(sia.getUri(), 1);
								extractedConcepts.add(sia.getUri());
							} else {
								logger.info("increase existing concept count: "
										+ sia.getUri());
								int count = conceptsCount.get(sia.getUri());
								count++;
								conceptsCount.put(sia.getUri(), count);
							}
						}
					}
				}
				
				// build the concepts
				for (String concept : extractedConcepts) {
					ConceptStatistic cs=new ConceptStatistic();
					cs.setConceptId(concept);
					cs.setCount(conceptsCount.get(concept));
					logger.info("get concept : " + cs.getConceptId());
					ClientSideConcept csc = space.getClientSideTaxonomy().get(
							cs.getConceptId());
					if(csc != null){
						logger.info("csc: " +csc.getURI());
						cs.setConcept(csc);
						statistic.add(cs);
					}
				}
			}
			if(statistic.size()>AnnotatePeopleServiceImpl.SUGESSTIONS_MAX_SIZE) return statistic.subList(0, AnnotatePeopleServiceImpl.SUGESSTIONS_MAX_SIZE);
			else return statistic;
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}


	public Set<String> getAllLabels(String spaceName, EventSenderCredentials cred, Language userLanguage) throws EventPermissionDeniedException{
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			Language spaceLanguage = LocalizedString.Language.getLanguage(currentSpace.getProperty(SpaceProperties.DEFAULT_LANGUAGE));
			ClientSideTaxonomy  clientSideTaxonomy = currentSpace.getClientSideTaxonomy();
			Collection<ClientSideConcept> allConcepts = clientSideTaxonomy.getConcepts();
					
			Set<String> prefsWithAlts= new HashSet<String>();
			for (ClientSideConcept current : allConcepts) {
				LocalizedString bestFitName = current.getBestFitText(SKOS.PREF_LABEL, userLanguage, spaceLanguage, Language.en);
				String nameOfCpt = bestFitName.getString(); 
//				if(nameOf)
				prefsWithAlts.add(nameOfCpt);
				//TODO which language to consider for alt labels?
				for (LocalizedString altLabel : current.getTexts(SKOS.ALT_LABEL, userLanguage)) {
						String name = altLabel.getString() + " " + "("+nameOfCpt+")";
						prefsWithAlts.add(name);						
				}
			}
			return prefsWithAlts;
		} catch (IOException e) {
			logger.error(e);
			return null; //TODO real error handling
		} 
	}

	public void saveAnnotationData(String spaceName, EventSenderCredentials cred, Language userLanguage, TaggedPerson taggedPerson, Map<String,String> conceptMap, String webURL) throws EventPermissionDeniedException{
		try{
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			EventBus eb = currentSpace.getEventBus();
			ClientSideTaxonomy clientSideTaxonomy = currentSpace.getClientSideTaxonomy();
			//check if tagged person already exists
			if (taggedPerson.getURI().equals(""))
			{
				TaggedPerson person = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson(taggedPerson.getEmail(), taggedPerson.getName()), cred);
				for(Entry<String,String> entry : conceptMap.entrySet()){
					// check if concept already exist
					String conceptURI = entry.getValue();
					if(conceptURI.equals("")){
						conceptURI = createNewConcept(cred, userLanguage, currentSpace, clientSideTaxonomy, entry.getKey());
					}
					eb.executeCommandNow(new AddPersonTag(person.getURI(), webURL, conceptURI, cred.getSenderURI()), cred);
				}
			}
			else
			{
			//TODO ggf. URL setzen
				// TODO ggf. TaggedPerson frisch holen
				Set<ManualPersonTag> tagsToRemove = taggedPerson.getUserTags(cred.getSenderURI());
				Set<String> usedConceptURIs = new HashSet<String>();
				for(Entry<String,String> entry : conceptMap.entrySet()){
					String conceptURI = entry.getValue();
					if(conceptURI.equals("")){
						conceptURI = createNewConcept(cred, userLanguage, currentSpace, clientSideTaxonomy, entry.getKey());
					}
					ManualPersonTag currentTag = taggedPerson.hasUserTag(cred.getSenderURI(), conceptURI); 
					if(currentTag == null && !usedConceptURIs.contains(conceptURI)) 
					{
						eb.executeCommandNow(new AddPersonTag(taggedPerson.getURI(), webURL, conceptURI, cred.getSenderURI()), cred);
					}
					else
					{
						tagsToRemove.remove(currentTag);
					}
					usedConceptURIs.add(conceptURI);
				}
				for(ManualPersonTag toRemove : tagsToRemove){
					eb.executeCommandNow(new RemovePersonTag(toRemove.getTagID(), taggedPerson.getURI(), toRemove.getConceptID()) , cred);
				}
			}
		} catch (IOException e) {
			logger.error(e.getLocalizedMessage());
		} 	
	}

	private String createNewConcept(EventSenderCredentials cred, Language userLanguage, Space space, ClientSideTaxonomy clientSideTaxonomy, String conceptLabel) throws EventPermissionDeniedException {
		String conceptURI;
		conceptURI = (String) space.getEventBus().executeCommandNow(new CreateConceptCmd(new LocalizedString(conceptLabel, userLanguage)), cred);
		// put new concept under "prototypical concepts" concept 
		ClientSideConcept proto;
		try {
			proto = clientSideTaxonomy.getConceptForPrefLabel(new LocalizedString(space.getProperty(SpaceProperties.PROTOTYPICAL_CONCEPT_NAME), Language.en));
			 //TODO how to deal with "prototypical concepts" container and multilinguality?
			String protoURI;
			// create "prototypical concepts" concept if does not exist
			if(proto == null) { 
				protoURI = (String) space.getEventBus().executeCommandNow(new CreateConceptCmd(new LocalizedString(space.getProperty(SpaceProperties.PROTOTYPICAL_CONCEPT_NAME))), cred);
			}
			else
				protoURI = proto.getURI();
			space.getEventBus().executeCommandNow(new AddConnectionCmd(conceptURI, SKOS.HAS_BROADER, protoURI), cred);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return conceptURI;
	}

	public void removeTagsForUser(String spaceName, EventSenderCredentials cred, String taggedPersonURI) throws EventPermissionDeniedException{
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			if(!taggedPersonURI.equals("")) currentSpace.getEventBus().executeCommandNow(new RemovePersonTagsByUser(taggedPersonURI, cred.getSenderURI()), cred);
		} catch (IOException e) {
			logger.error(e);
		} 
	}

		
	public Boolean removeUser(String spaceName, EventSenderCredentials cred, String taggedPersonURI)throws EventPermissionDeniedException
	{
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			if(!taggedPersonURI.equals("")) 
			{
				return (Boolean)(currentSpace.getEventBus().executeCommandNow(new RemovePerson(taggedPersonURI, cred.getSenderURI()), cred));
			}
		} catch (IOException e) {
			logger.error(e);
		} 
		return false;
	}

	
	/**
	 * Removes a single Tag from a person.
	 * @param spaceName
	 * @param cred
	 * @param taggedPersonURI
	 * @param tagToRemove
	 */
	public void removeTag(String spaceName, EventSenderCredentials cred, TaggedPerson taggedPerson,String conceptURI) {
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			if(taggedPerson != null){
				Set<PersonTag> tagsToRemove = taggedPerson.getTags(conceptURI);
				for(PersonTag toRemove : tagsToRemove){
					currentSpace.getEventBus().executeCommandNow(new RemovePersonTag(toRemove.getTagID(), taggedPerson.getURI(), toRemove.getConceptID()), cred);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		} 
	}
	
	public TaggedPerson getTaggedPersonByEmail(String spaceName, EventSenderCredentials cred, String email) throws EventPermissionDeniedException{
		try{
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			EventBus eb = currentSpace.getEventBus();
			TaggedPerson taggedPerson = (TaggedPerson) eb.executeQuery(new GetTaggedPersonForEmail(email), cred);
			// check if email belongs to an existing user; if so create a tagged person for her,
			if(taggedPerson == null){
				User user = server.getUserDatabase().getUserForEmail(email);
				if(user != null) taggedPerson = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson(email, user.getName()), cred);
			}
			return taggedPerson;
		} catch (IOException e) {
			logger.error(e);
			return null; //TODO real error handling
		}
	}
	
	public TaggedPerson createTaggedPerson(String spaceName, EventSenderCredentials cred, String email, String name) throws EventPermissionDeniedException{
		try{
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			EventBus eb = currentSpace.getEventBus();
			TaggedPerson taggedPerson = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson(email, name), cred);
			return taggedPerson;
		} catch (IOException e) {
			logger.error(e);
			return null; //TODO real error handling
		}
	}
	
	
	public Set<ClientSideConcept> getConceptsForURIs(String spaceName, EventSenderCredentials cred,Set<String> conceptURIs) throws EventPermissionDeniedException{
		Set<ClientSideConcept> concepts = new HashSet<ClientSideConcept>();
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			ClientSideTaxonomy  clientSideTaxonomy = currentSpace.getClientSideTaxonomy();
			for(String currentURI : conceptURIs){
				if(clientSideTaxonomy.get(currentURI) != null) concepts.add(clientSideTaxonomy.get(currentURI));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return concepts;
	}
	
	public ClientSideConcept getConceptForLabel(String spaceName, EventSenderCredentials cred, LocalizedString label) throws EventPermissionDeniedException{
		try{
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			ClientSideTaxonomy  clientSideTaxonomy = currentSpace.getClientSideTaxonomy();
			// TODO language stuff; --> suggestBox mit Konzepten
			ClientSideConcept concept = clientSideTaxonomy.getConceptForPrefLabel(label);
			if(concept == null){
				Set<ClientSideConcept> concepts = clientSideTaxonomy.getConceptsForPrefLabel(label.getString());
				Iterator<ClientSideConcept> it = concepts.iterator();
				if(it.hasNext()) return it.next();
				else return null;
			}
			return concept;
		} catch (IOException e) {
			logger.error(e);
			return null; //TODO real error handling
		}
	}
	
	@Override
	public void sendEvent(Event event, String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException {
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			space.getEventBus().sendEvent(event, cred);
		} catch (IOException e) {
			logger.error(e); //TODO realExceptionHandlings
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TaggedPerson> getTaggedPersons(String spaceName, EventSenderCredentials cred) {
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			return (List<TaggedPerson>) space.getEventBus().executeQuery(new GetAllTaggedPersons(), cred);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		} catch (EventPermissionDeniedException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return null;
	}

	@Override
	public Collection<ClientSideConcept> getConcepts(String spaceName, EventSenderCredentials cred) {
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			ClientSideTaxonomy  clientSideTaxonomy = currentSpace.getClientSideTaxonomy();
			Collection<ClientSideConcept> allConcepts = clientSideTaxonomy.getConcepts();
			List<ClientSideConcept> result = new ArrayList<ClientSideConcept>();
			for(ClientSideConcept c : allConcepts){
				result.add(c);
			}
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		} catch (EventPermissionDeniedException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return null;
	}
}
