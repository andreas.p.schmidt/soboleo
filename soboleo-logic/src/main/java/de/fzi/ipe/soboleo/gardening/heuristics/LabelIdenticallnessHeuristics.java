/*
 * FZI - Information Process Engineering 
 * Created on 02.02.2010 by zach
 */
package de.fzi.ipe.soboleo.gardening.heuristics;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptLabelIdenticalProblem;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

/**
 * Collects the heuristics that deal with labels that are identical (but that shouldn't be)
 */
public class LabelIdenticallnessHeuristics {

	public static Set<GardeningRecommendation> getRecommendations(ClientSideTaxonomy tax) {
		LabelSetMap labelMap = createLabelMap(tax);
		Set<GardeningRecommendation> toReturn = new HashSet<GardeningRecommendation>();
		for (ClientSideConcept concept:tax.getConcepts()) {
			for (SKOS labelType: SKOS.values()) {
				LocalizedString label = concept.getText(labelType);
				if (label != null) {
					ConceptLabelIdenticalProblem problem = checkLabel(labelMap, concept,labelType,label);
					if (problem != null) toReturn.add(problem);
				}
			}
		}
		return toReturn;
	}
	
	private static ConceptLabelIdenticalProblem checkLabel(LabelSetMap labelMap, ClientSideConcept concept, SKOS labelType, LocalizedString label) {
		ConceptLabelIdenticalProblem problem = null;
		for (LabelTupel currentTupel:labelMap.get(label.getString())) {
			if (concept.getURI().equals((currentTupel.concept.getURI()))) {
				continue; // don't bother - it is the same concept
			}
			else if (!label.getLanguage().equals(currentTupel.label.getLanguage())) {
				continue; // don't bother - different language
			}
			else {
				if (problem == null) problem = new ConceptLabelIdenticalProblem(concept,labelType,label);
				int prio = 8;
				if (labelType == SKOS.HIDDEN_LABEL && currentTupel.labelType == SKOS.HIDDEN_LABEL) prio = 9; 
				else if (labelType == SKOS.PREF_LABEL) prio = 6;
				else if (currentTupel.labelType == SKOS.PREF_LABEL) prio = 6;
				problem.addProblem(currentTupel.concept, currentTupel.labelType, currentTupel.label, prio);
			}
		}
		return problem;
	}

	private static LabelSetMap createLabelMap(ClientSideTaxonomy tax) {
		LabelSetMap toReturn = new LabelSetMap();
		for (ClientSideConcept concept:tax.getConcepts()) {
			for (SKOS labelType: SKOS.values()) {
				LocalizedString label = concept.getText(labelType);
				if (label != null) {
					toReturn.add(label.getString(),new LabelTupel(concept,labelType,label));
				}
			}
		}
		return toReturn;
	}
	
	
	private static class LabelSetMap {
		
		private Map<String,Set<LabelTupel>> data = new HashMap<String,Set<LabelTupel>>();
		
		void add(String label, LabelTupel labelTupel) {
			Set<LabelTupel> currentSet = data.get(label);
			if (currentSet == null) {
				currentSet = new HashSet<LabelTupel>();
				data.put(label, currentSet);
			}
			currentSet.add(labelTupel);
		}
		
		@SuppressWarnings("unchecked")
		Set<LabelTupel> get(String label) {
			if (data.get(label)!=null) return data.get(label);
			else return Collections.EMPTY_SET;
		}
		
	}
	
	
	private static class LabelTupel {
		final SKOS labelType;
		final LocalizedString label;
		final ClientSideConcept concept; 
		
		LabelTupel(ClientSideConcept concept, SKOS labelType, LocalizedString label) {
			this.labelType = labelType;
			this.label = label;
			this.concept = concept;
		}
	}
	
	
}
