package de.fzi.ipe.soboleo.gardening.recSys;
import java.util.Map;

public class CosineSimilarity
{
	public static double calcCosineSimilarity(int[] array1, int[] array2)
	{
		double numerator = 0;
		
		for(int i=0; i<array1.length; i++)
		{
			numerator += array1[i] * array2[i];
		}
		
		double dem1 = 0;
		double dem2 = 0;
		
		for(int i=0; i<array1.length; i++)
		{
			dem1 = dem1 += array1[i] * array1[i];
			dem2 = dem2 += array2[i] * array2[i];
		}
		
		double denominator = Math.sqrt(dem1) * Math.sqrt(dem2);
		
		double result = numerator / denominator;
		return result;
	}
	public static double calcCosineSimilarity(Map<Integer,Integer> m1, Map<Integer,Integer> m2)
	{
		double numerator = 0;
		
		for(Integer k: m1.keySet())
		{
			if(m2.containsKey(k))
			numerator += m1.get(k)*m2.get(k);
		}
		
		double dem1 = 0;
		double dem2 = 0;
		
		for(Integer k: m1.keySet())
		{
			
			dem1 += m1.get(k)*m1.get(k);
			
		}
		for(Integer k: m2.keySet())
		{
			
			dem2 += m2.get(k)*m2.get(k);
			
			
		}
		
		double denominator = Math.sqrt(dem1) * Math.sqrt(dem2);
		
		
		double result = numerator / denominator;
		return result;
	}
	public static double calcCosineSimilarity(intPair[] array1, intPair[] array2)
    {
        double numerator = 0;
        @SuppressWarnings("unused")
		int match = 0;
   
        int i = 0;
        int j = 0;
       
        while(i < array1.length && j < array2.length)
        {
            if(array1[i].b == array2[j].b)
            {
                numerator += array1[i].a * array2[j].a;
                match++;
               
                i++;
                j++;
            }
            else if (array1[i].b < array2[j].b)
            {
                i++;
            }
            else
            {
                j++;
            }
        }
       
        double dem1 = 0;
        double dem2 = 0;
       
        for(int a=0; a<array1.length; a++)
        {
                dem1 += array1[a].a * array1[a].a;
               
        }
   
        for(int a=0; a<array2.length; a++)
        {

                dem2 += array2[a].a * array2[a].a;
        }
   
        double denominator = Math.sqrt(dem1) * Math.sqrt(dem2);
        if (denominator==0)
       	return 0;
       
        double result = numerator / denominator;
       
       
        return result;   
    }
    
	
		
	}
