package de.fzi.ipe.soboleo.space.logging;

import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.document.AddTag;
import de.fzi.ipe.soboleo.event.document.RemoveTag;
import de.fzi.ipe.soboleo.event.logging.BrowseConcept;
import de.fzi.ipe.soboleo.event.logging.BrowseRoots;
import de.fzi.ipe.soboleo.event.logging.SubscribeConcept;
import de.fzi.ipe.soboleo.event.logging.SubscribeRoots;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.AddTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ChangeTextCmd;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveTextCmd;
import de.fzi.ipe.soboleo.event.peopletagging.AddPersonTag;
import de.fzi.ipe.soboleo.event.peopletagging.RemovePersonTag;
import de.fzi.ipe.soboleo.event.webdocument.AddWebDocumentTag;
import de.fzi.ipe.soboleo.event.webdocument.RemoveWebDocumentTag;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetWebDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersons;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsByNameAndTopic;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.logging.client.UserLoggingService;
import de.fzi.ipe.soboleo.space.logging.client.UserLoggingServiceImpl;
import de.fzi.ipe.soboleo.user.User;

@Component
public class ExternalEventLoggerImpl extends Thread implements EventLogger {
	private final Logger logger = Logger.getLogger(this.getClass());
	private LinkedBlockingQueue<LogTask> taskQueue = new LinkedBlockingQueue<LogTask>();
	private boolean stopped = false;
	
	@Autowired private UserLoggingService userLoggingService;
	@Autowired private Server server;
	
		
	@Override
	public void log(Event event, Space space) {
		taskQueue.add(new LogTask(event, space));
	}
	
	public ExternalEventLoggerImpl() {
		this.userLoggingService= new UserLoggingServiceImpl();
		this.start();
	}
	
	public synchronized void stopExternalLogger() {
		stopped = true;
		this.interrupt();
	}
	
	public void run() {
		while (!stopped) {
			try {
				LogTask task = taskQueue.take();
				process(task);
			} catch(InterruptedException ie) { ; 
			} catch(Throwable e) {
				e.printStackTrace();
			}
			
		}
		
	}

	private void process(LogTask task) {
		Space space = task.space;
		Event event = task.event;
		try{
			User user = server.getUserDatabase().getUser(event.getSenderURI());
			if(event instanceof GetWebDocumentsSearchResult){
				GetWebDocumentsSearchResult search = (GetWebDocumentsSearchResult) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), "", "searchForDigitalResources", concatExtractedConceptURIs(search.getExtractedConceptURIs()));
			}
			else if(event instanceof GetDocumentsSearchResult){
				GetDocumentsSearchResult search = (GetDocumentsSearchResult) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), "", "searchForDigitalResources", concatExtractedConceptURIs(search.getExtractedConceptURIs()));
			}
			else if (event instanceof SearchPersonsByNameAndTopic){
				SearchPersonsByNameAndTopic search = (SearchPersonsByNameAndTopic) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), "", "searchForPersons", concatExtractedConceptURIs(search.getExtractedConceptURIs()));
			}
			else if (event instanceof SearchPersons){
				SearchPersons search = (SearchPersons) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), "", "searchForPersons", concatExtractedConceptURIs(search.getExtractedConceptURIs()));
			}
			else if(event instanceof AddConnectionCmd){
				AddConnectionCmd cmd = (AddConnectionCmd) event;
				String actionType = "";
				if(cmd.getConnection().equals(SKOS.HAS_BROADER)) actionType = "addTagConnectionBroader";
				else if (cmd.getConnection().equals(SKOS.HAS_NARROWER)) actionType = "addTagConnectionNarrower";
				else if(cmd.getConnection().equals(SKOS.RELATED)) actionType = "addTagConnectionRelated";
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getFromURI(), actionType, cmd.getToURI());
			} else if(event instanceof AddTextCmd){
				AddTextCmd cmd = (AddTextCmd) event;
				String actionType = "";
				if(cmd.getConnection().equals(SKOS.PREF_LABEL)) actionType = "addTagPreferredLabel";
				else if (cmd.getConnection().equals(SKOS.ALT_LABEL)) actionType = "addTagAlternativeLabel";
				else if(cmd.getConnection().equals(SKOS.HIDDEN_LABEL)) actionType = "addTagHiddenLabel";
				else if(cmd.getConnection().equals(SKOS.NOTE)) actionType = "addTagDescription";
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getFromConceptURI(), actionType, cmd.getText().toString());
			} else if(event instanceof ChangeTextCmd){
				ChangeTextCmd cmd = (ChangeTextCmd) event;
				String actionType = "";
				if(cmd.getConnection().equals(SKOS.PREF_LABEL)) actionType = "changeTagPreferredLabel";
				else if (cmd.getConnection().equals(SKOS.ALT_LABEL)) actionType = "changeTagAlternativeLabel";
				else if(cmd.getConnection().equals(SKOS.HIDDEN_LABEL)) actionType = "changeTagHiddenLabel";
				else if(cmd.getConnection().equals(SKOS.NOTE)) actionType = "changeTagDescription";
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getFromConceptURI(), actionType, cmd.getNewText().toString());
			} else if(event instanceof CreateConceptCmd){
				CreateConceptCmd cmd = (CreateConceptCmd) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), "", "createTag", cmd.getInitialName().toString());
			} else if(event instanceof RemoveConceptCmd){
				RemoveConceptCmd cmd = (RemoveConceptCmd) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getConceptURI(), "removeTag", "");
			} else if(event instanceof RemoveConnectionCmd){
				RemoveConnectionCmd cmd = (RemoveConnectionCmd) event;
				String actionType = "";
				if(cmd.getConnection().equals(SKOS.HAS_BROADER)) actionType = "removeTagConnectionBroader";
				else if (cmd.getConnection().equals(SKOS.HAS_NARROWER)) actionType = "removeTagConnectionNarrower";
				else if(cmd.getConnection().equals(SKOS.RELATED)) actionType = "removeTagConnectionRelated";
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getFromConceptURI(), actionType, cmd.getToConceptURI());
			} else if (event instanceof RemoveTextCmd){
				RemoveTextCmd cmd = (RemoveTextCmd) event;
				String actionType = "";
				if(cmd.getConnection().equals(SKOS.PREF_LABEL)) actionType = "removeTagPreferredLabel";
				else if (cmd.getConnection().equals(SKOS.ALT_LABEL)) actionType = "removeTagAlternativeLabel";
				else if(cmd.getConnection().equals(SKOS.HIDDEN_LABEL)) actionType = "removeTagHiddenLabel";
				else if(cmd.getConnection().equals(SKOS.NOTE)) actionType = "removeTagDescription";
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getFromConceptURI(), actionType, cmd.getText().toString());
			} else if(event instanceof AddTag){
				AddTag cmd = (AddTag) event;
				if(cmd instanceof AddWebDocumentTag){
					IndexDocument doc = space.getSearchingLuceneWrapper().getDocument(cmd.getDocumentURI());
					if(doc != null)	userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getSia().getUri(), "addSharedTagToDigitalResource", doc.getURL());
				}
				else userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getSia().getUri(), "addSharedTagToDigitalResource", cmd.getDocumentURI());
				
			} else if (event instanceof RemoveTag){
				RemoveTag cmd = (RemoveTag) event;
				if(cmd instanceof RemoveWebDocumentTag){
					IndexDocument doc = space.getSearchingLuceneWrapper().getDocument(cmd.getDocURI());
					if(doc != null)	userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getConceptURI(), "removeSharedTagToDigitalResource", doc.getURL());
				}
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getConceptURI(), "addSharedTagToDigitalResource", cmd.getDocURI());
			} else if(event instanceof AddPersonTag){
				AddPersonTag cmd = (AddPersonTag) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getConceptID(), "removeSharedTagFromPerson", cmd.getTaggedPersonURI());
			} else if (event instanceof RemovePersonTag){
				RemovePersonTag cmd = (RemovePersonTag) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), cmd.getConceptURI(), "removeSharedTagFromPerson", cmd.getTaggedPersonURI());
			}
			else if(event instanceof BrowseRoots){
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), "", "browseAllTags", "");
			} else if(event instanceof BrowseConcept){
				BrowseConcept e = (BrowseConcept) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), e.getConceptURI(), "browseTag", "");
			} else if(event instanceof SubscribeRoots){
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), "", "subscribeAllTags", "");
			} else if(event instanceof SubscribeConcept){
				SubscribeConcept e = (SubscribeConcept) event;
				userLoggingService.addUserEventRequest(user.getKey(), "mailto:"+user.getEmail(), space.getURI(), e.getConceptURI(), "browseTag", "");
			}
		}catch(Exception e){
			logger.error(e);
		}
	}
	
	private String concatExtractedConceptURIs(Set<String> extractedURIs){
		StringBuffer toReturn = new StringBuffer();
		for(String conceptURI : extractedURIs){
			toReturn.append(conceptURI);
			toReturn.append(",");
		}
		toReturn.deleteCharAt(toReturn.length()-1);
		return toReturn.toString();
	}	
	
}
