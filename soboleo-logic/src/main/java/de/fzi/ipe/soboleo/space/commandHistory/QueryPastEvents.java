/*
 * FZI - Information Process Engineering 
 * Created on 27.09.2008 by zach
 */
package de.fzi.ipe.soboleo.space.commandHistory;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Query used to ask for events newer than an event with a particular 
 * id
 */
public class QueryPastEvents extends QueryEvent{

	private long eventId;
	
	public QueryPastEvents(long eventId) {
		this.eventId = eventId;
	}
	
	public long getEventIdQueried() {
		return eventId;
	}
	
	
	
}
