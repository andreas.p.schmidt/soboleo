package de.fzi.ipe.soboleo.gardening.recSys;

public class intPair implements Comparable<intPair> {
	public int a; // id
	public int b; // count

	public static boolean ascending = false;
	public static boolean decendingA = false;

	public intPair() {

	}

	public intPair(int a, int b) {
		this.a = a;
		this.b = b;
	}

	// default is descending
	public int compareTo(intPair ip) {
		if (ascending)
			return b - ip.b;

		if (decendingA)
			return ip.a - a;

		if (ip.b != b)
			return ip.b - b;

		return ip.a - a;
	}

	public static void setAscending(boolean b) {
		ascending = b;
	}

	public static void setDecendingA(boolean b) {
		decendingA = b;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + a;
		result = prime * result + b;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		intPair other = (intPair) obj;
		if (a != other.a)
			return false;
		if (b != other.b)
			return false;
		return true;
	}

	public String toString() {
		return a + "," + b;
	}
}
