package de.fzi.ipe.soboleo.gardening.recommendations;


import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;

public class ConceptRecommendation extends  GardeningRecommendation implements IsSerializable{
	
	private ClientSideConcept concept;
	private ClientSideConcept recommendation;
	public double conf;
	int p;
	
	private SKOS type;
	
	@SuppressWarnings("unused")
	private ConceptRecommendation() {; }
	
	public ConceptRecommendation(ClientSideConcept concept, ClientSideConcept recommendation,double confidence){
	this.concept=concept;
	this.recommendation=recommendation;
	this.conf=confidence;

	}
	public ConceptRecommendation(ClientSideConcept concept, ClientSideConcept recommendation,int p){
		this.concept=concept;
		this.recommendation=recommendation;
		this.p=p;
		this.conf=p;
		}
	public ClientSideConcept getConcept() {
		
		return concept;
	}
		
	public String getConceptURI() {
		
		return concept.getURI();
	}

	public double getConfidence(){
		return conf;
	}
	
	public int getPriority() {
		
		return p;
	}
	public String getRecommendationURI(){
		return recommendation.getURI();}
	
	public ClientSideConcept getRecommendation(){
		return recommendation;
		}
	
	public void setType(SKOS type){
		this.type = type;
	}
	
	public SKOS getType(){
		return type;
	}
	
	public String toString(){
		String result=new String();
		result=concept.getText(SKOS.PREF_LABEL).getString()+": "+recommendation.getText(SKOS.PREF_LABEL).getString()+" "+conf;
		return result;
	}


}
