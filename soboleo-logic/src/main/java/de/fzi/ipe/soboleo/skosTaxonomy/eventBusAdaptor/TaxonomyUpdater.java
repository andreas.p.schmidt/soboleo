/*
 * Class that keeps an ClientSideTaxonomy up-to-date based on the events on the
 * event bus. This class does not need to be registered with the EventBus - it does this itself. 
 * The best way to use this class is to just query for a ClientSideTaxonomy with 'update' set to true.
 * Created on 20.02.2009 by zach
 */
package de.fzi.ipe.soboleo.skosTaxonomy.eventBusAdaptor;

import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.eventbus.EventBusEventProcessor;

@Component
public class TaxonomyUpdater implements EventBusEventProcessor{

	private ClientSideTaxonomy taxonomy;
	
	public TaxonomyUpdater(ClientSideTaxonomy taxonomy) {
		this.taxonomy = taxonomy;
	}

	@Override
	public void receiveEvent(Event event) {
		if (event instanceof CommandEvent) {
			taxonomy.apply((CommandEvent) event);
		}
		
	}

}
