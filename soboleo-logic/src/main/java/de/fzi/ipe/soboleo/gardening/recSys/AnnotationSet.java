package de.fzi.ipe.soboleo.gardening.recSys;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;

public class AnnotationSet {
	
  //  private ClientSideTaxonomy tax;	
	//private Set<WebDocument> docs;
	public int[][] bar;
	public int length = 0;
	private int maxUsrId = -1;
	private int maxResId = -1;
	private int maxTagId = -1;
	public Map <String,Integer> userMap=new HashMap<String,Integer>();
	public Map <String,Integer> resMap=new HashMap<String,Integer>();
	public Map <String,Integer> tagMap=new HashMap<String,Integer>();

	 public AnnotationSet(ClientSideTaxonomy tax,Set<WebDocument> docs){
		// this.tax=tax;
		 //this.docs=docs;
		 int annotationNum=0;
		 for (WebDocument w:docs){
		// System.out.println(w.getURL());
		 Set<Tag> annotations=w.getAllTags();
		 annotationNum=annotations.size()+annotationNum;
		 }
		 bar=new int[annotationNum][3];
		 int i=0; int userId=1;int tagId=1;int resId=1;
		 for(WebDocument w:docs){
		 Set<Tag> annotations=w.getAllTags();
		
		 for(Tag t:annotations){
			 
			 if (userMap.containsKey(t.getUserID()))
		     bar[i][0]=userMap.get(t.getUserID())	;
			 else{
			userMap.put(t.getUserID(), userId);
			//System.out.println("tag.getuserId="+t.getUserID());
			 bar[i][0]=userId;
			userId++;
			 }
			 if (resMap.containsKey(w.getURI()))
			     bar[i][1]=resMap.get(w.getURI())	;
				 else{
				resMap.put(w.getURI(), resId);
			//	System.out.println("w.geturi="+w.getURI());
				 bar[i][1]=resId;
				resId++;
				 }
			 if (tagMap.containsKey(t.getConceptID()))
			     bar[i][2]=tagMap.get(t.getConceptID())	;
				 else{
				tagMap.put(t.getConceptID(), tagId);
			//	System.out.println("tag.getconcetpId="+t.getConceptID()+" "+tagId);
				
				 bar[i][2]=tagId;
				tagId++;
				 }
			 i++;
				 }
		 length = bar.length;
		 }
		 }
	 
	 public intPair[][] buildResourceProfileAsTagBinary()
		{
			
			intPair[][] temp = buildResourceProfileAsTagCount();
			
			for(int i=0; i<temp.length; i++)
			{
				for(int j=0; j<temp[i].length; j++)
				{
					temp[i][j].a = 1;
				}
			}
			
			
			return temp;
		}
		
		public intPair[][] buildTagProfileAsResourceCount()
		{
			
			
			@SuppressWarnings("unchecked")
			ArrayList<intPair>[] lists = new ArrayList[getMaxTagId()+1];
			
			for(int i=0; i<lists.length; i++)
				lists[i] = new ArrayList<intPair>();
				
			for(int i=0; i<length; i++)
			{	
				intPair ip = new intPair();
				ip.b = getRes(i);
				ip.a = 1;
				
				lists[getTag(i)].add(ip);
			}
			
			for(int i=0; i<lists.length; i++)
			{
				Collections.sort(lists[i]);
				
				for(int j=lists[i].size()-1; j>0; j--)
				{
					if( lists[i].get(j).b ==  lists[i].get(j-1).b )
					{
						lists[i].get(j-1).a += lists[i].get(j).a;
						lists[i].remove(j);
					}
				}
			}
			
			intPair[][] result = new intPair[lists.length][];
			
			for(int i=0; i<result.length; i++)
			{
				result[i] = new intPair[lists[i].size()];
				
				for(int j=0; j<result[i].length; j++)
				{
					result[i][j] = lists[i].get(j);
				}
			}
			
			intPair.setAscending(true);
			for(int i=0; i<result.length; i++)
			{
				Arrays.sort(result[i]);
			}
			intPair.setAscending(false);

			
			return result;
		}
		public intPair[][] buildResourceProfileAsTagCount()
		{
						
			@SuppressWarnings("unchecked")
			ArrayList<intPair>[] lists = new ArrayList[getMaxResId()+1];
			
			for(int i=0; i<lists.length; i++)
				lists[i] = new ArrayList<intPair>();
				
			for(int i=0; i<length; i++)
			{	
				boolean found = false;
				for(int j=0; j<lists[getRes(i)].size(); j++)
				{
					int x = getTag(i);
					
					if(lists[getRes(i)].get(j).b == x)
					{
						lists[getRes(i)].get(j).a++;
						j=lists[getRes(i)].size();
						found=true;
					}
				}
				
				if(!found)
				{
					intPair ip = new intPair();
					ip.b = getTag(i);
					ip.a = 1;
					
					lists[getRes(i)].add(ip);
				}
			}
			
			intPair[][] result = new intPair[lists.length][];
			
			for(int i=0; i<result.length; i++)
			{
				result[i] = new intPair[lists[i].size()];
				
				for(int j=0; j<result[i].length; j++)
				{
					result[i][j] = lists[i].get(j);
				}
			}
			
			intPair.setAscending(true);
			for(int i=0; i<result.length; i++)
			{
				Arrays.sort(result[i]);
			}
			intPair.setAscending(false);

			return result;
		}
		public intPair[][] buildTagProfileAsUserCount()
		{
			
			
			@SuppressWarnings("unchecked")
			ArrayList<intPair>[] lists = new ArrayList[getMaxTagId()+1];
			
			for(int i=0; i<lists.length; i++)
				lists[i] = new ArrayList<intPair>();
				
			for(int i=0; i<length; i++)
			{	
				intPair ip = new intPair();
				ip.b = getUser(i);
				ip.a = 1;
				
				lists[getTag(i)].add(ip);
			}
			
			for(int i=0; i<lists.length; i++)
			{
				Collections.sort(lists[i]);
				
				for(int j=lists[i].size()-1; j>0; j--)
				{
					if( lists[i].get(j).b ==  lists[i].get(j-1).b )
					{
						lists[i].get(j-1).a += lists[i].get(j).a;
						lists[i].remove(j);
					}
				}
			}
			
			intPair[][] result = new intPair[lists.length][];
			
			for(int i=0; i<result.length; i++)
			{
				result[i] = new intPair[lists[i].size()];
				
				for(int j=0; j<result[i].length; j++)
				{
					result[i][j] = lists[i].get(j);
				}
			}
			
			intPair.setAscending(true);
			for(int i=0; i<result.length; i++)
			{
				Arrays.sort(result[i]);
			}
			intPair.setAscending(false);
			
			
			return result;
		}
		public int getUser(int i)
		{
			return bar[i][0];
		}
		
		public int getRes(int i)
		{
			return bar[i][1];
		}
		
		public int getTag(int i)
		{
			return bar[i][2];
		}
		public int getMaxUserId()
		{
			if(maxUsrId != -1)
				return maxUsrId;
			
			int max = 0;
			for (int i = 0; i < bar.length; i++)
				if (bar[i][0] > max) max = bar[i][0];
			
			maxUsrId = max;
			return max;
		}

		public int getMaxResId()
		{
			if(maxResId != -1)
				return maxResId;
			
			int max = 0;
			for (int i = 0; i < bar.length; i++)
				if (bar[i][1] > max) max = bar[i][1];
			
			maxResId = max;
			return max;
		}
		
		public int getMaxTagId()
		{
			if(maxTagId != -1)
				return maxTagId;
			
			int max = 0;
			for (int i = 0; i < bar.length; i++)
				if (bar[i][2] > max) max = bar[i][2];
			
			maxTagId = max;
			return max;
		}
		public intPair[] getTagasResourceVector(int tag){
			intPair[][] tagprof= buildResourceProfileAsTagCount();
			 return tagprof[tag];
		}

}
