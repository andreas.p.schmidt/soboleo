package de.fzi.ipe.soboleo.server.client.rpc;

import com.google.gwt.user.client.rpc.RemoteService;

import de.fzi.ipe.soboleo.beans.client.ClientConfiguration;


public interface LoginService extends RemoteService{
	
	public ClientConfiguration getConfig(String key, String space);
	
	public String login(String email, String password);
	
}
