package de.fzi.ipe.mature;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fzi.ipe.mature.XMLDialog.XMLUser;
import de.fzi.ipe.soboleo.beans.dialog.Dialog;
import de.fzi.ipe.soboleo.event.dialog.AddParticipant;
import de.fzi.ipe.soboleo.event.dialog.SetDialogContent;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetDialog;
import de.fzi.ipe.soboleo.server.MessageConnector;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.upb.mature.api.java.MessageListener;
import de.upb.mature.api.java.UserCredentials;
import de.upb.mature.api.java.UserCredentialsProvider;
import de.upb.mature.api.java.WidgetServerConnector;
import de.upb.mature.api.java.WidgetServerConnectorException;
import de.upb.mature.widgetserver.messagesystem.Message;

/**
 * @deprecated
 *
 */

@Component
public class WidgetConnector implements MessageConnector {

	@Autowired
	private Server server;

	WidgetServerConnector widgetServer;
	XMLDialogRW rw;
	private boolean connected = false;
	UCProvider ucp = new UCProvider("nelkner", "nelkner");
	
	public WidgetConnector() throws IOException, WidgetServerConnectorException{
		rw = new XMLDialogRW();
		widgetServer = new WidgetServerConnector("soboleo", new MessageListener(){
			@Override
			public void onMessage(Message msg) {
				if(msg.getMessageType() != null && msg.getMessageType().equals("wm.soboleo.returnContent")) processResponse(msg.getContent());
				else{
					System.out.println("Incoming Message from Widget Server:\n\t"
						+ msg.getMessageType());
					System.out.println("Inhalt:\n" + msg.getContent());
//					processResponse(msg.getContent());
				}
			}
		});
		this.connect();
	}
	
	private void connect()throws WidgetServerConnectorException{
		try {
			widgetServer.connect(ucp.getUserCredentials(), new WidgetConnectorErrorListener(this, widgetServer, ucp, 3, 10));
			connected = true;
		} catch (WidgetServerConnectorException e) {
			// TODO Auto-generated catch block
			connected = false;
			throw e;
		}
	}
	
	public synchronized void setConnected(boolean connectedStatus) {
		this.connected = connectedStatus;
	}
	
	public int sendMessage(String content) throws IOException{
		int result = 0;
		System.out.println("send message: " +content);
		try {
			if(!connected)	this.connect(); 
			result = widgetServer.sendGroupBroadcastMessage("wm.soboleo.startDiscussion", content);
		} catch (WidgetServerConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IOException(e.getCause());
		}
		return result;
	}
	
	public void processResponse(String content) {
		System.out.println("content: " +content);
		try {			
			if(content.indexOf("<content>") != -1) content = content.substring(8);
			if(content.indexOf("</content>") != -1) content = content.substring(0, content.length()-10);
			XMLDialog responseDialog = rw.fromString(content);
			if(responseDialog != null){
				Space space = server.getSpaces().getSpace(responseDialog.getSpaceURI());
				Dialog dialog = (Dialog) space.getEventBus().executeQuery(new GetDialog(responseDialog.getDialogURI()), EventSenderCredentials.SERVER);
				for(XMLUser participant:responseDialog.getParticipants()){
					if(!dialog.hasParticipant(participant.getUserURI())){
						space.getEventBus().executeCommand(new AddParticipant(responseDialog.getDialogURI(), participant.getUserURI()), EventSenderCredentials.SERVER);
					}
				}
				String dialogContent = responseDialog.getDialogContent();
				dialogContent = dialogContent.replaceFirst("<!\\[CDATA\\[", "");
				dialogContent = dialogContent.replace("]]>", "");
				if(!dialogContent.equals("")) dialogContent = "<table>" + dialogContent +"</table>";
				space.getEventBus().executeCommand(new SetDialogContent(responseDialog.getDialogURI(), dialogContent), EventSenderCredentials.SERVER);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EventPermissionDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String readFileToString(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		StringBuilder builder = new StringBuilder();
		String temp = reader.readLine();
		while (temp != null) {
			builder.append(temp);
			temp = reader.readLine();
		}
		reader.close();
		return builder.toString();
	}
	
	private class UCProvider implements UserCredentialsProvider {
		 UserCredentials uc;		 
		 public UCProvider(String username, String password) {
		  this.uc = new UserCredentials(username, password);
		 }		 
		 @Override
		 public UserCredentials getUserCredentials() {
		  return this.uc;
		 }
	}
}