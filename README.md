# License

This software is licensed under the GNU Public License v3.

SOBOLEO has originally been developed by FZI Research Center for Information
Technologies (http://www.fzi.de) by Simone Braun, Valentin Zacharias, and 
Andreas Walter. It was released as open source software in 2012.

This development was supported by the European Commission within the context of
the MATURE IP project (http://mature-ip.eu).

Further development and refactoring have been taken over by Karlsruhe University
of Applied Sciences (http://www.iwi.hs-karlsruhe.de) under the coordination of
Andreas P. Schmidt.

A current demo is available under http://soboleo.knowledge-maturing.com

Contact: http://andreas.schmidt.name

